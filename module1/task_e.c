// Даны n нестрого возрастающих массивов A_i и m нестрого убывающих массивов B_j. Все массивы имеют одну и ту же длину l.
// Далее даны q запросов вида (i, j), ответ на запрос – такое k, что max(A_i_k, B_j_k) минимален. Если таких k несколько,
// можно вернуть любое.
//
// #Входные данные
// На первой строке числа n,m,l (1 ≤ n, m ≤ 900; 1 ≤ l ≤ 3000). Следующие n строк содержат описания массивов A_i.
// Каждый массив описывается перечислением l элементов. Элементы массива – целые числа от 0 до (10^5 − 1). Далее
// число m и описание массивов B_j в таком же формате. Массивы и элементы внутри массива нумеруются с 1.
// На следюущей строке - число запросов q (1 ≤ q ≤ n * m). Следующие q строк содержат пары чисел i, j (1 ≤ i ≤ n,
// 1 ≤ j ≤ m).
//
// #Выходные данные
// Выведите q чисел от 1 до l – ответы на запросы.

#include <stdio.h>
#include <malloc.h>

// Рассматриваем массив a из length элементов, в котором
// a[i] = 1, если increasing[i] > decreasing[i], 0 иначе.
//
// Дальше можно найти нужную позицию k (на стыке 0 и 1) бинпоиском,
// как в 5 задаче теор. домашки.
int bin_search(const int *increasing, const int *decreasing, int length) {
  int left = 0;
  int right = length - 1;

  while (left < (right - 1)) {
    int middle = (left + right) / 2;

    if (increasing[middle] > decreasing[middle]) {
      right = middle;
    } else {
      left = middle;
    }
  }

  int k;
  if ((increasing[left] < decreasing[left]) && (increasing[right] < decreasing[right])) {
    k = right;
  } else if ((increasing[left] < decreasing[left]) && (increasing[right] >= decreasing[right])) {
    if (increasing[right] < decreasing[left]) {
      k = right;
    } else {
      k = left;
    }
  } else {
    k = left;
  }

  return k;
}

int main() {
  int n, m, l;
  scanf("%d%d%d", &n, &m, &l);

  int (*A)[l] = (int (*)[l]) malloc(sizeof(int[n][l]));
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < l; j++) {
      scanf("%d", &A[i][j]);
    }
  }

  int (*B)[l] = (int (*)[l]) malloc(sizeof(int[m][l]));
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < l; j++) {
      scanf("%d", &B[i][j]);
    }
  }

  int q;
  scanf("%d", &q);

  for (int k = 0; k < q; k++) {
    int i, j;
    scanf("%d%d", &i, &j);

    printf("%d\n", bin_search(A[i - 1], B[j - 1], l) + 1);
  }

  free(A);
  free(B);

  return 0;
}
