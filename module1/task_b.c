// По заданной строке из круглых/квадратных/фигурных открывающих и закрывающих скобок определить,
// является ли она правильной скобочной последовательностью.
//
// Множество правильных скобочных последовательностей (ПСП) определяется как наименьшее множество с условиями:
// + пустая строка является ПСП;
// + если S — ПСП, то (S), [S], {S} — тоже ПСП;
// + если S1 и S2 — ПСП, то S1S2 — тоже ПСП.

#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>
#include <assert.h>

typedef struct Node {
  struct Node *next;
  int value;
} Node;

typedef struct Stack {
  Node *top;
  int size;
} Stack;

void InitStack(Stack *stack) {
  assert(stack != NULL);

  stack->size = 0;
  stack->top = NULL;
}

bool IsEmpty(Stack *stack) {
  assert(stack != NULL);
  return (stack->size == 0);
}

int PushBack(Stack *stack, int element) {
  assert(stack != NULL);

  Node *new_node = (Node *) malloc(sizeof(Node));
  if (new_node == NULL) {
    return -1;
  }

  new_node->value = element;
  new_node->next = stack->top;

  stack->top = new_node;
  stack->size++;

  return 0;
}

int PopBack(Stack *stack) {
  assert(stack != NULL);
  assert(!IsEmpty(stack));

  Node *top = stack->top;
  Node *next = top->next;
  int element = top->value;

  free(top);
  stack->top = next;
  stack->size--;

  return element;
}

int Back(Stack *stack) {
  assert(stack != NULL);
  assert(!IsEmpty(stack));
  return stack->top->value;
}

int Size(Stack *stack) {
  assert(stack != NULL);
  return stack->size;
}

void Clear(Stack *stack) {
  assert(stack != NULL);
  while (!IsEmpty(stack)) {
    PopBack(stack);
  }
}

bool IsMatch(char opening, char closing) {
  return ((opening == '(') && (closing == ')')) ||
      ((opening == '[') && (closing == ']')) ||
      ((opening == '{') && (closing == '}'));
}

bool IsOpening(char bracket) {
  return (bracket == '(') || (bracket == '[') || (bracket == '{');
}

int main() {
  Stack brackets;
  InitStack(&brackets);

  char current_bracket;
  while ((current_bracket = (char) getchar()) != '\n') {
    if (IsOpening(current_bracket)) {
      PushBack(&brackets, current_bracket);
    } else if (!IsEmpty(&brackets) && IsMatch(Back(&brackets), current_bracket)) {
      PopBack(&brackets);
    } else {
      printf("no\n");
      return 0;
    }
  }

  printf((Size(&brackets) == 0) ? "yes\n" : "no\n");
  return 0;
}
