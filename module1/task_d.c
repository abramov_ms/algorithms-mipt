// Гоблины Мглистых гор очень любят ходить к своим шаманам. Так как гоблинов много, к шаманам часто образуются очень
// длинные очереди. А поскольку много гоблинов в одном месте быстро образуют шумную толку, которая мешает шаманам
// проводить сложные медицинские манипуляции, последние решили установить некоторые правила касательно порядка в очереди.
//
// Обычные гоблины при посещении шаманов должны вставать в конец очереди. Привилегированные же гоблины, знающие особый
// пароль, встают ровно в ее середину, причем при нечетной длине очереди они встают сразу за центром.
//
// Так как гоблины также широко известны своим непочтительным отношением ко всяческим правилам и законам, шаманы
// попросили вас написать программу, которая бы отслеживала порядок гоблинов в очереди.
//
// #Входные данные
//
// В первой строке входных данных записано число N (1 ≤ N ≤ 105) − количество запросов. Следующие N строк содержат
// описание запросов в формате:
// + "+ i"  − гоблин с номером i (1 ≤ i ≤ N) встаёт в конец очереди.
// + "* i"  − привилегированный гоблин с номером i встает в середину очереди.
// + "-"    − первый гоблин из очереди уходит к шаманам. Гарантируется, что на момент такого запроса очередь не пуста.
//
// #Выходные данные
//
// Для каждого запроса типа "-" программа должна вывести номер гоблина, который должен зайти к шаманам.

#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include <stdbool.h>

typedef struct Node {
  struct Node *prev;
  int value;
} Node;

typedef struct Queue {
  Node *first;
  Node *middle;
  Node *last;
  int size;
} Queue;

void InitQueue(Queue *queue) {
  assert(queue != NULL);

  queue->first = queue->middle = queue->last = NULL;
  queue->size = 0;
}

int Size(Queue *queue) {
  assert(queue != NULL);
  return queue->size;
}

bool IsEmpty(Queue *queue) {
  assert(queue != NULL);
  return (Size(queue) == 0);
}

int PushBack(Queue *queue, int element) {
  assert(queue != NULL);

  Node *new_last = (Node *) malloc(sizeof(Node));
  if (new_last == NULL) {
    return -1;
  }

  new_last->value = element;
  new_last->prev = NULL;

  if (IsEmpty(queue)) {
    queue->first = queue->middle = queue->last = new_last;
  } else {
    queue->last->prev = new_last;
    queue->last = new_last;

    if ((Size(queue) % 2) == 0) {
      queue->middle = queue->middle->prev;
    }
  }

  queue->size++;

  return 0;
}

int PushMiddle(Queue *queue, int element) {
  assert(queue != NULL);

  if (Size(queue) < 2) {
    PushBack(queue, element);
    return 0;
  }

  Node *new_element = (Node *) malloc(sizeof(Node));
  if (new_element == NULL) {
    return -1;
  }

  new_element->value = element;
  new_element->prev = queue->middle->prev;
  queue->middle->prev = new_element;

  if ((Size(queue) % 2) == 0) {
    queue->middle = new_element;
  }

  queue->size++;

  return 0;
}

int Front(Queue *queue) {
  assert(queue != NULL);
  assert(!IsEmpty(queue));

  return queue->first->value;
}

int PopFront(Queue *queue) {
  assert(queue != NULL);
  assert(!IsEmpty(queue));

  Node *first = queue->first;
  int element = first->value;

  queue->first = first->prev;
  if ((Size(queue) % 2) == 0) {
    queue->middle = queue->middle->prev;
  }

  free(first);
  queue->size--;

  return element;
}

void ClearQueue(Queue *queue) {
  assert(queue != NULL);

  while (!IsEmpty(queue)) {
    PopFront(queue);
  }
}

int main() {
  int N;
  scanf("%d", &N);

  Queue goblins;
  InitQueue(&goblins);

  for (int i = 0; i < N; i++) {
    char command;
    scanf(" %c", &command);

    if (command == '-') {
      printf("%d\n", Front(&goblins));
      PopFront(&goblins);
    } else {
      int new_goblin;
      scanf("%d", &new_goblin);

      int return_code;
      switch (command) {
        case '+':
          return_code = PushBack(&goblins, new_goblin);
          break;
        case '*':
          return_code = PushMiddle(&goblins, new_goblin);
          break;
        default:
          printf("Unknown command: %c\n", command);
          return -1;
      }

      if (return_code != 0) {
        printf("Could not allocate new node at iteration %d\n", i);
        return -1;
      }
    }
  }

  ClearQueue(&goblins);

  return 0;
}
