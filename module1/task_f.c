// Напишите программу, которая для заданного массива A = ⟨a1, a2, …, an⟩ находит количество пар (i, j) таких,
// что i < j и a_i > a_j. Обратите внимание на то, что ответ может не влезать в int.
//
// #Входные данные
// Первая строка входного файла содержит натуральное число n (1 ⩽ n ⩽ 100000) — количество элементов массива.
// Вторая строка содержит n попарно различных элементов массива A — целых неотрицательных чисел, не превосходящих 109.
//
// #Выходные данные
// В выходной файл выведите одно число — ответ на задачу.

#include <stdio.h>
#include <malloc.h>

int Min(int x, int y) {
  return (x < y) ? x : y;
}

int MergeCountInversions(int *src_array, int *destination, int start, int middle, int end) {
  int inversions_count = 0;

  int first_index = start;
  int second_index = middle + 1;
  int destination_index = start;

  while ((first_index <= middle) && (second_index <= end)) {
    if (src_array[first_index] <= src_array[second_index]) {
      destination[destination_index] = src_array[first_index];
      first_index++;
    } else {
      destination[destination_index] = src_array[second_index];
      second_index++;

      inversions_count += middle - first_index + 1;
    }

    destination_index++;
  }

  while (first_index <= middle) {
    destination[destination_index] = src_array[first_index];
    destination_index++;
    first_index++;
  }

  while (second_index <= end) {
    destination[destination_index] = src_array[second_index];
    destination_index++;
    second_index++;
  }

  for (int copy_index = start; copy_index <= end; copy_index++) {
    src_array[copy_index] = destination[copy_index];
  }

  return inversions_count;
}

long long MergeSortCountInversions(int *array, int length) {
  int *sorted_array = (int *) malloc(sizeof(int[length]));
  long long inversions_count = 0;

  for (int unit_length = 1; unit_length < length; unit_length *= 2) {
    for (int start = 0; start < length; start += (unit_length * 2)) {
      int middle = Min(start + unit_length - 1, length - 1);
      int end = Min(start + (unit_length * 2) - 1, length - 1);

      inversions_count += MergeCountInversions(array, sorted_array, start, middle, end);
    }
  }

  free(sorted_array);

  return inversions_count;
}

int main() {
  FILE *in = fopen("inverse.in", "r");
  FILE *out = fopen("inverse.out", "w");

  int n;
  fscanf(in, "%d", &n);

  int *A = (int *) malloc(sizeof(int[n]));
  for (int i = 0; i < n; i++) {
    fscanf(in, "%d", &A[i]);
  }

  fprintf(out, "%lld", MergeSortCountInversions(A, n));

  fclose(in);
  fclose(out);

  free(A);

  return 0;
}
