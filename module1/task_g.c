// Герой по имени Магина сражается с группой из n монстров с помощью легендарного топора, известного как Ярость Битвы.
// Каждый из монстров имеет a_i очков здоровья. Каждым ударом топора Магина уменьшает здоровье того, кого он ударил,
// на p очков, при этом уменьшая здоровье всех остальных монстров на q очков. Монстр умирает, когда у него остается 0
// или менее очков здоровья. Магина хочет при каждом ударе выбирать цель таким образом, чтобы убить всех монстров
// за минимальное количество ударов. Требуется определить это количество.
//
// #Входные данные
// В первой строке содержатся три целых числа через пробел: n, p и q (1 ⩽ n ⩽ 200000, 1 ⩽ q ⩽ p ⩽ 10^9) — количество
// монстров, урон по цели и урон по всем остальным соответственно.
// Во второй строке содержатся n целых чисел через пробел: a_i (1 ⩽ a_i ⩽ 10^9) — количество очков здоровья
// у каждого из монстров.
//
// #Выходные данные
// Выведите единственное целое число — минимальное количество ударов, за которое Магина сможет убить всех монстров.

#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>

bool IsPossible(const int *hp, int enemies_count, int direct_damage, int global_damage, int turns) {
  int additional_damage = direct_damage - global_damage;

  long long punches_count = 0;
  for (int i = 0; i < enemies_count; i++) {
    if (hp[i] > 1LL * turns * global_damage) {
      if (additional_damage == 0) {
        return false;
      }

      long long current_punches = (hp[i] - (1LL * turns * global_damage)) / additional_damage;
      if ((current_punches * additional_damage) < (hp[i] - (1LL * turns * global_damage))) {
        current_punches++;
      }

      punches_count += current_punches;
    }
  }

  return (punches_count <= turns);
}

int BinSearchMinTurns(const int *hp, int enemies_count, int direct_damage, int global_damage, int glb, int lub) {
  int left = glb;
  int right = lub;

  while (left < right) {
    int middle = (left + right) / 2;

    if (IsPossible(hp, enemies_count, direct_damage, global_damage, middle)) {
      right = middle;
    } else {
      left = middle + 1;
    }
  }

  return right;
}

int main() {
  int n, p, q;
  scanf("%d%d%d", &n, &p, &q);

  int *a = (int *) malloc(sizeof(int[n]));

  int max_hp = 0;
  long long hp_sum = 0;

  for (int i = 0; i < n; i++) {
    scanf("%d", &a[i]);

    hp_sum += a[i];
    if (a[i] > max_hp) {
      max_hp = a[i];
    }
  }

  int min_possible_turns = (int) (hp_sum / (p + (1LL * q * (n - 1))));
  int max_possible_turns = (max_hp / q) + 1;

  int min_turns = BinSearchMinTurns(a, n, p, q, min_possible_turns, max_possible_turns);
  printf("%d\n", min_turns);

  free(a);
}
