// Гистограмма является многоугольником, сформированным из последовательности прямоугольников, выровненных на общей
// базовой линии. Прямоугольники имеют равную ширину, но могут иметь различные высоты. Обычно гистограммы используются
// для представления дискретных распределений, например, частоты символов в текстах. Отметьте, что порядок
// прямоугольников очень важен. Вычислите область самого большого прямоугольника в гистограмме, который также находится
// на общей базовой линии.

#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>
#include <assert.h>

typedef struct Bar {
  int number;
  int height;
} Bar;

typedef struct Node {
  struct Node *next;
  Bar *value;
} Node;

typedef struct Stack {
  Node *top;
  int size;
} Stack;

void InitStack(Stack *stack) {
  assert(stack != NULL);

  stack->size = 0;
  stack->top = NULL;
}

bool IsEmpty(Stack *stack) {
  assert(stack != NULL);
  return (stack->size == 0);
}

int PushBack(Stack *stack, Bar *element) {
  assert(stack != NULL);

  Node *new_node = (Node *) malloc(sizeof(Node));
  if (new_node == NULL) {
    return -1;
  }

  new_node->value = element;
  new_node->next = stack->top;

  stack->top = new_node;
  stack->size++;

  return 0;
}

Bar *PopBack(Stack *stack) {
  assert(stack != NULL);
  assert(!IsEmpty(stack));

  Node *top = stack->top;
  Node *next = top->next;
  Bar *element = top->value;

  free(top);
  stack->top = next;
  stack->size--;

  return element;
}

Bar *Back(Stack *stack) {
  assert(stack != NULL);
  assert(!IsEmpty(stack));
  return stack->top->value;
}

int Size(Stack *stack) {
  assert(stack != NULL);
  return stack->size;
}

void Clear(Stack *stack) {
  assert(stack != NULL);
  while (!IsEmpty(stack)) {
    PopBack(stack);
  }
}

int main() {
  int N;
  scanf("%d", &N);

  Stack bars;
  InitStack(&bars);

  long long max_area = 0;

  for (int i = 0; i < N; i++) {
    int h;
    scanf("%d", &h);

    Bar current_bar = {i, h};
    int last_popped_bar_number = -1;

    while (!IsEmpty(&bars) && (current_bar.height < Back(&bars)->height)) {
      long long area = 1LL * Back(&bars)->height * (current_bar.number - Back(&bars)->number);
      if (area > max_area) {
        max_area = area;
      }

      last_popped_bar_number = Back(&bars)->number;
      free(PopBack(&bars));
    }

    Bar* bar_to_push = (Bar *)malloc(sizeof(Bar));
    if (last_popped_bar_number == -1) {
      *bar_to_push = current_bar;
    } else {
      bar_to_push->number = last_popped_bar_number;
      bar_to_push->height = current_bar.height;
    }

    PushBack(&bars, bar_to_push);
  }

  while (!IsEmpty(&bars)) {
    long long area = 1LL * Back(&bars)->height * (N - Back(&bars)->number);
    if (area > max_area) {
      max_area = area;
    }
    free(PopBack(&bars));
  }

  printf("%lld\n", max_area);
}
