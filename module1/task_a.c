// Реализуйте свой стек. Решения, использующие std::stack, получат 1 балл. Решения, хранящие стек в массиве, получат 1.5 балла.
// Решения, использующие указатели, получат 2 балла.
//
// Гарантируется, что количество элементов в стеке ни в какой момент времени не превышает 10000.
//
// Обработайте следующие запросы:
// + push n: добавить число n в конец стека и вывести «ok»;
// + pop: удалить из стека последний элемент и вывести его значение, либо вывести «error», если стек был пуст;
// + back: сообщить значение последнего элемента стека, либо вывести «error», если стек пуст;
// + size: вывести количество элементов в стеке;
// + clear: опустошить стек и вывести «ok»;
// + exit: вывести «bye» и завершить работу.

#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>
#include <assert.h>
#include <mem.h>

typedef struct Node {
  struct Node *next;
  int value;
} Node;

typedef struct Stack {
  Node *top;
  int size;
} Stack;

void InitStack(Stack *stack) {
  assert(stack != NULL);

  stack->size = 0;
  stack->top = NULL;
}

bool IsEmpty(Stack *stack) {
  assert(stack != NULL);
  return (stack->size == 0);
}

int PushBack(Stack *stack, int element) {
  assert(stack != NULL);

  Node *new_node = (Node *) malloc(sizeof(Node));
  if (new_node == NULL) {
    return -1;
  }

  new_node->value = element;
  new_node->next = stack->top;

  stack->top = new_node;
  stack->size++;

  return 0;
}

int PopBack(Stack *stack) {
  assert(stack != NULL);
  assert(!IsEmpty(stack));

  Node *top = stack->top;
  Node *next = top->next;
  int element = top->value;

  free(top);
  stack->top = next;
  stack->size--;

  return element;
}

int Back(Stack *stack) {
  assert(stack != NULL);
  assert(!IsEmpty(stack));
  return stack->top->value;
}

int Size(Stack *stack) {
  assert(stack != NULL);
  return stack->size;
}

void Clear(Stack *stack) {
  assert(stack != NULL);
  while (!IsEmpty(stack)) {
    PopBack(stack);
  }
}

int main() {
  Stack stack;
  InitStack(&stack);

  while (true) {
    char command[10];
    scanf("%s", command);

    if (strcmp(command, "push") == 0) {
      int element;
      scanf("%d", &element);
      printf((PushBack(&stack, element) == 0) ? "ok\n" : "error\n");
    } else if (strcmp(command, "pop") == 0) {
      if (!IsEmpty(&stack)) {
        printf("%d\n", PopBack(&stack));
      } else {
        printf("error\n");
      }
    } else if (strcmp(command, "back") == 0) {
      if (!IsEmpty(&stack)) {
        printf("%d\n", Back(&stack));
      } else {
        printf("error\n");
      }
    } else if (strcmp(command, "size") == 0) {
      printf("%d\n", Size(&stack));
    } else if (strcmp(command, "clear") == 0) {
      Clear(&stack);
      printf("ok\n");
    } else {
      Clear(&stack);
      printf("bye\n");
      break;
    }
  }

  return 0;
}
