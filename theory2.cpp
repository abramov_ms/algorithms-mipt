#include <iostream>
#include <vector>
#include <tuple>

struct Node {
  int sum;
  int sum_of_squares;
  int sum_of_cubes;
};

void BuildSegTree(std::vector<Node> &seg_tree,
                  const std::vector<int> &leafs,
                  int vertex,
                  int left,
                  int right) {
  if (left == right) {
    seg_tree[vertex] = Node{leafs[left], leafs[left] * leafs[left], leafs[left] * leafs[left] * leafs[left]};
  } else {
    auto middle = (left + right) / 2;
    BuildSegTree(seg_tree, leafs, (vertex * 2) + 1, left, middle);
    BuildSegTree(seg_tree, leafs, (vertex * 2) + 2, middle + 1, right);
    seg_tree[vertex] = Node{
        seg_tree[(vertex * 2) + 1].sum + seg_tree[(vertex * 2) + 2].sum,
        seg_tree[(vertex * 2) + 1].sum_of_squares + seg_tree[(vertex * 2) + 2].sum_of_squares,
        seg_tree[(vertex * 2) + 1].sum_of_cubes + seg_tree[(vertex * 2) + 2].sum_of_cubes,
    };
  }
}

Node Sum(const std::vector<Node> &seg_tree,
         int vertex,
         int tree_left,
         int tree_right,
         int x,
         int y) {
  if (x > y) {
    return Node{0, 0, 0};
  }
  if (x == tree_left && y == tree_right) {
    return seg_tree[vertex];
  } else {
    auto middle = (tree_left + tree_right) / 2;
    auto left_part = Sum(seg_tree, vertex * 2 + 1, tree_left, middle, x, std::min(y, middle));
    auto right_part = Sum(seg_tree, vertex * 2 + 2, middle + 1, tree_right, std::max(x, middle + 1), y);
    auto node = Node{left_part.sum + right_part.sum, left_part.sum_of_squares + right_part.sum_of_squares,
                     left_part.sum_of_cubes + right_part.sum_of_cubes};

    return node;
  }
}

void Update(std::vector<Node> &seg_tree,
            int vertex,
            int tree_left,
            int tree_right,
            int position,
            int new_value) {
  if (tree_left == tree_right) {
    seg_tree[vertex] = Node{new_value, new_value * new_value, new_value * new_value * new_value};
  } else {
    int middle = (tree_left + tree_right) / 2;
    if (position <= middle) {
      Update(seg_tree, 2 * vertex + 1, tree_left, middle, position, new_value);
    } else {
      Update(seg_tree, 2 * vertex + 2, middle + 1, tree_right, position, new_value);
    }

    seg_tree[vertex].sum = seg_tree[2 * vertex + 1].sum + seg_tree[2 * vertex + 2].sum;
    seg_tree[vertex].sum_of_squares = seg_tree[2 * vertex + 1].sum_of_squares + seg_tree[2 * vertex + 2].sum_of_squares;
    seg_tree[vertex].sum_of_cubes = seg_tree[2 * vertex + 1].sum_of_cubes + seg_tree[2 * vertex + 2].sum_of_cubes;
  }
}

int main() {
  std::vector<int> arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  std::vector<Node> seg_tree(arr.size() * 2);

  BuildSegTree(seg_tree, arr, 0, 0, arr.size() - 1);

  Update(seg_tree, 0, 0, arr.size() - 1, 6, 100);
  arr[6] = 100;

  int l, r;
  std::cin >> l >> r;

  auto sums = Sum(seg_tree, 0, 0, arr.size() - 1, l, r);

  auto just_sum = sums.sum;
  auto sum_of_pair_products = (sums.sum * sums.sum - sums.sum_of_squares) / 2;
  auto sum_of_triplet_products =
      (sums.sum * sums.sum * sums.sum - 3 * sums.sum * sums.sum_of_squares + 2 * sums.sum_of_cubes) / 6;

  std::cout << just_sum << std::endl;
  std::cout << sum_of_pair_products << std::endl;
  std::cout << sum_of_triplet_products << std::endl;

  std::cout << std::endl;

  auto s = 0;
  for (int i = l; i <= r; i++) {
    s += arr[i];
  }
  std::cout << s << std::endl;

  s = 0;
  for (int i = l; i <= r; i++) {
    for (int j = i + 1; j <= r; j++) {
      s += arr[i] * arr[j];
    }
  }
  std::cout << s << std::endl;

  s = 0;
  for (int i = l; i <= r; i++) {
    for (int j = i + 1; j <= r; j++) {
      for (int k = j + 1; k <= r; k++) {
        s += arr[i] * arr[j] * arr[k];
      }
    }
  }
  std::cout << s << std::endl;
}
