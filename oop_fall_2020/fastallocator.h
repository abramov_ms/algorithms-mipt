// В этой задаче вам предлагается научиться пользоваться аллокаторами, а также разобраться с устройством контейнера list
// и понять, что иногда нестандартный аллокатор может давать выигрыш в производительности по сравнению со стандартным.
//
// # Часть 1.
//
// Напишите шаблонный класс FixedAllocator<size_t chunkSize>, занимающийся выделением блоков памяти небольшого
// фиксированного размера. Выделение и освобождение блока должно происходить за амортизированное O(1).
//
// После этого напишите шаблонный класс FastAllocator<typename T>, интерфейс которого аналогичен интерфейсу
// std::allocator. Внутри себя FastAllocator должен решить: либо обратиться к FixedAllocator для очередной аллокации
// фиксированного размера, либо воспользоваться стандартными new/delete. Размеры, для которых надо использовать
// FixedAllocator, можно выбрать самостоятельно в виде констант в коде. (Например, для 4 и 8 байт обращаемся к
// соответствующему FixedAllocator, для других размеров - нет.) Не должно создаваться более одного FixedAllocator’а для
// каждого конкретного размера.
//
// Класс FastAllocator должен удовлетворять требованиям к аллокатору, описанным на странице
// https://en.cppreference.com/w/cpp/named_req/Allocator. Он должен быть STL-совместимым, то есть позволять
// использование в качестве аллокатора для стандартных контейнеров. В частности, должны быть определены:
// - Конструктор по умолчанию, конструктор копирования, деструктор;
// - Методы allocate, deallocate;
// - Метод select_on_container_copy_construction, если логика работы вашего аллокатора этого потребует.
//
// Проверьте себя: напишите тестирующую функцию, которая создает std::list и выполняет над ним последовательность
// случайных добавлений/удалений элементов. Если вы все сделали правильно, то std::list<int, FastAllocator<int>>, скорее
// всего, будет работать быстрее, чем std::list<int, std::allocator<int>>.
//
// # Часть 2.
//
// Напишите класс List - двусвязный список с правильным использованием аллокатора. Правильное использование аллокатора
// означает, что ваш лист должен удовлетворять требованиям
// https://en.cppreference.com/w/cpp/named_req/AllocatorAwareContainer. Должно быть два шаблонных параметра: T - тип
// элементов в листе, Allocator - тип используемого аллокатора (по умолчанию - std::allocator<T>). Внутри List’а
// определите тип Node - элемент List’а. Реализуйте методы:
// - explicit List(const Allocator& alloc = Allocator());
// - List(size_t count, const T& value = T(), const Allocator& alloc = Allocator());
// - Метод get_allocator(), возвращающий объект аллокатора, используемый в листе на данный момент;
// - Конструктор копирования, деструктор, копирующий оператор присваивания;
// - Метод size(), работающий за O(1);
// - Методы push_back, push_front, pop_back, pop_front;
// - Двунаправленные итераторы, удовлетворяющие требованиям
//   https://en.cppreference.com/w/cpp/named_req/BidirectionalIterator. Также поддержите константные и
//   reverse-итераторы;
// - Методы begin, end, cbegin, cend, rbegin, rend, crbegin, crend;
// - Методы insert(iterator, const T&), а также erase(iterator) - для удаления и добавления одиночных элементов в
//   список.
//
// Поддерживать безопасность относительно исключений, а также move-семантику в этой задаче необязательно.
//
// Проверьте себя еще раз: выполните последовательность случайных добавлений-удалений элементов в List<int,
// FastAllocator<int>>. Работает ли это быстрее, чем для List<int, std::allocator<int>>?
//
// Как ваш собственный List, так и std::list должен показывать более высокую производительность с FastAllocator’ом, чем
// со стандартным аллокатором. В контесте это будет проверяться путем замеров времени выполнения большого количества
// однотипных операций над листом. Если ваш аллокатор проиграет по времени стандартному аллокатору, вы не пройдете
// тесты.
//
// В тестах будет использоваться g++-8 с параметром -O2. Чтобы успешно пройти тесты, ваш аллокатор должен быть быстрее
// стандартного хотя бы на 10%. (У меня имеется FastAllocator, дающий выигрыш более 50%, так что получить выигрыш 10%
// очень даже реально.)

#include <memory>

template<size_t ChunkSize>
class FixedAllocator {
private:
    struct Node {
        int8_t* memory_pool;
        size_t pool_size_in_chunks;
        size_t pool_capacity_in_chunks;
        Node* prev_node;

        explicit Node(size_t chunks_count, Node* prev_node = nullptr) :
                memory_pool(new int8_t[ChunkSize * chunks_count]), pool_size_in_chunks(0),
                pool_capacity_in_chunks(chunks_count), prev_node(prev_node) {}
        ~Node() noexcept { delete[] memory_pool, delete prev_node; }
    };

    Node* last_node_;
    size_t chunks_count_;

public:
    FixedAllocator() : last_node_(new Node(1)), chunks_count_(1) {}
    FixedAllocator(const FixedAllocator& other) = delete;

    ~FixedAllocator() noexcept { delete last_node_; }

    void* Allocate(size_t chunks_count);
    void Deallocate(void* pointer, size_t size) noexcept { /* all deallocations happen in ~FixedAllocator() only */ }

    bool operator==(const FixedAllocator& other) noexcept { return last_node_ == other.last_node_; }

private:
    void Grow();
};

template<size_t ChunkSize>
void* FixedAllocator<ChunkSize>::Allocate(size_t chunks_count) {
    if (last_node_->pool_size_in_chunks + chunks_count > last_node_->pool_capacity_in_chunks) {
        Grow();
    }

    size_t prev_size = last_node_->pool_size_in_chunks;
    last_node_->pool_size_in_chunks += chunks_count;

    return last_node_->memory_pool + prev_size * ChunkSize;
}

template<size_t ChunkSize>
void FixedAllocator<ChunkSize>::Grow() {
    auto new_node = new Node(3 * chunks_count_, last_node_);
    last_node_ = new_node;
    chunks_count_ *= 4;
}


template<typename T>
class FastAllocator {
    template<typename U>
    friend class FastAllocator;

private:
    std::shared_ptr<FixedAllocator<24>> fixed_allocator_24_bytes;

public:
    using value_type = T;

    FastAllocator() : fixed_allocator_24_bytes(std::make_shared<FixedAllocator<24>>()) {}
    FastAllocator(const FastAllocator& other) noexcept = default;
    template<typename U>
    explicit FastAllocator(const FastAllocator<U>& other) noexcept :
            fixed_allocator_24_bytes(other.fixed_allocator_24_bytes) {}

    FastAllocator& operator=(const FastAllocator& other);

    T* allocate(size_t count);
    void deallocate(T* pointer, size_t count) noexcept;

    [[nodiscard]] bool operator==(const FastAllocator& other) const noexcept;
    [[nodiscard]] bool operator!=(const FastAllocator& other) const noexcept { return !(*this == other); }
};

template<typename T>
FastAllocator<T>& FastAllocator<T>::operator=(const FastAllocator& other) {
    fixed_allocator_24_bytes = other.fixed_allocator_24_bytes;
    return *this;
}

template<typename T>
T* FastAllocator<T>::allocate(size_t count) {
    size_t size_in_bytes = sizeof(T) * count;
    if (size_in_bytes == 24) {
        return reinterpret_cast<T*>(fixed_allocator_24_bytes->Allocate(1));
    } else {
        return reinterpret_cast<T*>(::operator new(sizeof(T) * count));
    }
}

template<typename T>
void FastAllocator<T>::deallocate(T* pointer, size_t count) noexcept {
    size_t size_in_bytes = sizeof(T) * count;
    if (size_in_bytes != 24) {
        ::operator delete(pointer);
    }
}

template<typename T>
bool FastAllocator<T>::operator==(const FastAllocator<T> &other) const noexcept {
    return fixed_allocator_24_bytes == other.fixed_allocator_24_bytes;
}


template<typename T, typename Allocator = std::allocator<T>>
class List {
private:
    using allocator_traits = std::allocator_traits<Allocator>;

    using RawMemoryAllocator = typename allocator_traits::template rebind_alloc<int8_t>;
    using raw_memory_allocator_traits = std::allocator_traits<RawMemoryAllocator>;

    struct Node;
    using NodeAllocator = typename allocator_traits::template rebind_alloc<Node>;
    using node_allocator_traits = std::allocator_traits<NodeAllocator>;

    template<bool IsConst>
    class BaseIterator;
    template<bool IsConst>
    class Iterator;
    template<bool IsConst>
    class ReverseIterator;

    Allocator allocator_;
    RawMemoryAllocator raw_memory_allocator_;
    NodeAllocator node_allocator_;
    Node* endpoint_;
    size_t size_;

public:
    using allocator_type = Allocator;

    using iterator = Iterator<false>;
    using const_iterator = Iterator<true>;
    using reverse_iterator = ReverseIterator<false>;
    using const_reverse_iterator = ReverseIterator<true>;

    explicit List(const Allocator& allocator = Allocator()) noexcept(noexcept(Allocator()));
    List(size_t count, const T& value, const Allocator& allocator = Allocator());
    List(size_t count, const Allocator& allocator = Allocator());
    List(const List& other);

    List& operator=(const List& other);

    ~List();

    void push_back(const T& value);
    void pop_back() noexcept;
    void push_front(const T& value);
    void pop_front() noexcept;
    template<bool IsConst>
    iterator insert(Iterator<IsConst> position, const T& value);
    template<bool IsConst>
    iterator erase(Iterator<IsConst> position);

    T& back() noexcept { return endpoint_->prev->value; }
    const T& back() const noexcept { return endpoint_->prev->value; }
    T& front() noexcept { return endpoint_->next->value; }
    const T& front() const noexcept { return endpoint_->next->value; }

    [[nodiscard]] size_t size() const noexcept { return size_; }
    [[nodiscard]] bool empty() const noexcept { return size() == 0; }
    Allocator get_allocator() const noexcept { return allocator_; }

    iterator begin() { return iterator(endpoint_->next); }
    const_iterator begin() const { return const_iterator(endpoint_->next); }
    iterator end() { return iterator(endpoint_); }
    const_iterator end() const { return const_iterator(endpoint_); }

    const_iterator cbegin() const { return const_iterator(endpoint_->next); }
    const_iterator cend() const { return const_iterator(endpoint_); }

    reverse_iterator rbegin() { return reverse_iterator(endpoint_->prev); }
    reverse_iterator rend() { return reverse_iterator(endpoint_); }
    const_reverse_iterator rbegin() const { return const_reverse_iterator(endpoint_->prev); }
    const_reverse_iterator rend() const { return const_reverse_iterator(endpoint_); }

    const_reverse_iterator crbegin() const { return const_reverse_iterator(endpoint_->prev); }
    const_reverse_iterator crend() const { return const_reverse_iterator(endpoint_); }

private:
    void ConstructEndpoint();
    void DeleteEndpoint();
};

template<typename T, typename Allocator>
struct List<T, Allocator>::Node {
    T value;
    Node* prev;
    Node* next;

    Node(const T& value, Node* prev, Node* next) : value(value), prev(prev), next(next) {}
    Node(Node* prev, Node* next) : value(T()), prev(prev), next(next) {}
};

template<typename T, typename Allocator>
template<bool IsConst>
class List<T, Allocator>::BaseIterator {
protected:
    Node* node_;

public:
    using value_type = T;
    using iterator_category = std::bidirectional_iterator_tag;
    using difference_type = ptrdiff_t;
    using reference = typename std::conditional<IsConst, const T&, T&>::type;
    using pointer = typename std::conditional<IsConst, const T*, T*>::type;

    BaseIterator(Node* node) : node_(node) {}

    reference operator*() { return node_->value; }
    pointer operator->() { return &node_->value; }

    bool operator==(const BaseIterator& other) { return node_ == other.node_; }
    bool operator!=(const BaseIterator& other) { return !(*this == other); }

    Node* node() { return node_; }
};

template<typename T, typename Allocator>
template<bool IsConst>
class List<T, Allocator>::Iterator : public BaseIterator<IsConst> {
public:
    Iterator(Node* node) : BaseIterator<IsConst>(node) {}
    operator Iterator<true>() { return Iterator<true>(BaseIterator<IsConst>::node_); }

    Iterator& operator++();
    const Iterator operator++(int);
    Iterator& operator--();
    const Iterator operator--(int);
};

template<typename T, typename Allocator>
template<bool IsConst>
typename List<T, Allocator>::template Iterator<IsConst>& List<T, Allocator>::Iterator<IsConst>::operator++() {
    BaseIterator<IsConst>::node_ = BaseIterator<IsConst>::node_->next;
    return *this;
}

template<typename T, typename Allocator>
template<bool IsConst>
const typename List<T, Allocator>::template Iterator<IsConst> List<T, Allocator>::Iterator<IsConst>::operator++(int) {
    auto self_copy = *this;
    ++(*this);
    return self_copy;
}

template<typename T, typename Allocator>
template<bool IsConst>
typename List<T, Allocator>::template Iterator<IsConst>& List<T, Allocator>::Iterator<IsConst>::operator--() {
    BaseIterator<IsConst>::node_ = BaseIterator<IsConst>::node_->prev;
    return *this;
}

template<typename T, typename Allocator>
template<bool IsConst>
const typename List<T, Allocator>::template Iterator<IsConst> List<T, Allocator>::Iterator<IsConst>::operator--(int) {
    auto self_copy = *this;
    --(*this);
    return self_copy;
}

template<typename T, typename Allocator>
template<bool IsConst>
class List<T, Allocator>::ReverseIterator : public BaseIterator<IsConst> {
public:
    ReverseIterator(Node* node) : BaseIterator<IsConst>(node) {}
    operator ReverseIterator<true>() { return ReverseIterator<true>(BaseIterator<IsConst>::node_); }

    iterator base();

    ReverseIterator& operator++();
    const ReverseIterator operator++(int);
    ReverseIterator& operator--();
    const ReverseIterator operator--(int);
};

template<typename T, typename Allocator>
template<bool IsConst>
typename List<T, Allocator>::iterator List<T, Allocator>::template ReverseIterator<IsConst>::base() {
    typename List<T, Allocator>::iterator base(BaseIterator<IsConst>::node_);
    return ++base;
}

template<typename T, typename Allocator>
template<bool IsConst>
typename List<T, Allocator>::template ReverseIterator<IsConst>&
List<T, Allocator>::ReverseIterator<IsConst>::operator++() {
    BaseIterator<IsConst>::node_ = BaseIterator<IsConst>::node_->prev;
    return *this;
}

template<typename T, typename Allocator>
template<bool IsConst>
const typename List<T, Allocator>::template ReverseIterator<IsConst>
List<T, Allocator>::ReverseIterator<IsConst>::operator++(int) {
    auto self_copy = *this;
    ++(*this);
    return self_copy;
}

template<typename T, typename Allocator>
template<bool IsConst>
typename List<T, Allocator>::template ReverseIterator<IsConst>&
List<T, Allocator>::ReverseIterator<IsConst>::operator--() {
    BaseIterator<IsConst>::node_ = BaseIterator<IsConst>::node_->next;
    return *this;
}

template<typename T, typename Allocator>
template<bool IsConst>
const typename List<T, Allocator>::template ReverseIterator<IsConst>
List<T, Allocator>::ReverseIterator<IsConst>::operator--(int) {
    auto self_copy = *this;
    --(*this);
    return self_copy;
}

template<typename T, typename Allocator>
List<T, Allocator>::List(const Allocator& allocator) noexcept(noexcept(Allocator())) :
        allocator_(allocator_traits::select_on_container_copy_construction(allocator)),
        raw_memory_allocator_(allocator_traits::select_on_container_copy_construction(allocator)),
        node_allocator_(allocator_traits::select_on_container_copy_construction(allocator)), size_(0) {
    ConstructEndpoint();
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const T &value, const Allocator& allocator) :
        allocator_(allocator_traits::select_on_container_copy_construction(allocator)),
        raw_memory_allocator_(allocator_traits::select_on_container_copy_construction(allocator)),
        node_allocator_(allocator_traits::select_on_container_copy_construction(allocator)), size_(0) {
    ConstructEndpoint();
    for (size_t pushed = 0; pushed < count; ++pushed) {
        push_back(value);
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const Allocator& allocator) :
        allocator_(allocator_traits::select_on_container_copy_construction(allocator)),
        raw_memory_allocator_(allocator_traits::select_on_container_copy_construction(allocator)),
        node_allocator_(allocator_traits::select_on_container_copy_construction(allocator)), size_(0) {
    ConstructEndpoint();
    for (size_t element = 0; element < count; ++element) {
        auto new_node = node_allocator_traits::allocate(node_allocator_, 1);
        node_allocator_traits::construct(node_allocator_, new_node, endpoint_->prev, endpoint_);
        endpoint_->prev->next = new_node;
        endpoint_->prev = new_node;
        ++size_;
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(const List<T, Allocator>& other) :
        allocator_(allocator_traits::select_on_container_copy_construction(other.allocator_)),
        raw_memory_allocator_(allocator_traits::select_on_container_copy_construction(other.allocator_)),
        node_allocator_(allocator_traits::select_on_container_copy_construction(other.allocator_)), size_(0) {
    ConstructEndpoint();
    for (const auto& element : other) {
        push_back(element);
    }
}

template<typename T, typename Allocator>
List<T, Allocator>& List<T, Allocator>::operator=(const List& other) {
    while (!empty()) {
        pop_back();
    }
    DeleteEndpoint();

    if (allocator_traits::propagate_on_container_copy_assignment::value) {
        allocator_ = other.allocator_;
        raw_memory_allocator_ = other.raw_memory_allocator_;
        node_allocator_ = other.node_allocator_;
    }

    ConstructEndpoint();
    for (const auto& element : other) {
        push_back(element);
    }

    return *this;
}

template<typename T, typename Allocator>
List<T, Allocator>::~List() {
    for (auto node = endpoint_->next, next = node->next; node != endpoint_; node = next, next = node->next) {
        node_allocator_traits::destroy(node_allocator_, node);
        node_allocator_traits::deallocate(node_allocator_, node, 1);
    }
    DeleteEndpoint();
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_back(const T& value) {
    auto new_node = node_allocator_traits::allocate(node_allocator_, 1);
    node_allocator_traits::construct(node_allocator_, new_node, value, endpoint_->prev, endpoint_);
    endpoint_->prev->next = new_node;
    endpoint_->prev = new_node;
    ++size_;
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_back() noexcept {
    auto node_to_delete = endpoint_->prev;
    endpoint_->prev = node_to_delete->prev;
    endpoint_->prev->next = endpoint_;
    node_allocator_traits::destroy(node_allocator_, node_to_delete);
    node_allocator_traits::deallocate(node_allocator_, node_to_delete, 1);
    --size_;
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_front(const T &value) {
    auto new_node = node_allocator_traits::allocate(node_allocator_, 1);
    node_allocator_traits::construct(node_allocator_, new_node, value, endpoint_, endpoint_->next);
    endpoint_->next->prev = new_node;
    endpoint_->next = new_node;
    ++size_;
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_front() noexcept {
    auto node_to_delete = endpoint_->next;
    endpoint_->next = node_to_delete->next;
    endpoint_->next->prev = endpoint_;
    node_allocator_traits::destroy(node_allocator_, node_to_delete);
    node_allocator_traits::deallocate(node_allocator_, node_to_delete, 1);
    --size_;
}

template<typename T, typename Allocator>
template<bool IsConst>
typename List<T, Allocator>::iterator List<T, Allocator>::insert(Iterator<IsConst> position, const T &value) {
    auto prev_position = position;
    --prev_position;

    auto new_node = node_allocator_traits::allocate(node_allocator_, 1);
    node_allocator_traits::construct(node_allocator_, new_node, value, prev_position.node(), position.node());

    new_node->prev->next = new_node;
    new_node->next->prev = new_node;
    ++size_;

    return iterator(new_node);
}

template<typename T, typename Allocator>
template<bool IsConst>
typename List<T, Allocator>::iterator List<T, Allocator>::erase(Iterator<IsConst> position) {
    auto next_position = position;
    ++next_position;
    auto prev_position = position;
    --prev_position;

    node_allocator_traits::destroy(node_allocator_, position.node());
    node_allocator_traits::deallocate(node_allocator_, position.node(), 1);

    prev_position.node()->next = next_position.node();
    next_position.node()->prev = prev_position.node();
    --size_;

    return iterator(next_position.node());
}

template<typename T, typename Allocator>
void List<T, Allocator>::ConstructEndpoint() {
    endpoint_ = reinterpret_cast<Node*>(raw_memory_allocator_traits::allocate(raw_memory_allocator_, sizeof(Node)));
    endpoint_->prev = endpoint_;
    endpoint_->next = endpoint_;
}

template<typename T, typename Allocator>
void List<T, Allocator>::DeleteEndpoint() {
    auto real_endpoint_memory = reinterpret_cast<int8_t*>(endpoint_);
    raw_memory_allocator_traits::deallocate(raw_memory_allocator_, real_endpoint_memory, sizeof(Node));
}
