// Напишите иерархию классов для работы с геометрическими фигурами на плоскости.
//
// + Структура Point - точка на плоскости. Точку можно задать двумя числами типа double. Должны быть открыты поля x и y.
//   Точки можно сравнивать операторами == и !=.
// + Класс Line - прямая. Прямую можно задать двумя точками, можно двумя числами (угловой коэффициент и сдвиг), можно
//   точкой и числом (угловой коэффициент). Линии можно сравнивать операторами == и !=.
// + Абстрактный класс Shape - фигура.
// + Класс Polygon - многоугольник. Многоугольник - частный случай фигуры. У многоугольника можно спросить
//   verticesCount() - количество вершин - и std::vector<Point> getVertices - сами вершины без возможности изменения.
//   Можно спросить isConvex() - выпуклый ли. Можно сконструировать многоугольник из вектора точек-вершин в порядке
//   обхода. Можно сконструировать многоугольник из точек, передаваемых в качестве параметров через запятую
//   (т.е. неуказанное число аргументов). Для простоты будем считать, что многоугольники с самопересечениями никогда не
//   возникают (гарантируется, что в тестах таковые будут отсутствовать). Кроме того, считаем, что три последовательных
//   вершины многоугольника никогда не лежат на одной прямой.
// + Класс Ellipse - эллипс. Эллипс - частный случай фигуры. У эллипса можно спросить std::pair<Point,Point> focuses() -
//   его фокусы; std::pair<Line, Line> directrices() - пару его директрис; double eccentricity() - его эксцентриситет,
//   Point center() - его центр. Эллипс можно сконструировать из двух точек и double (два фокуса и сумма расстояний от
//   эллипса до них);
// + Класс Circle - круг. Круг - частный случай эллипса. У круга можно спросить double radius() - радиус. Круг можно
//   задать точкой и числом (центр и радиус).
// + Класс Rectangle - прямоугольник. Прямоугольник - частный случай многоугольника. У прямоугольника можно спросить
//   Point center() - его центр; std::pair<Line, Line> diagonals() - пару его диагоналей. Прямоугольник можно
//   сконструировать по двум точкам (его противоположным вершинам) и числу (отношению смежных сторон), причем из двух
//   таких прямоугольников выбирается тот, у которого более короткая сторона расположена по левую сторону от диагонали,
//   если смотреть от первой заданной точки в направлении второй.
// + Класс Square - квадрат. Квадрат - частный случай прямоугольника. У квадрата можно спросить Circle
//   circumscribedCircle(), Circle inscribedCircle(). Квадрат можно задать двумя точками - противоположными вершинами.
// + Класс Triangle - треугольник. Треугольник - частный случай многоугольника. У треугольника можно спросить Circle
//   circumscribedCircle(), Circle inscribedCircle(), Point centroid() - его центр масс, Point orthocenter() - его
//   ортоцентр, Line EulerLine() - его прямую Эйлера, Circle ninePointsCircle() - его окружность Эйлера.
//
// У любой фигуры можно спросить:
// + double perimeter() - периметр;
// + double area() - площадь;
// + operator==(const Shape& another) - совпадает ли эта фигура с другой как множество точек. (В частности, треугольник
//   ABC равен треугольнику BCA.)
// + isCongruentTo(const Shape& another) - равна ли эта фигура другой в геометрическом смысле, то есть можно ли
//   совместить эти фигуры движением плоскости. Движение – это отображение плоскости на себя, сохраняющее расстояния.
// + isSimilarTo(const Shape& another) - подобна ли эта фигура другой, то есть можно ли перевести одну фигуру в другую
//   преобразованием подобия. (Определение преобразования подобия, кто не знает, можно посмотреть в Википедии.)
// + containsPoint(Point point) - находится ли точка внутри фигуры.
// Фигуры не обязаны иметь одинаковый тип, чтобы считаться равными, конгруэнтными или подобными! Любую фигуру должно
// быть можно сравнить с любой другой и получить правильный ответ, независимо от настоящих типов этих фигур!
//
// С любой фигурой можно сделать:
// + rotate(Point center, double angle) - поворот на угол (в градусах, против часовой стрелки) относительно точки;
// + reflex(Point center) - симметрию относительно точки;
// + reflex(Line axis) - симметрию относительно прямой;
// + scale(Point center, double coefficient) - гомотетию с коэффициентом coefficient и центром center.
//
// В вашем файле должна отсутствовать функция main(), а сам файл должен называться geometry.h. В качестве компилятора
// необходимо выбирать Make. Ваш код будет вставлен посредством #include в программу, содержащую тесты.

#pragma once

#include <vector>
#include <cmath>
#include <cstdarg>
#include <set>
#include <iostream>
#include <algorithm>

const double kEpsilon = 1e-6;

bool doublesEqual(double lhs, double rhs) { return (std::abs(lhs - rhs) < kEpsilon); }
bool doubleLess(double lhs, double rhs) { return (lhs < (rhs + kEpsilon)); }

class Line;

struct Point {
  Point(double x, double y) : x(x), y(y) {}
  Point(const Point &) = default;

  static Point midpoint(const Point &start, const Point &end);

  void rotate(const Point &, double);
  void reflex(const Point &);
  void reflex(const Line &);
  void scale(const Point &, double);

  bool operator<(const Point &) const;

  bool operator==(const Point &other) const { return (doublesEqual(x, other.x) && doublesEqual(y, other.y)); }
  bool operator!=(const Point &other) const { return !(*this == other); }

  double x;
  double y;
};

Point Point::midpoint(const Point &start, const Point &end) {
  return Point((start.x + end.x) / 2, (start.y + end.y) / 2);
}

void Point::rotate(const Point &origin, double angle) {
  angle = angle / 180 * M_PI;

  auto new_x = origin.x + cos(angle) * (x - origin.x) - sin(angle) * (y - origin.y);
  auto new_y = origin.y + cos(angle) * (y - origin.y) + sin(angle) * (x - origin.x);

  x = new_x;
  y = new_y;
}

void Point::reflex(const Point &origin) {
  auto dx = origin.x - x;
  auto dy = origin.y - y;

  x += 2 * dx;
  y += 2 * dy;
}

void Point::scale(const Point &origin, double coefficient) {
  auto dx = x - origin.x;
  auto dy = y - origin.y;

  dx *= coefficient;
  dy *= coefficient;

  x = origin.x + dx;
  y = origin.y + dy;
}

bool Point::operator<(const Point &other) const {
  return !doublesEqual(x, other.x) ? doubleLess(x, other.x) : doubleLess(y, other.y);
}

struct Vector {
  Vector(double x, double y) : x(x), y(y) {}
  explicit Vector(const Point &radius) : x(radius.x), y(radius.y) {}
  Vector(const Point &start, const Point &end) : x(end.x - start.x), y(end.y - start.y) {}

  static double cross(const Vector &lhs, const Vector &rhs) { return ((lhs.x * rhs.y) - (rhs.x * lhs.y)); }
  static double dot(const Vector &lhs, const Vector &rhs) { return ((lhs.x * rhs.x) + (lhs.y * rhs.y)); }
  static double cos(const Vector &lhs, const Vector &rhs);

  Point asPoint() const { return Point(x, y); }

  void rotate(const Point &, double);
  void reflex(const Point &);
  void reflex(const Line &);
  void scale(const Point &, double);

  double length() const { return std::sqrt((x * x) + (y * y)); }

  bool operator==(const Vector &other) const { return doublesEqual(x * other.y, other.x * y); }
  bool operator!=(const Vector &other) const { return !(*this == other); }

  Vector operator-() const { return Vector(-x, -y); }
  Vector &operator*=(double);
  Vector &operator+=(const Vector &);
  Vector &operator-=(const Vector &other) { return (*this += -other); }

  double x;
  double y;
};

double Vector::cos(const Vector &lhs, const Vector &rhs) {
  return (lhs.x * rhs.x + lhs.y + rhs.y) / (lhs.length() * rhs.length());
}

void Vector::rotate(const Point &origin, double angle) {
  auto start = Point(0, 0);
  auto end = Point(x, y);

  start.rotate(origin, angle);
  end.rotate(origin, angle);

  x = end.x - start.x;
  y = end.y - start.y;
}

void Vector::reflex(const Point &origin) {
  auto start = Point(0, 0);
  auto end = Point(x, y);

  start.reflex(origin);
  end.reflex(origin);

  x = end.x - start.x;
  y = end.y - start.y;
}

void Vector::reflex(const Line &axis) {
  auto start = Point(0, 0);
  auto end = Point(x, y);

  start.reflex(axis);
  end.reflex(axis);

  x = end.x - start.x;
  y = end.y - start.y;
}

void Vector::scale(const Point &origin, double coefficient) {
  auto start = Point(0, 0);
  auto end = Point(x, y);

  start.scale(origin, coefficient);
  end.scale(origin, coefficient);

  x = end.x - start.x;
  y = end.y - start.y;
}

Vector &Vector::operator*=(double coefficient) {
  x *= coefficient;
  y *= coefficient;

  return *this;
}

Vector &Vector::operator+=(const Vector &other) {
  x += other.x;
  y += other.y;

  return *this;
}

Vector operator*(const Vector &subject, double coefficient) {
  auto copy = subject;
  copy *= coefficient;

  return copy;
}

Vector operator+(const Vector &lhs, const Vector &rhs) {
  auto copy = lhs;
  copy += rhs;

  return copy;
}

Vector operator-(const Vector &lhs, const Vector &rhs) {
  auto copy = lhs;
  copy -= rhs;

  return copy;
}

class Line {
 public:
  static Point intersect(const Line &, const Line &);

  Line(const Point &start, const Vector &direction) : start_(start), direction_(direction) {}
  Line(const Point &start, const Point &end) : start_(start), direction_(start, end) {}
  Line(double skew, double offset) : start_(Point(0, offset)), direction_(start_, Point(1, offset + skew)) {}
  Line(const Point &start, double skew) : start_(start), direction_(start, Point(start.x + 1, start.y + skew)) {}

  bool operator==(const Line &);
  bool operator!=(const Line &other) { return !(*this == other); }

  Vector norm() const { return Vector(direction_.y, -direction_.x); }
  Point start() const { return start_; }

 private:
  Point start_;
  Vector direction_;
};

void Point::reflex(const Line &axis) {
  auto norm = axis.norm();
  auto start = Vector(axis.start());

  auto radius = Vector(*this);
  auto coefficient = 2 * (Vector::dot(start - radius, norm) / Vector::dot(norm, norm));

  auto reflection = radius + norm * coefficient;
  x = reflection.x;
  y = reflection.y;
}

bool Line::operator==(const Line &other) {
  return (direction_ == other.direction_) && (direction_ == Vector(start_, other.start_));
}

Point Line::intersect(const Line &lhs, const Line &rhs) {
  auto a = lhs.direction_.y;
  auto b = lhs.direction_.x;
  auto c = lhs.start_.x;
  auto d = lhs.start_.y;

  auto p = rhs.direction_.y;
  auto q = rhs.direction_.x;
  auto r = rhs.start_.x;
  auto s = rhs.start_.y;

  auto intersection_x = (b * (p * r - q * s + q * d) - a * q * c) / (p * b - a * q);
  auto intersection_y = (p * b * d - a * (-p * r + q * s + p * c)) / (p * b - a * q);

  return Point(intersection_x, intersection_y);
}

class Shape {
 public:
  virtual double perimeter() const = 0;
  virtual double area() const = 0;
  virtual bool isSimilarTo(const Shape &) const = 0;
  virtual bool isCongruentTo(const Shape &) const = 0;
  virtual bool containsPoint(const Point &) const = 0;

  virtual void rotate(const Point &, double) = 0;
  virtual void reflex(const Point &) = 0;
  virtual void reflex(const Line &) = 0;
  virtual void scale(const Point &, double) = 0;

  virtual bool operator==(const Shape &) const = 0;
  bool operator!=(const Shape &other) const { return !(*this == other); }

  virtual ~Shape() = default;
};

class Polygon : public Shape {
 public:
  explicit Polygon(std::vector<Point> vertices) : vertices(std::move(vertices)) {}
  //Polygon(const std::initializer_list<Point> &vertices = {}) : vertices(vertices) {}
  Polygon(std::initializer_list<Point> vertices) : vertices(vertices) {}

  size_t verticesCount() const { return vertices.size(); }
  bool isConvex();

  double similarityCoefficient(const Shape &) const;

  virtual double perimeter() const override;
  virtual double area() const override;
  virtual bool isSimilarTo(const Shape &) const override;
  virtual bool isCongruentTo(const Shape &) const override;
  virtual bool containsPoint(const Point &) const override;

  virtual void rotate(const Point &, double) override;
  virtual void reflex(const Line &) override;
  virtual void reflex(const Point &) override;
  virtual void scale(const Point &, double) override;

  virtual bool operator==(const Shape &) const override;

  std::vector<Point> getVertices() const { return vertices; }

 protected:
  std::vector<Point> vertices;
};

bool Polygon::isConvex() {
  if (verticesCount() <= 2) {
    return true;
  }

  auto a = vertices[0];
  auto b = vertices[1];
  auto c = vertices[2];
  auto prev_product = Vector::cross(Vector(a, b), Vector(b, c));

  for (size_t i = 1; i < verticesCount(); i++) {
    auto x = vertices[i % verticesCount()];
    auto y = vertices[(i + 1) % verticesCount()];
    auto z = vertices[(i + 2) % verticesCount()];

    auto product = Vector::cross(Vector(x, y), Vector(y, z));
    if (doubleLess(product * prev_product, 0)) {
      return false;
    }

    prev_product = product;
  }

  return true;
}

double Polygon::perimeter() const {
  double perimeter = 0;
  for (size_t i = 1; i <= verticesCount(); i++) {
    Vector side(vertices[(i - 1) % verticesCount()], vertices[i % verticesCount()]);
    perimeter += side.length();
  }

  return perimeter;
}

double Polygon::area() const {
  double area = 0;
  for (size_t i = 1; i <= verticesCount(); i++) {
    Vector prev_vector(vertices[(i - 1) % verticesCount()]);
    Vector current_vector(vertices[i % verticesCount()]);

    area += Vector::cross(prev_vector, current_vector);
  }
  area /= 2;

  return std::abs(area);
}

double Polygon::similarityCoefficient(const Shape &other) const {
  auto other_polygon = dynamic_cast<const Polygon *>(&other);
  if (other_polygon == nullptr) {
    return -1;
  }

  if (verticesCount() != other_polygon->verticesCount()) {
    return -1;
  }

  std::multiset<double> distances, other_distances;
  for (size_t i = 0; i < verticesCount(); i++) {
    for (size_t j = i + 1; j < verticesCount(); j++) {
      auto diagonal = Vector(vertices[i], vertices[j]);
      distances.insert(diagonal.length());
      auto other_diagonal = Vector(other_polygon->vertices[i], other_polygon->vertices[j]);
      other_distances.insert(other_diagonal.length());
    }
  }

  double similarity_coefficient = *distances.begin() / *other_distances.begin();
  for (auto i = distances.begin(), j = other_distances.begin(); i != distances.end(); i++, j++) {
    if (!doublesEqual(*i / *j, similarity_coefficient)) {
      return -1;
    }
  }

  return similarity_coefficient;
}

bool Polygon::isSimilarTo(const Shape &other) const {
  return (similarityCoefficient(other) != -1);
}

bool Polygon::isCongruentTo(const Shape &other) const {
  return doublesEqual(similarityCoefficient(other), 1);
}

bool Polygon::containsPoint(const Point &point) const {
  int windings_number = 0;
  for (size_t i = 0; i < verticesCount(); i++) {
    if (point == vertices[i]) {
      return true;
    }

    auto next_index = (i + 1) % verticesCount();
    auto &current_vertex = vertices[i];
    auto &next_vertex = vertices[next_index];

    if ((point.y == current_vertex.y) && (point.y == next_vertex.y)) {
      if ((std::min(current_vertex.x, next_vertex.x) <= point.x)
          && point.x <= std::max(current_vertex.x, next_vertex.x)) {
        return true;
      }
    } else {
      auto is_current_below = (current_vertex.y < point.y);
      auto is_next_below = (next_vertex.y < point.y);

      if (is_current_below != is_next_below) {
        auto current_vector = Vector(point, current_vertex);
        auto next_vector = Vector(point, next_vertex);
        auto cross_product = Vector::cross(current_vector, next_vector);

        auto is_counterclockwise = !doubleLess(cross_product, 0);
        if (is_current_below == is_counterclockwise) {
          windings_number += is_current_below ? 1 : -1;
        }
      }
    }
  }

  return (windings_number != 0);
}

void Polygon::rotate(const Point &origin, double angle) {
  for (auto &vertex : vertices) {
    vertex.rotate(origin, angle);
  }
}

void Polygon::reflex(const Line &axis) {
  for (auto &vertex : vertices) {
    vertex.reflex(axis);
  }
}

void Polygon::reflex(const Point &origin) {
  for (auto &vertex : vertices) {
    vertex.reflex(origin);
  }
}

void Polygon::scale(const Point &origin, double coefficient) {
  for (auto &vertex : vertices) {
    vertex.scale(origin, coefficient);
  }
}

bool Polygon::operator==(const Shape &other) const {
  auto other_polygon = dynamic_cast<const Polygon *>(&other);
  if (other_polygon == nullptr) {
    return false;
  }

  if (other_polygon->verticesCount() != verticesCount()) {
    return false;
  }

  auto vertices_set = std::set<Point>(vertices.begin(), vertices.end());
  auto other_vertices_set = std::set<Point>(other_polygon->vertices.begin(), other_polygon->vertices.end());

  return (vertices_set == other_vertices_set);
}

class Rectangle : public Polygon {
 public:
  Rectangle(const Point &, const Point &, double);

  Point center() const { return Point::midpoint(vertices[0], vertices[2]); }
  std::pair<Line, Line> diagonals() const;
};

Rectangle::Rectangle(const Point &top_left, const Point &bottom_right, double tan) : Polygon({}) {
  auto midpoint = Point::midpoint(top_left, bottom_right);

  if (tan > 1) {
    tan = 1 / tan;
  }

  auto angle = -2 * std::atan(tan);

  auto top_left_image = top_left;
  top_left_image.rotate(midpoint, angle);
  auto bottom_right_image = bottom_right;
  bottom_right_image.rotate(midpoint, angle);

  vertices = std::vector<Point>{top_left, top_left_image, bottom_right, bottom_right_image};
}

std::pair<Line, Line> Rectangle::diagonals() const {
  auto top_left_bottom_right = Line(vertices[0], vertices[2]);
  auto top_right_bottom_left = Line(vertices[1], vertices[3]);

  auto diagonals = std::make_pair(top_left_bottom_right, top_right_bottom_left);

  return diagonals;
}

class Ellipse : public Shape {
 public:
  Ellipse(const Point &focus_1, const Point &focus_2, double radius)
      : focus_1_(focus_1),
        focus_2_(focus_2),
        a_(radius / 2),
        c_(Vector(focus_1, focus_2).length() / 2),
        b_(std::sqrt((a_ * a_) - (c_ * c_))) {};

  std::pair<Line, Line> directrices() const;
  double eccentricity() const { return c_ / a_; }
  Point center() const { return Point::midpoint(focus_1_, focus_2_); }

  virtual double perimeter() const override;
  virtual double area() const override;
  virtual bool isSimilarTo(const Shape &) const override;
  virtual bool isCongruentTo(const Shape &) const override;
  virtual bool containsPoint(const Point &) const override;

  virtual void rotate(const Point &, double) override;
  virtual void reflex(const Point &) override;
  virtual void reflex(const Line &) override;
  virtual void scale(const Point &, double) override;

  virtual bool operator==(const Shape &) const override;

  double radius() const { return a_; }

 private:
  Point focus_1_;
  Point focus_2_;
  double a_;
  double c_;
  double b_;
};

double Ellipse::perimeter() const {
  auto perimeter = 4 * a_ * std::comp_ellint_2(eccentricity());

  return perimeter;
}

double Ellipse::area() const {
  return M_PI * a_ * b_;
}

bool Ellipse::isSimilarTo(const Shape &other) const {
  auto other_ellipse = dynamic_cast<const Ellipse *>(&other);
  if (other_ellipse == nullptr) {
    return false;
  }

  return (Vector(a_, b_) == Vector(other_ellipse->a_, other_ellipse->b_));
}

bool Ellipse::isCongruentTo(const Shape &other) const {
  auto other_ellipse = dynamic_cast<const Ellipse *>(&other);
  if (other_ellipse == nullptr) {
    return false;
  }

  return isSimilarTo(*other_ellipse) && doublesEqual(a_ / other_ellipse->a_, 1);
}

bool Ellipse::containsPoint(const Point &point) const {
  return !doubleLess(2 * a_, Vector(focus_1_, point).length() + Vector(focus_2_, point).length());
}

void Ellipse::rotate(const Point &origin, double angle) {
  focus_1_.rotate(origin, angle);
  focus_2_.rotate(origin, angle);
}

void Ellipse::reflex(const Line &axis) {
  focus_1_.reflex(axis);
  focus_2_.reflex(axis);
}

void Ellipse::reflex(const Point &origin) {
  focus_1_.reflex(origin);
  focus_2_.reflex(origin);
}

void Ellipse::scale(const Point &origin, double coefficient) {
  focus_1_.scale(origin, coefficient);
  focus_2_.scale(origin, coefficient);

  a_ *= coefficient;
  b_ *= coefficient;
  c_ *= coefficient;
}

bool Ellipse::operator==(const Shape &other) const {
  auto other_ellipse = dynamic_cast<const Ellipse *>(&other);
  if (other_ellipse == nullptr) {
    return false;
  }

  auto focuses = std::multiset<Point>{focus_1_, focus_2_};
  auto other_focuses = std::multiset<Point>{other_ellipse->focus_1_, other_ellipse->focus_2_};

  return isCongruentTo(other) && (focuses == other_focuses);
}

std::pair<Line, Line> Ellipse::directrices() const {
  auto df = Vector(focus_1_, focus_2_);
  df.scale(Point(0, 0), (eccentricity() * eccentricity()) / 2);
  auto midpoint_vector = Vector(center());

  auto start_1 = midpoint_vector + df;
  auto start_2 = midpoint_vector - df;
  auto direction = Vector(df.y, -df.x);

  auto directrice_1 = Line(start_1.asPoint(), direction);
  auto directrice_2 = Line(start_2.asPoint(), direction);

  return std::make_pair(directrice_1, directrice_2);
}

class Circle : public Ellipse {
 public:
  Circle(const Point &center, double radius) : Ellipse(center, center, 2 * radius) {}
};

class Square : public Rectangle {
 public:
  Square(const Point &top_left, const Point &bottom_right) : Rectangle(top_left, bottom_right, 1) {}

  Circle inscribedCircle() const;
  Circle circumscribedCircle() const;
};

Circle Square::inscribedCircle() const {
  double radius = Vector(vertices[0], vertices[1]).length() / 2;
  Point midpoint = center();

  auto circle = Circle(midpoint, radius);

  return circle;
}

Circle Square::circumscribedCircle() const {
  double radius = Vector(vertices[0], vertices[2]).length() / 2;
  Point midpoint = center();

  auto circle = Circle(midpoint, radius);

  return circle;
}

class Triangle : public Polygon {
 public:
  Triangle(const Point &a, const Point &b, const Point &c) : Polygon({a, b, c}) {}
  Triangle(std::initializer_list<Point> vertices) : Polygon(vertices) {}

  Line EulerLine() const { return Line(circumscribedCircle().center(), orthocenter()); }

  Point centroid() const;
  Point orthocenter() const;
  Circle inscribedCircle() const;
  Circle circumscribedCircle() const;
  Circle ninePointsCircle() const;
};

Point Triangle::centroid() const {
  auto x = (vertices[0].x + vertices[1].x + vertices[2].x) / 3;
  auto y = (vertices[0].y + vertices[1].y + vertices[2].y) / 3;

  return Point(x, y);
}

Point Triangle::orthocenter() const {
  auto side_1 = Vector(vertices[0], vertices[1]);
  auto side_1_norm = Vector(side_1.y, -side_1.x);
  auto height_0_1 = Line(vertices[2], side_1_norm);

  auto side_2 = Vector(vertices[0], vertices[2]);
  auto side_2_norm = Vector(side_2.y, -side_2.x);
  auto height_0_2 = Line(vertices[1], side_2_norm);

  return Line::intersect(height_0_1, height_0_2);
}

Circle Triangle::inscribedCircle() const {
  auto side_1 = Vector(vertices[0], vertices[1]);
  side_1 *= 1 / side_1.length();
  auto side_2 = Vector(vertices[0], vertices[2]);
  side_2 *= 1 / side_2.length();
  auto side_3 = Vector(vertices[1], vertices[2]);
  side_3 *= 1 / side_3.length();

  auto bisector_1_direction = side_1 + side_2;
  auto bisector_2_direction = -side_1 + side_3;

  auto bisector_1 = Line(vertices[0], bisector_1_direction);
  auto bisector_2 = Line(vertices[1], bisector_2_direction);

  auto inscribed_circle_center = Line::intersect(bisector_1, bisector_2);
  auto inscribed_circle_radius = 2 * area() / perimeter();

  return Circle(inscribed_circle_center, inscribed_circle_radius);
}

Circle Triangle::circumscribedCircle() const {
  auto side_1 = Vector(vertices[0], vertices[1]);
  auto side_1_midpoint = Point::midpoint(vertices[0], vertices[1]);
  auto side_1_norm = Vector(side_1.y, -side_1.x);
  auto side1_center_perpendicular = Line(side_1_midpoint, side_1_norm);

  auto side_2 = Vector(vertices[0], vertices[2]);
  auto side_2_midpoint = Point::midpoint(vertices[0], vertices[2]);
  auto side_2_norm = Vector(side_2.y, -side_2.x);
  auto side_2_center_perpendicular = Line(side_2_midpoint, side_2_norm);

  auto side_3 = Vector(vertices[1], vertices[2]);

  auto circumscribed_circle_center = Line::intersect(side1_center_perpendicular, side_2_center_perpendicular);
  auto circumscribed_circle_radius = (side_1.length() * side_2.length() * side_3.length()) / (4 * area());

  return Circle(circumscribed_circle_center, circumscribed_circle_radius);
}

Circle Triangle::ninePointsCircle() const {
  auto side_1_midpoint = Point::midpoint(vertices[0], vertices[1]);
  auto side_2_midpoint = Point::midpoint(vertices[0], vertices[2]);
  auto side_3_midpoint = Point::midpoint(vertices[1], vertices[2]);

  Triangle midpoints_triangle{side_1_midpoint, side_2_midpoint, side_3_midpoint};

  return midpoints_triangle.circumscribedCircle();
}
