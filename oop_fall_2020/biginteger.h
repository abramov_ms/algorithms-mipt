// Напишите класс BigInteger для работы с длинными целыми числами. Должны поддерживаться операции:
// + сложение, вычитание, умножение, деление, остаток по модулю, работающие так же, как и для int; составное
//   присваивание с этими операциями. Умножение должно работать за o-малое от n^2 (нет). Деление должно работать не
//   дольше, чем за O(n^2).
// + унарный минус, префиксный и постфиксный инкремент и декремент. Префиксный инкремент и декремент должны работать за
//   O(1) в среднем.
// + операторы сравнения == != < > <= >=
// + вывод в поток и ввод из потока
// + метод toString(), возвращающий строковое представление числа
// + конструирование из int (в том числе неявное преобразование, когда это надо)
// + неявное преобразование в bool, когда это надо (должно работать в условных выражениях)
// + опционально - литеральный суффикс bi для написания литералов типа BigInteger, см. справку здесь
//   https://en.cppreference.com/w/cpp/language/user_literal
//
// Используя класс BigInteger, напишите класс Rational для работы с рациональными числами сколь угодно высокой точности.
// Числа Rational должны представляться в виде несократимых обыкновенных дробей, где числитель и знаменатель – сколь
// угодно длинные целые числа. Должны поддерживаться операции:
// + конструктор из BigInteger, из int
// + сложение, вычитание, умножение, деление, составное присваивание с этими операциями
// + унарный минус
// + операторы сравнения == != < > <= >=
// + метод toString(), возвращающий строковое представление числа (вида [минус]числитель/знаменатель), где числитель и
//   знаменатель - взаимно простые числа; если число на самом деле целое, то знаменатель выводить не надо
// + метод asDecimal(size_t precision = 0), возвращающий строковое представление числа в виде десятичной дроби с
//   precision знаками после запятой
// + оператор приведения к double
//
// В вашем файле должна отсутствовать функция main(), а сам файл должен называться biginteger.h. В качестве компилятора
// необходимо указывать GCC C++17 Make. Ваш код будет вставлен посредством  в программу, содержащую тесты; вследствие
// этого код необходимо отправлять в файле со строго соответствующим именем!

#include <iostream>
#include <vector>
#include <string>

class BigInteger {
 public:
  BigInteger() : digits_({0}), sign_(kPositive) {}
  BigInteger(int);
  BigInteger(const BigInteger &) = default;
  BigInteger(BigInteger &&other) noexcept: digits_(std::move(other.digits_)), sign_(other.sign_) {}

  static BigInteger Abs(const BigInteger &);
  static std::string DivideAsDecimal(const BigInteger &, const BigInteger &, size_t);

  explicit operator bool() const { return !IsZero(); }

  bool operator<(const BigInteger &) const;
  bool operator>(const BigInteger &other) const { return (other < *this); }
  bool operator<=(const BigInteger &other) const { return !(*this > other); }
  bool operator>=(const BigInteger &other) const { return !(*this < other); }
  bool operator==(const BigInteger &) const;
  bool operator!=(const BigInteger &other) const { return !(*this == other); }

  BigInteger &operator=(const BigInteger &other) = default;

  BigInteger operator-() const;
  BigInteger &operator++();
  const BigInteger operator++(int);
  BigInteger &operator--();
  const BigInteger operator--(int);

  BigInteger &operator+=(const BigInteger &);
  BigInteger &operator-=(const BigInteger &);
  BigInteger &operator*=(const BigInteger &);
  BigInteger &operator/=(const BigInteger &);
  BigInteger &operator%=(const BigInteger &);

  friend std::istream &operator>>(std::istream &, BigInteger &);
  friend std::ostream &operator<<(std::ostream &, const BigInteger &);

  std::string toString() const;

 private:
  typedef long long Digit;

  enum Sign { kPositive, kNegative };

  static const Digit kBase = 100000;
  static const int kBaseLog = 5;
  static const int kBaseBase = 10;
  static const char kZero = '0';
  static const char kMinus = '-';
  static const char kDot = '.';

  BigInteger(std::vector<Digit>, enum Sign);

  static void Normalize(std::vector<Digit> &, bool = false);

  BigInteger &AddDigitwise(const BigInteger &);
  BigInteger &SubtractDigitwise(const BigInteger &);
  bool IsZero() const { return (digits_.back() == 0); }
  bool CompareAbs(const BigInteger &) const;

  std::vector<Digit> digits_;
  enum Sign sign_;
};

BigInteger operator "" _bi(unsigned long long literal) {
  return BigInteger(literal);
}

BigInteger &BigInteger::operator+=(const BigInteger &other) {
  return (sign_ == other.sign_) ? AddDigitwise(other) : SubtractDigitwise(other);
}

BigInteger operator+(const BigInteger &lhs, const BigInteger &rhs) {
  auto lhs_copy = lhs;
  lhs_copy += rhs;

  return lhs_copy;
}

BigInteger &BigInteger::operator-=(const BigInteger &other) {
  return (sign_ == other.sign_) ? SubtractDigitwise(other) : AddDigitwise(other);
}

BigInteger operator-(const BigInteger &lhs, const BigInteger &rhs) {
  auto lhs_copy = lhs;
  lhs_copy -= rhs;

  return lhs_copy;
}

BigInteger &BigInteger::operator*=(const BigInteger &other) {
  if (IsZero() || other.IsZero()) {
    *this = 0_bi;
    return *this;
  }

  if (other == kBase) {
    digits_.insert(digits_.begin(), 0);
    return *this;
  }

  if (other == -1_bi) {
    sign_ = (sign_ == kPositive) ? kNegative : kPositive;
    return *this;
  }

  std::vector<Digit> prod_digits(digits_.size() + other.digits_.size());
  auto prod_sign = (sign_ == other.sign_) ? kPositive : kNegative;

  for (size_t i = 0; i < digits_.size(); i++) {
    for (size_t j = 0; j < other.digits_.size(); j++) {
      prod_digits[i + j] += digits_[i] * other.digits_[j];
    }
  }
  Normalize(prod_digits);

  digits_ = std::move(prod_digits);
  sign_ = prod_sign;

  return *this;
}

BigInteger operator*(const BigInteger &lhs, const BigInteger &rhs) {
  auto lhs_copy = lhs;
  lhs_copy *= rhs;

  return lhs_copy;
}

BigInteger &BigInteger::operator/=(const BigInteger &other) {
  if (other.IsZero()) {
    throw std::exception();
  }

  if (digits_.size() < other.digits_.size()) {
    *this = 0_bi;
  }

  if (IsZero()) {
    return *this;
  }

  auto abs_divisor = Abs(other);

  auto processed_digits_count = 0U;
  std::vector<Digit> most_significant_digits;
  most_significant_digits.reserve(abs_divisor.digits_.size());
  for (size_t i = digits_.size() - abs_divisor.digits_.size(); i < digits_.size(); i++) {
    auto digit = digits_[i];
    most_significant_digits.push_back(digit);
    processed_digits_count++;
  }

  BigInteger dividend(std::move(most_significant_digits), kPositive);
  std::vector<Digit> reverse_quotient_digits;

  do {
    bool is_first_digit_to_add = true;
    while (dividend < abs_divisor) {
      if (processed_digits_count == digits_.size()) {
        break;
      }

      auto new_digit = digits_[digits_.size() - processed_digits_count - 1];
      dividend *= kBase;
      dividend += new_digit;
      processed_digits_count++;

      if (!is_first_digit_to_add) {
        reverse_quotient_digits.push_back(0);
      }
      is_first_digit_to_add = false;
    }

    auto quotient_digit = 0;
    while (dividend >= abs_divisor) {
      dividend -= abs_divisor;
      quotient_digit++;
    }

    reverse_quotient_digits.push_back(quotient_digit);
  } while (processed_digits_count < digits_.size());

  auto first = reverse_quotient_digits.begin();
  auto last = reverse_quotient_digits.end();
  while ((first != last) && (first != --last)) {
    std::iter_swap(first, last);
    first++;
  }

  auto &quotient_digits = reverse_quotient_digits;
  if (quotient_digits.back() == 0) {
    *this = 0_bi;
    return *this;
  }

  auto quotient_sign = (sign_ == other.sign_) ? kPositive : kNegative;
  *this = BigInteger(std::move(quotient_digits), quotient_sign);

  return *this;
}

BigInteger operator/(const BigInteger &lhs, const BigInteger &rhs) {
  auto lhs_copy = lhs;
  lhs_copy /= rhs;

  return lhs_copy;
}

BigInteger &BigInteger::operator%=(const BigInteger &other) {
  if (other.IsZero()) {
    throw std::exception();
  }

  if (IsZero()) {
    return *this;
  }

  if (digits_.size() < other.digits_.size()) {
    return *this;
  }

  auto abs_divisor = Abs(other);

  auto processed_digits_count = 0U;
  std::vector<Digit> most_significant_digits;
  most_significant_digits.reserve(abs_divisor.digits_.size());
  for (size_t i = digits_.size() - abs_divisor.digits_.size(); i < digits_.size(); i++) {
    auto digit = digits_[i];
    most_significant_digits.push_back(digit);
    processed_digits_count++;
  }

  BigInteger dividend(std::move(most_significant_digits), kPositive);

  do {
    while (dividend < abs_divisor) {
      if (processed_digits_count == digits_.size()) {
        break;
      }

      auto new_digit = digits_[digits_.size() - processed_digits_count - 1];
      dividend *= kBase;
      dividend += new_digit;
      processed_digits_count++;
    }

    while (dividend >= abs_divisor) {
      dividend -= abs_divisor;
    }
  } while (processed_digits_count < digits_.size());

  if (!dividend.IsZero()) {
    dividend.sign_ = sign_;
  }
  *this = dividend;

  return *this;
}

BigInteger operator%(const BigInteger &lhs, const BigInteger &rhs) {
  auto lhs_copy = lhs;
  lhs_copy %= rhs;

  return lhs_copy;
}

BigInteger BigInteger::Abs(const BigInteger &subject) {
  BigInteger absolute_value = subject;
  absolute_value.sign_ = kPositive;

  return absolute_value;
}

std::string BigInteger::DivideAsDecimal(const BigInteger &x, const BigInteger &y, size_t precision) {
  if (precision == 0) {
    return (x / y).toString();
  }

  auto empowered_x = x;
  size_t power;
  for (power = 0; power < precision + 1; power += kBaseLog) {
    empowered_x *= kBase;
  }
  auto additional_digits = (power != 0) ? (power % precision) : 0;
  auto base_power = 1LL;
  for (size_t i = 0; i < additional_digits; i++) {
    base_power *= kBaseBase;
  }

  auto quotient = empowered_x / y;
  auto end_digit = ((quotient % base_power) * kBaseBase) / base_power;
  quotient /= base_power;
  if (end_digit >= 5) {
    ++quotient;
  } else if (end_digit <= -5) {
    --quotient;
  }

  std::string decimal_representation = Abs(quotient).toString();

  if (decimal_representation.size() <= precision) {
    decimal_representation =
        std::string(1, kZero) + std::string(1, kDot) + std::string(precision - decimal_representation.size(), kZero)
            + decimal_representation;
  } else {
    decimal_representation.insert(decimal_representation.size() - precision, 1, kDot);
  }

  if (quotient.sign_ == kNegative) {
    decimal_representation = std::string(1, kMinus) + decimal_representation;
  }

  return decimal_representation;
}

BigInteger::BigInteger(int subject) {
  if (subject == 0) {
    digits_ = {0};
    sign_ = kPositive;

    return;
  }

  sign_ = (subject > 0) ? kPositive : kNegative;
  auto absolute_value = std::abs(subject);

  while (absolute_value > 0) {
    Digit digit = absolute_value % kBase;
    digits_.push_back(digit);
    absolute_value /= kBase;
  }
}

std::string BigInteger::toString() const {
  std::string result;
  result.reserve(digits_.size() * kBaseLog + 1);

  if (sign_ == kNegative) {
    result.push_back(kMinus);
  }

  for (size_t shift = 1; shift <= digits_.size(); shift++) {
    auto digit = digits_[digits_.size() - shift];
    auto with_leading_zeros = std::to_string(digit);
    if (shift != 1) {
      with_leading_zeros = std::string(kBaseLog - with_leading_zeros.size(), kZero).append(with_leading_zeros);
    }
    result += with_leading_zeros;
  }

  return result;
}

bool BigInteger::CompareAbs(const BigInteger &other) const {
  if (digits_.size() == other.digits_.size()) {
    return std::lexicographical_compare(digits_.rbegin(), digits_.rend(), other.digits_.rbegin(), other.digits_.rend());
  } else {
    return digits_.size() < other.digits_.size();
  }
}

bool BigInteger::operator<(const BigInteger &other) const {
  if (sign_ != other.sign_) {
    return (sign_ == kNegative);
  }

  return (sign_ == kPositive) ? CompareAbs(other) : other.CompareAbs(*this);
}

bool BigInteger::operator==(const BigInteger &other) const {
  if ((sign_ != other.sign_) || (digits_.size() != other.digits_.size())) {
    return false;
  }

  return (digits_ == other.digits_);
}

BigInteger BigInteger::operator-() const {
  if (IsZero()) {
    return 0_bi;
  }

  BigInteger opposite(*this);
  opposite.sign_ = (sign_ == kPositive) ? kNegative : kPositive;

  return opposite;
}

BigInteger &BigInteger::operator++() {
  if (*this == -1_bi) {
    *this = 0_bi;
    return *this;
  }

  if (sign_ == kPositive) {
    digits_.front()++;
  } else {
    digits_.front()--;
  }

  Normalize(digits_, true);

  return *this;
}

const BigInteger BigInteger::operator++(int) {
  auto copy = *this;
  ++(*this);

  return copy;
}

BigInteger &BigInteger::operator--() {
  if (IsZero()) {
    *this = -1_bi;
    return *this;
  }

  if (sign_ == kNegative) {
    digits_.front()++;
  } else {
    digits_.front()--;
  }

  Normalize(digits_, true);

  return *this;
}

const BigInteger BigInteger::operator--(int) {
  auto copy = *this;
  --(*this);

  return copy;
}

std::istream &operator>>(std::istream &is, BigInteger &subject) {
  std::string input;
  is >> input;

  std::vector<BigInteger::Digit> digits;
  digits.reserve(input.size());
  auto sign = (input.front() != BigInteger::kMinus) ? BigInteger::kPositive : BigInteger::kNegative;

  auto start = (sign == BigInteger::kPositive) ? 0 : 1;
  auto size = input.size() - start;
  for (size_t i = 1; i <= size; i += BigInteger::kBaseLog) {
    std::string block;
    if (size - i + 1 >= BigInteger::kBaseLog) {
      block = input.substr(input.size() - i + 1 - BigInteger::kBaseLog, BigInteger::kBaseLog);
    } else {
      block = input.substr(start, size - i + 1);
    }

    digits.push_back(std::stoll(block));
  }

  while ((digits.size() > 1) && (digits.back() == 0)) {
    digits.pop_back();
  }

  BigInteger result = (digits.back() == 0) ? 0_bi : BigInteger(std::move(digits), sign);
  subject = result;

  return is;
}

std::ostream &operator<<(std::ostream &os, const BigInteger &subject) {
  auto decimal_representation = subject.toString();
  os << decimal_representation;

  return os;
}

BigInteger::BigInteger(std::vector<Digit> digits, enum Sign sign) : digits_(std::move(digits)), sign_(sign) {
  if (digits_.empty()) {
    digits_.push_back(0);
  }
}

void BigInteger::Normalize(std::vector<Digit> &generalized_digits, bool first_digits_only) {
  auto carry = 0;
  for (auto &digit : generalized_digits) {
    digit += carry;
    carry = 0;

    if (!((0 <= digit) && (digit < kBase))) {
      carry = (digit < 0) ? -1 : (digit / kBase);
      digit -= carry * kBase;
    }

    if ((carry == 0) && first_digits_only) {
      return;
    }
  }

  if (carry != 0) {
    generalized_digits.push_back(carry);
  }

  while ((generalized_digits.size() > 1) && (generalized_digits.back() == 0)) {
    generalized_digits.pop_back();
  }
}

BigInteger &BigInteger::AddDigitwise(const BigInteger &other) {
  digits_.resize(std::max(digits_.size(), other.digits_.size()));
  for (size_t digit = 0; digit < other.digits_.size(); digit++) {
    digits_[digit] += other.digits_[digit];
  }
  Normalize(digits_);

  return *this;
}

BigInteger &BigInteger::SubtractDigitwise(const BigInteger &other) {
  if (!CompareAbs(other)) {
    for (size_t digit = 0; digit < other.digits_.size(); digit++) {
      digits_[digit] -= other.digits_[digit];
    }
  } else {
    digits_.resize(other.digits_.size());
    for (size_t digit = 0; digit < other.digits_.size(); digit++) {
      digits_[digit] = other.digits_[digit] - digits_[digit];
    }
    *this *= -1;
  }

  Normalize(digits_);
  if (digits_.back() == 0) {
    sign_ = kPositive;
  }

  return *this;
}

class Rational {
 public:
  Rational() : numerator_(0_bi), denominator_(1_bi) {}
  Rational(BigInteger subject) : numerator_(std::move(subject)), denominator_(1_bi) {}
  Rational(int subject) : numerator_(subject), denominator_(1_bi) {}
  Rational(BigInteger, BigInteger);
  Rational(const Rational &) = default;
  Rational(Rational &&other) noexcept = default;

  explicit operator double() const { return std::stod(asDecimal(kPrecision)); }

  bool operator<(const Rational &) const;
  bool operator>(const Rational &other) const { return (other < *this); }
  bool operator<=(const Rational &other) const { return !(*this > other); }
  bool operator>=(const Rational &other) const { return !(*this < other); }
  bool operator==(const Rational &) const;
  bool operator!=(const Rational &other) const { return !(*this == other); }

  Rational &operator=(const Rational &);

  Rational operator-() const;

  Rational &operator+=(const Rational &other);
  Rational &operator-=(const Rational &other);
  Rational &operator*=(const Rational &other);
  Rational &operator/=(const Rational &other);

  std::string toString() const;
  std::string asDecimal(size_t = 0) const;

 private:
  static const int kPrecision = 10;

  void Simplify();

  BigInteger &Numerator() { return numerator_; }
  const BigInteger &Numerator() const { return numerator_; }
  BigInteger &Denominator() { return denominator_; }
  const BigInteger &Denominator() const { return denominator_; }

  BigInteger numerator_;
  BigInteger denominator_;
};

Rational::Rational(BigInteger numerator, BigInteger denominator)
    : numerator_(std::move(numerator)), denominator_(std::move(denominator)) {
  Simplify();
}

bool Rational::operator<(const Rational &other) const {
  return (Numerator() * other.Denominator()) < (other.Numerator() * Denominator());
}

bool Rational::operator==(const Rational &other) const {
  return (Numerator() == other.Numerator()) && (Denominator() == other.Denominator());
}

Rational &Rational::operator=(const Rational &other) {
  Numerator() = other.Numerator();
  Denominator() = other.Denominator();

  return *this;
}

Rational Rational::operator-() const {
  auto copy = *this;
  copy.Numerator() *= -1_bi;

  return copy;
}

Rational &Rational::operator+=(const Rational &other) {
  Numerator() = (Numerator() * other.Denominator()) + (other.Numerator() * Denominator());
  Denominator() = Denominator() * other.Denominator();

  Simplify();

  return *this;
}

Rational &Rational::operator-=(const Rational &other) {
  Numerator() = (Numerator() * other.Denominator()) - (other.Numerator() * Denominator());
  Denominator() = Denominator() * other.Denominator();

  Simplify();

  return *this;
}

Rational operator+(const Rational &lhs, const Rational &rhs) {
  auto lhs_copy = lhs;
  lhs_copy += rhs;

  return lhs_copy;
}

Rational operator-(const Rational &lhs, const Rational &rhs) {
  auto lhs_copy = lhs;
  lhs_copy -= rhs;

  return lhs_copy;
}

Rational &Rational::operator*=(const Rational &other) {
  Numerator() *= other.Numerator();
  Denominator() *= other.Denominator();

  Simplify();

  return *this;
}

Rational operator*(const Rational &lhs, const Rational &rhs) {
  auto lhs_copy = lhs;
  lhs_copy *= rhs;

  return lhs_copy;
}

Rational &Rational::operator/=(const Rational &other) {
  Numerator() *= other.Denominator();
  Denominator() *= other.Numerator();

  Simplify();

  return *this;
}

Rational operator/(const Rational &lhs, const Rational &rhs) {
  auto lhs_copy = lhs;
  lhs_copy /= rhs;

  return lhs_copy;
}

std::string Rational::toString() const {
  auto denominator = (Denominator() != 1_bi) ? ("/" + Denominator().toString()) : "";
  auto result = Numerator().toString() + denominator;

  return result;
}

std::string Rational::asDecimal(size_t precision) const {
  auto decimal_representation = BigInteger::DivideAsDecimal(Numerator(), Denominator(), precision);

  return decimal_representation;
}

BigInteger Gcd(BigInteger lhs, BigInteger rhs) {
  while (rhs != 0_bi) {
    lhs %= rhs;
    std::swap(lhs, rhs);
  }

  return lhs;
}

void Rational::Simplify() {
  if (Denominator() < 0_bi) {
    Numerator() *= -1;
    Denominator() *= -1;
  }

  if ((Numerator() % Denominator()) == 0_bi) {
    Numerator() /= Denominator();
    Denominator() = 1_bi;

    return;
  }

  auto gcd = Gcd(BigInteger::Abs(Numerator()), Denominator());
  Numerator() /= gcd;
  Denominator() /= gcd;
}