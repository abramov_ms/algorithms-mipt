// В этой задаче можно использовать std::vector. Другие стандартные контейнеры использовать нельзя. Необходимо
// реализовать шаблонный класс Deque<T> - сильно упрощенный аналог класса std::deque<T>. Тип T не обязан иметь
// конструктор по умолчанию, чтобы его можно было хранить в Deque.
//
// Ваш Deque должен обладать следующей функциональностью:
// - Конструкторы: по умолчанию, конструктор копирования, конструктор от int, конструктор от (int, const T&).
//   Оператор присваивания deque.
// - Метод size(), возвращающий текущий размер контейнера.
// - Обращение по индексу: квадратными скобками (без проверок выхода за границу) и at (кидающее std::out_of_range).
//   Должно работать за гарантированное O(1).
// - Методы push_back, pop_back, push_front, pop_front. Должны работать за амортизированное O(1).
// - Должен быть внутренний тип iterator (с маленькой буквы, в лучших традициях STL). Этот тип, помимо очевидного,
//   должен поддерживать:
//   - Инкремент, декремент
//   - Сложение с целыми числами
//   - Сравнение < > <= >= == !=
//   - Взятие разности двух итераторов
//   - Разыменование (унарная звездочка), результатом является T&
//   - Оператор ->, результатом является T*
// - Также должен быть константный итератор const_iterator. Отличие его от обычного iterator в том, что он не позволяет
//   менять лежащий под ним элемент. Конвертация неконстантного итератора в константный допустима, обратно - нет.
//   (Постарайтесь реализовать константный итератор так, чтобы его код не являлся копипастой кода обычного iterator.)
// - Операции push_back, push_front, pop_back и pop_front не должны инвалидировать указатели и ссылки на остальные
//   элементы дека. Требование неинвалидации итераторов отменяется.
// - Методы begin, cbegin, end и cend, возвращающие неконстантные и константные итераторы на начало и на “элемент после
//   конца” контейнера соответственно. Если сам контейнер константный, то методы begin и end тоже возвращают константные
//   итераторы.
// - reverse-итераторы, а также методы rbegin, rend, crbegin, crend.
// - Метод insert(iterator, const T&), делающий вставку в контейнер по итератору. Все элементы справа сдвигаются на один
//   вправо, вставка работает линейное время.
// - Метод erase(iterator), удаляющий элемент из контейнера по итератору. Все элементы справа сдвигаются на один влево,
//   удаление работает линейное время.
// - Все методы вашего Deque должны быть строго безопасны относительно исключений. Это значит, что в случае исключения в
//   конструкторе или операторе присваивания типа T во время выполнения какого-либо метода дека, последний должен
//   вернуться в исходное состояние, которое было до начала выполнения метода, и пробросить исключение наверх в
//   вызывающий код.

#include <vector>
#include <stdexcept>
#include <memory>

template<typename T>
class Deque {
private:
    template<bool IsConst>
    class BaseIterator;
    template<bool IsConst>
    class Iterator;
    template<bool IsConst>
    class ReverseIterator;

    static const size_t kBucketSize = 1000;

    std::vector<T*> buckets_;
    size_t first_bucket_position_ = 1;
    size_t first_element_position_ = 0;
    size_t last_bucket_position_ = 0;
    size_t last_element_position_ = 0;
    size_t size_ = 0;

public:
    using iterator = Iterator<false>;
    using const_iterator = Iterator<true>;
    using reverse_iterator = ReverseIterator<false>;
    using const_reverse_iterator = ReverseIterator<true>;
    
    Deque() = default;
    Deque(const Deque& other);
    explicit Deque(size_t size, const T& fill = T());

    Deque& operator=(const Deque& other);

    ~Deque();

    [[nodiscard]] size_t size() const { return size_; }
    [[nodiscard]] bool empty() const { return size() == 0; }

    [[nodiscard]] T& operator[](size_t index);
    [[nodiscard]] const T& operator[](size_t index) const;

    [[nodiscard]] T& at(size_t index);
    [[nodiscard]] const T& at(size_t index) const { return at(index); }

    void push_back(T element);
    void pop_back();
    void push_front(T element);
    void pop_front();

    void insert(iterator it, const T& subject);
    void erase(iterator it);

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const { return begin(); }
    const_iterator cend() const { return end(); }
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const { return rbegin(); }
    const_reverse_iterator crend() const { return rend(); }

private:
    static void DeleteBuckets(std::vector<T*>& buckets, size_t first_bucket_position, size_t last_bucket_position,
                              size_t first_element_position, size_t last_element_position);

    template<typename Callback>
    void Initialize(const Callback& fill_callback);
    void PushFirst(T element);
    void Grow();
};

template<typename T>
template<bool IsConst>
class Deque<T>::BaseIterator {
public:
    using value_type = T;
    using difference_type = ptrdiff_t;
    using iterator_category = std::random_access_iterator_tag;
    using reference = typename std::conditional<IsConst, const T&, T&>::type;
    using pointer = typename std::conditional<IsConst, const T*, T*>::type;
    
protected:
    using BucketsIter = typename std::conditional<IsConst,
        typename std::vector<T*>::const_iterator, typename std::vector<T*>::iterator>::type;
    
    size_t position_in_deque_;
    BucketsIter buckets_iterator_;
    typename BucketsIter::value_type bucket_;
    
public:
    BaseIterator(size_t position_in_deque, BucketsIter buckets_iterator) :
        position_in_deque_(position_in_deque), buckets_iterator_(buckets_iterator), bucket_(*buckets_iterator) {}
    BaseIterator(const BaseIterator& other) = default;
    BaseIterator& operator=(const BaseIterator& other) = default;    
        
    reference operator*() { return *(bucket_ + PositionInBucket()); }
    pointer operator->() { return bucket_ + PositionInBucket(); }
    
    bool operator==(const BaseIterator& other);
    bool operator!=(const BaseIterator& other) { return !(*this == other); }

protected:
    size_t PositionInBucket() const { return position_in_deque_ % kBucketSize; }
    void Move(int shift);
};

template<typename T>
template<bool IsConst>
bool Deque<T>::BaseIterator<IsConst>::operator==(const BaseIterator& other) {
    return buckets_iterator_ == other.buckets_iterator_ &&
           position_in_deque_ == other.position_in_deque_;
}

template<typename T>
template<bool IsConst>
void Deque<T>::BaseIterator<IsConst>::Move(int shift) {
    int bucket_shift;
    if (shift > 0) {
        bucket_shift = (PositionInBucket() + shift) / kBucketSize;
    } else {
        bucket_shift = -static_cast<int>(kBucketSize - 1 - PositionInBucket() - shift) / static_cast<int>(kBucketSize);
    }

    if (bucket_shift != 0) {
        buckets_iterator_ += bucket_shift;
        bucket_ = *buckets_iterator_;
    }

    position_in_deque_ += shift;
}

template<typename T>
template<bool IsConst>
class Deque<T>::Iterator : public BaseIterator<IsConst> {
public:
    Iterator(size_t position_in_deque, typename BaseIterator<IsConst>::BucketsIter buckets_iterator) :
        BaseIterator<IsConst>(position_in_deque, buckets_iterator) {}
    Iterator(const Iterator& other) = default;
    Iterator& operator=(const Iterator& other) = default;
    
    operator Iterator<true>() { 
        return Iterator<true>(BaseIterator<IsConst>::position_in_deque_, BaseIterator<IsConst>::buckets_iterator_); }
        
    Iterator& operator++() { return BaseIterator<IsConst>::Move(1), *this; }
    const Iterator operator++(int);
    Iterator& operator--() { return BaseIterator<IsConst>::Move(-1), *this; }
    const Iterator operator--(int);

    Iterator& operator+(int shift) { return BaseIterator<IsConst>::Move(shift), *this; }
    Iterator& operator-(int shift) { return BaseIterator<IsConst>::Move(-shift), *this; }

    bool operator<(const Deque::Iterator<IsConst>& other) const {
        return BaseIterator<IsConst>::position_in_deque_ < other.position_in_deque_; }
    bool operator>(const Deque::Iterator<IsConst>& other) const {
        return BaseIterator<IsConst>::position_in_deque_ > other.position_in_deque_; }
    bool operator <=(const Deque::Iterator<IsConst>& other) { return !(*this > other); }
    bool operator >=(const Deque::Iterator<IsConst>& other) { return !(*this < other); }

    typename BaseIterator<IsConst>::difference_type Distance(const Iterator& other) const {
        return static_cast<ptrdiff_t>(BaseIterator<IsConst>::position_in_deque_) - other.position_in_deque_; }
};

template<typename T>
template<bool IsConst>
const typename Deque<T>::template Iterator<IsConst> Deque<T>::Iterator<IsConst>::operator++(int) {
    auto self_copy = *this;
    ++(*this);
    
    return self_copy;
}

template<typename T>
template<bool IsConst>
const typename Deque<T>::template Iterator<IsConst> Deque<T>::Iterator<IsConst>::operator--(int) {
    auto self_copy = *this;
    --(*this);

    return self_copy;
}

template<typename T>
template<bool IsConst>
class Deque<T>::ReverseIterator : public BaseIterator<IsConst> {
public:
    ReverseIterator(size_t position_in_deque, typename BaseIterator<IsConst>::BucketsIter buckets_iterator) :
        BaseIterator<IsConst>(position_in_deque, buckets_iterator) {}
    ReverseIterator(const ReverseIterator& other) = default;
    ReverseIterator& operator=(const ReverseIterator& other) = default;

    operator ReverseIterator<true>() {
        return ReverseIterator<true>(
                BaseIterator<IsConst>::position_in_deque_, BaseIterator<IsConst>::buckets_iterator_); }

    ReverseIterator& operator++() { return BaseIterator<IsConst>::Move(-1), *this; }
    const ReverseIterator operator++(int);
    ReverseIterator& operator--() { return BaseIterator<IsConst>::Move(1), *this; }
    const ReverseIterator operator--(int);

    ReverseIterator& operator+(int shift) { return BaseIterator<IsConst>::Move(shift), *this; }
    ReverseIterator& operator-(int shift) { return BaseIterator<IsConst>::Move(-shift), *this; }

    bool operator<(const Deque::ReverseIterator<IsConst>& other) const {
        return BaseIterator<IsConst>::position_in_deque_ < other.position_in_deque_; }
    bool operator>(const Deque::ReverseIterator<IsConst>& other) const {
        return BaseIterator<IsConst>::position_in_deque_ > other.position_in_deque_; }
    bool operator <=(const Deque::ReverseIterator<IsConst>& other) { return !(*this > other); }
    bool operator >=(const Deque::ReverseIterator<IsConst>& other) { return !(*this < other); }

    typename BaseIterator<IsConst>::difference_type Distance(const ReverseIterator& other) const {
        return -static_cast<ptrdiff_t>(BaseIterator<IsConst>::position_in_deque_) + other.position_in_deque_; }
};

template<typename T>
template<bool IsConst>
const typename Deque<T>::template ReverseIterator<IsConst> Deque<T>::ReverseIterator<IsConst>::operator++(int) {
    auto self_copy = *this;
    ++(*this);

    return self_copy;
}

template<typename T>
template<bool IsConst>
const typename Deque<T>::template ReverseIterator<IsConst> Deque<T>::ReverseIterator<IsConst>::operator--(int) {
    auto self_copy = *this;
    --(*this);

    return self_copy;
}

template<typename Iter>
typename Iter::difference_type operator-(const Iter& from, const Iter& to) {
    return from.Distance(to);
}

template<typename T>
Deque<T>::Deque(const Deque<T>& other):
    buckets_(other.buckets_.size()), first_bucket_position_(other.first_bucket_position_),
    first_element_position_(other.first_element_position_), last_bucket_position_(other.last_bucket_position_),
    last_element_position_(other.last_element_position_), size_(other.size_) {
    auto fill_bucket_callback = [&](size_t bucket) {
        T* first = bucket == first_bucket_position_ ?
                   other.buckets_[first_bucket_position_] + first_element_position_ :
                   other.buckets_[bucket];
        T* last = bucket == last_bucket_position_ ?
                  other.buckets_[last_bucket_position_] + last_element_position_ + 1 :
                  other.buckets_[bucket] + kBucketSize;
        T* first_d = bucket == first_bucket_position_ ?
                     buckets_[first_bucket_position_] + first_element_position_ :
                     buckets_[bucket];

        std::uninitialized_copy(first, last, first_d);
    };
    Initialize(fill_bucket_callback);
}

template<typename T>
Deque<T>::Deque(size_t size, const T& fill):
    buckets_((size + kBucketSize - 1) / kBucketSize), first_bucket_position_(0), first_element_position_(0),
    last_bucket_position_((size - 1) / kBucketSize), last_element_position_((size  - 1) % kBucketSize), size_(size) {
    auto fill_bucket_callback = [&](size_t bucket) {
        size_t element;
        try {
            for (element = bucket == first_bucket_position_ ? first_element_position_ : 0;
                 element < (bucket == last_bucket_position_ ? last_element_position_ + 1 : kBucketSize);
                 ++element) {
                new(buckets_[bucket] + element) T(fill);
            }
        } catch (...) {
            for (size_t copied_element = 0; copied_element < element; ++copied_element) {
                buckets_[bucket][copied_element].~T();
            }

            throw;
        }
    };
    Initialize(fill_bucket_callback);
}

template<typename T>
Deque<T>& Deque<T>::operator=(const Deque<T> &other) {
    this->~Deque();

    buckets_ = std::vector<T*>(other.buckets_.size());
    first_bucket_position_ = other.first_bucket_position_;
    first_element_position_ = other.first_element_position_;
    last_bucket_position_ = other.last_bucket_position_;
    last_element_position_ = other.last_element_position_;
    size_ = other.size_;

    auto fill_bucket_callback = [&](size_t bucket) {
        T *first = bucket == first_bucket_position_ ?
                   other.buckets_[first_bucket_position_] + first_element_position_ :
                   other.buckets_[bucket];
        T *last = bucket == last_bucket_position_ ?
                  other.buckets_[last_bucket_position_] + last_element_position_ + 1 :
                  other.buckets_[bucket] + kBucketSize;
        T *first_d = bucket == first_bucket_position_ ?
                     buckets_[first_bucket_position_] + first_element_position_ :
                     buckets_[bucket];

        std::uninitialized_copy(first, last, first_d);
    };
    Initialize(fill_bucket_callback);

    return *this;
}

template<typename T>
Deque<T>::~Deque() {
    DeleteBuckets(buckets_, first_bucket_position_, last_bucket_position_, first_element_position_,
                  last_element_position_);
}

template<typename T>
T& Deque<T>::operator[](size_t index) {
    index += first_element_position_;
    return buckets_[first_bucket_position_ + (index / kBucketSize)][index % kBucketSize];
}

template<typename T>
const T& Deque<T>::operator[](size_t index) const {
    index += first_element_position_;
    return buckets_[first_bucket_position_ + (index / kBucketSize)][index % kBucketSize];
}

template<typename T>
T& Deque<T>::at(size_t index) {
    if (index >= size()) {
        throw std::out_of_range(
            "index (which is " + std::to_string(index) + ")" +
            " >= this->size() (which is " + std::to_string(size()) + ")"
        );

    }

    return (*this)[index];
}

template<typename T>
void Deque<T>::push_back(T element) {
    if (empty()) {
        PushFirst(element);
        return;
    }

    Grow();

    ++size_;
    last_element_position_ = (last_element_position_ + 1) % kBucketSize;
    if (last_element_position_ == 0) {
        ++last_bucket_position_;
        buckets_[last_bucket_position_] = reinterpret_cast<T*>(new int8_t[sizeof(T) * kBucketSize]);
    }

    buckets_[last_bucket_position_][last_element_position_] = std::move(element);
}

template<typename T>
void Deque<T>::pop_back() {
    buckets_[last_bucket_position_][last_element_position_].~T();

    --size_;
    last_element_position_ = (last_element_position_ + (kBucketSize - 1)) % kBucketSize;
    if (last_element_position_ == kBucketSize - 1) {
        delete[] reinterpret_cast<int8_t*>(buckets_[last_bucket_position_]);
        if (last_bucket_position_ != 0) {
            --last_bucket_position_;
        }
    }
}

template<typename T>
void Deque<T>::push_front(T element) {
    if (empty()) {
        PushFirst(element);
        return;
    }

    Grow();

    ++size_;
    first_element_position_ = (first_element_position_ + (kBucketSize - 1)) % kBucketSize;
    if (first_element_position_ == kBucketSize - 1) {
        --first_bucket_position_;
        buckets_[first_bucket_position_] = reinterpret_cast<T*>(new int8_t[sizeof(T) * kBucketSize]);
    }

    buckets_[first_bucket_position_][first_element_position_] = std::move(element);
}

template <typename T>
void Deque<T>::pop_front() {
    buckets_[first_bucket_position_][first_element_position_].~T();

    --size_;
    first_element_position_ = (first_element_position_ + 1) % kBucketSize;
    if (first_element_position_ == 0) {
        delete[] reinterpret_cast<int8_t*>(buckets_[first_bucket_position_]);
        ++first_bucket_position_;
    }
}

template<typename T>
void Deque<T>::insert(iterator it, const T& subject) {
    if (empty()) {
        PushFirst(subject);
        return;
    }

    auto buckets_backup = std::move(buckets_);
    iterator current_it(first_bucket_position_ * kBucketSize + first_element_position_,
                        buckets_backup.begin() + first_bucket_position_);
    auto first_bucket_position_backup = first_bucket_position_;
    auto last_bucket_position_backup = last_bucket_position_;
    auto first_element_position_backup = first_element_position_;
    auto last_element_position_backup = last_element_position_;

    size_t shift = 0;
    size_t new_buckets_count = buckets_backup.size();
    if (last_bucket_position_ == buckets_.size() - 1 && last_element_position_ == kBucketSize) {
        new_buckets_count *= 2;
        shift += new_buckets_count / 4;
    }
    first_bucket_position_ += shift;
    last_bucket_position_ += shift;

    ++size_;
    last_element_position_ = (last_element_position_ + 1) % kBucketSize;
    if (last_element_position_ == 0) {
        ++last_bucket_position_;
    }
    buckets_ = std::vector<T*>(new_buckets_count);

    auto fill_callback = [&](size_t bucket) {
        size_t element;
        try {
            for (element = bucket == first_bucket_position_ ? first_element_position_ : 0;
                 element < (bucket == last_bucket_position_ ? last_element_position_ + 1 : kBucketSize);
                 ++element, ++current_it) {
                if (current_it == it) {
                    new(buckets_[bucket] + element) T(subject);
                    new(buckets_[bucket] + (++element)) T(*current_it);
                } else {
                    new(buckets_[bucket] + element) T(*current_it);
                }
            }
        } catch (...) {
            for (size_t copied_element = 0; copied_element < element; ++copied_element) {
                buckets_[bucket][copied_element].~T();
            }

            throw;
        }
    };
    Initialize(fill_callback);

    DeleteBuckets(buckets_backup, first_bucket_position_backup, last_bucket_position_backup,
                  first_element_position_backup, last_element_position_backup);
}

template <typename T>
void Deque<T>::erase(iterator it) {
    auto buckets_backup = std::move(buckets_);
    iterator current_it(first_bucket_position_ * kBucketSize + first_element_position_,
                        buckets_backup.begin() + first_bucket_position_);
    auto first_bucket_position_backup = first_bucket_position_;
    auto last_bucket_position_backup = last_bucket_position_;
    auto first_element_position_backup = first_element_position_;
    auto last_element_position_backup = last_element_position_;

    --size_;
    last_element_position_ = (last_element_position_ + (kBucketSize - 1)) % kBucketSize;
    if (last_element_position_ == kBucketSize - 1) {
        --last_bucket_position_;
    }

    buckets_ = std::vector<T*>(buckets_backup.size());

    auto fill_callback = [&](size_t bucket) {
        size_t element;
        try {
            for (element = bucket == first_bucket_position_ ? first_element_position_ : 0;
                 element < (bucket == last_bucket_position_ ? last_element_position_ + 1 : kBucketSize);
                 ++element, ++current_it) {
                if (current_it == it) {
                    ++current_it;
                }
                new(buckets_[bucket] + element) T(*current_it);
            }
        } catch (...) {
            for (size_t copied_element = 0; copied_element < element; ++copied_element) {
                buckets_[bucket][copied_element].~T();
            }

            throw;
        }
    };
    Initialize(fill_callback);

    DeleteBuckets(buckets_backup, first_bucket_position_backup, last_bucket_position_backup,
                  first_element_position_backup, last_element_position_backup);
}

template <typename T>
typename Deque<T>::iterator Deque<T>::begin() {
    return iterator(first_bucket_position_ * kBucketSize + first_element_position_,
                    buckets_.begin() + first_bucket_position_);
}

template <typename T>
typename Deque<T>::const_iterator Deque<T>::begin() const {
    return const_iterator(first_bucket_position_ * kBucketSize + first_element_position_,
                          buckets_.begin() + first_bucket_position_);
}

template <typename T>
typename Deque<T>::iterator Deque<T>::end() {
    size_t one_past_last_position_in_bucket = (last_element_position_ + 1) % kBucketSize;
    size_t one_past_last_bucket_position = last_bucket_position_;
    if (one_past_last_position_in_bucket == 0) {
        ++one_past_last_bucket_position;
    }

    return iterator(one_past_last_bucket_position * kBucketSize + one_past_last_position_in_bucket,
                    buckets_.begin() + one_past_last_bucket_position);
}

template <typename T>
typename Deque<T>::const_iterator Deque<T>::end() const {
    size_t one_past_last_position_in_bucket = (last_element_position_ + 1) % kBucketSize;
    size_t one_past_last_bucket_position = last_bucket_position_;
    if (one_past_last_position_in_bucket == 0) {
        ++one_past_last_bucket_position;
    }

    return const_iterator(one_past_last_bucket_position * kBucketSize + one_past_last_position_in_bucket,
                          buckets_.begin() + one_past_last_bucket_position);
}

template <typename T>
typename Deque<T>::reverse_iterator Deque<T>::rbegin() {
    return reverse_iterator(last_bucket_position_ * kBucketSize + last_element_position_,
                            buckets_.begin() + last_bucket_position_);
}

template <typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::rbegin() const {
    return const_reverse_iterator(last_bucket_position_ * kBucketSize + last_element_position_,
                                  buckets_.begin() + last_bucket_position_);
}

template <typename T>
typename Deque<T>::reverse_iterator Deque<T>::rend() {
    size_t one_past_last_position_in_bucket = (first_element_position_ + kBucketSize - 1) % kBucketSize;
    size_t one_past_last_bucket_position = first_bucket_position_ - 1;
    if (one_past_last_position_in_bucket == 0) {
        --one_past_last_bucket_position;
    }

    return reverse_iterator(one_past_last_bucket_position * kBucketSize + one_past_last_position_in_bucket,
                            buckets_.end());
}

template <typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::rend() const {
    size_t one_past_last_position_in_bucket = (first_element_position_ + kBucketSize - 1) % kBucketSize;
    size_t one_past_last_bucket_position = first_bucket_position_;
    if (one_past_last_position_in_bucket == 0) {
        --one_past_last_bucket_position;
    }

    return const_reverse_iterator(one_past_last_bucket_position * kBucketSize + one_past_last_position_in_bucket,
                                  buckets_.end());
}

template<typename T>
template<typename Callback>
void Deque<T>::Initialize(const Callback& fill_callback) {
    for (size_t bucket = first_bucket_position_; bucket <= last_bucket_position_; ++bucket) {
        try {
            buckets_[bucket] = reinterpret_cast<T*>(new int8_t[sizeof(T) * kBucketSize]);
            fill_callback(bucket);
        } catch (...) {
            for (size_t processed_bucket = 0; processed_bucket < bucket; ++processed_bucket) {
                for (size_t copied_object = processed_bucket == 0 ? first_bucket_position_ : 0;
                     copied_object < kBucketSize;
                     ++copied_object) {
                    buckets_[processed_bucket][copied_object].~T();
                }
                delete[] reinterpret_cast<int8_t*>(buckets_[processed_bucket]);
            }
            delete[] reinterpret_cast<int8_t*>(buckets_[bucket]);

            throw;
        }
    }
}

template<typename T>
void Deque<T>::DeleteBuckets(std::vector<T*>& buckets, size_t first_bucket_position, size_t last_bucket_position,
                             size_t first_element_position, size_t last_element_position) {
    for (size_t bucket = first_bucket_position; bucket <= last_bucket_position; ++bucket) {
        for (size_t element = bucket == first_bucket_position ? first_element_position : 0;
             element < (bucket == last_bucket_position ? last_element_position + 1 : kBucketSize);
             ++element) {
            buckets[bucket][element].~T();
        }
        delete[] reinterpret_cast<int8_t*>(buckets[bucket]);
    }
}

template<typename T>
void Deque<T>::PushFirst(T element) {
    T* bucket = reinterpret_cast<T*>(new int8_t[sizeof(T) * kBucketSize]);
    bucket[kBucketSize / 2] = std::move(element);
    buckets_ = { bucket };

    first_bucket_position_ = last_bucket_position_ = 0;
    first_element_position_ = last_element_position_ = kBucketSize / 2;
    size_ = 1;
}

template<typename T>
void Deque<T>::Grow() {
    while ((first_bucket_position_ == 0 && first_element_position_ == 0) ||
           (last_bucket_position_ == buckets_.size() - 1 && last_element_position_ == kBucketSize - 1)) {
        std::vector<T*> new_buckets(buckets_.size() * 2);
        size_t first_copied_bucket_position = new_buckets.size() / 4;
        std::copy(buckets_.begin(), buckets_.end(), new_buckets.begin() + first_copied_bucket_position);

        buckets_ = std::move(new_buckets);

        first_bucket_position_ += first_copied_bucket_position;
        last_bucket_position_ += first_copied_bucket_position;
    }
}
