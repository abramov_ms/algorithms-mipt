// В этой задаче запрещено пользоваться какими-либо стандартными контейнерами. Выделения памяти нужно делать вручную.
// Разрешается подключать только <iostream> и <cstring>.
//
// Напишите класс String - упрощенный аналог библиотечного std::string. Необходимо поддержать следующий функционал:
// + Конструктор от C-style строки (const char*).
// + Конструктор от двух параметров: число n и символ c - создает строку из n копий символа c.
// + Конструктор по умолчанию, копирующий конструктор и оператор присваивания.
// + Оператор ==, позволяющий проверять строки на совпадение.
// + Квадратные скобки, корректно работающие как для константных, так и для неконстантных строк.
// + Метод length(), возвращающий размер строки.
// + Методы push_back(char) и pop_back(). Учетное время работы должно быть O(1).
// + Методы front() и back(), возвращающие ссылку на начальный и на последний символы строки соответственно.
// + Оператор +=, позволяющий добавить к строке символ или другую строку. Добавление символа должно работать за
//   амортизированное O(1).
// + Оператор +, позволяющий складывать строки с символами, символы со строками и строки со строками.
// + Методы find(substring) и rfind(substring), позволяющие найти самое левое и самое правое вхождение подстроки
//   substring в данную строку. Возвращают индекс найденного вхождения. Если вхождений не найдено, нужно вернуть
//   length() (фиктивный индекс).
// + Метод substr(start, count), возвращающий подстроку начиная с индекса start и состоящую из count символов.
// + Метод empty(), проверяющий, пуста ли строка. Метод clear(), позволяющий очистить строку.
// + Операторы вывода в поток << и ввода из потока >>.
//
// В вашем файле должна отсутствовать функция main(), а сам файл должен называться string.h. В качестве компилятора
// необходимо указывать Make C++17. Ваш код будет вставлен посредством команды #include "string.h" в программу,
// содержащую тесты; вследствие этого код необходимо отправлять в файле со строго соответствующим именем!

#pragma once

#include <iostream>
#include <cstring>

class String {
 public:
  String() : size_(0), capacity_(1), characters_(new char[capacity_]) {}
  String(const String &);
  String(const char *);
  String(size_t, char);

  ~String();

  bool operator==(const String &);
  String &operator=(const String &);
  char &operator[](size_t index) { return characters_[index]; }
  const char &operator[](size_t index) const { return characters_[index]; }
  String &operator+=(const String &);
  String &operator+=(char);

  size_t length() const { return size_; }
  bool empty() const { return (length() == 0); }
  void clear();

  void push_back(char);
  void pop_back();
  char &front() { return characters_[0]; }
  char &back() { return characters_[size_ - 1]; }
  const char &front() const { return characters_[0]; }
  const char &back() const { return characters_[size_ - 1]; }

  size_t find(const String &) const;
  size_t rfind(const String &) const;
  String substr(size_t, size_t) const;

 private:
  size_t size_;
  size_t capacity_;
  char *characters_;
};

String::String(const String &other) : size_(other.size_), capacity_(other.capacity_), characters_(new char[capacity_]) {
  memcpy(characters_, other.characters_, size_);
}

String::String(const char *characters)
    : size_(strlen(characters)), capacity_(2 * size_), characters_(new char[capacity_]) {
  memcpy(characters_, characters, size_);
}

String::String(size_t n, char c) : size_(n), capacity_(2 * size_), characters_(new char[capacity_]) {
  memset(characters_, c, size_);
}

String::~String() {
  delete[] characters_;
}

String &String::operator=(const String &other) {
  if (this != &other) {
    if (other.capacity_ != capacity_) {
      delete[] characters_;
      capacity_ = other.capacity_;
      characters_ = new char[capacity_];
    }

    memcpy(characters_, other.characters_, other.size_);
    size_ = other.size_;
  }

  return *this;
}

bool String::operator==(const String &other) {
  return (size_ == other.size_) && (memcmp(characters_, other.characters_, size_) == 0);
}

String &String::operator+=(const String &other) {
  if ((2 * (size_ + other.size_)) > capacity_) {
    size_t new_capacity = 2 * (size_ + other.size_);
    char *new_chars_destination = new char[new_capacity];
    memcpy(new_chars_destination, characters_, size_);

    delete[] characters_;
    capacity_ = new_capacity;
    characters_ = new_chars_destination;
  }

  memcpy(characters_ + size_, other.characters_, other.size_);
  size_ += other.size_;

  return *this;
}

String &String::operator+=(char c) {
  push_back(c);
  return *this;
}

void String::push_back(char c) {
  if ((2 * size_) > capacity_) {
    char *new_chars_destination = new char[capacity_ * 2];
    memcpy(new_chars_destination, characters_, size_);

    delete[] characters_;
    capacity_ *= 2;
    characters_ = new_chars_destination;
  }

  characters_[size_] = c;
  size_++;
}

void String::pop_back() {
  size_--;

  if ((4 * size_) < capacity_) {
    char *new_chars_destination = new char[capacity_ / 2];
    memcpy(new_chars_destination, characters_, size_);

    delete[] characters_;
    capacity_ /= 2;
    characters_ = new_chars_destination;
  }
}

size_t String::find(const String &substring) const {
  if (substring.length() > length()) {
    return length();
  }

  for (size_t needle_index = 0; needle_index <= length() - substring.length(); needle_index++) {
    bool is_substring = true;
    for (size_t substring_index = 0; substring_index < substring.length(); substring_index++) {
      if ((*this)[needle_index + substring_index] != substring[substring_index]) {
        is_substring = false;
        break;
      }
    }

    if (is_substring) {
      return needle_index;
    }
  }

  return length();
}

size_t String::rfind(const String &substring) const {
  if (substring.length() > length()) {
    return length();
  }

  for (size_t shift = 0; shift <= length() - substring.length(); shift++) {
    bool is_substring = true;
    for (size_t substring_index = 0; substring_index < substring.length(); substring_index++) {
      if ((*this)[(length() - substring.length() - shift) + substring_index] != substring[substring_index]) {
        is_substring = false;
        break;
      }
    }

    if (is_substring) {
      size_t needle_index = length() - substring.length() - shift;
      return needle_index;
    }
  }

  return length();
}

String String::substr(size_t start, size_t count) const {
  char *substring_chars = new char[count + 1];
  memcpy(substring_chars, characters_ + start, count);
  substring_chars[count] = 0;

  String substring(substring_chars);

  return substring;
}

void String::clear() {
  size_ = 0;
  capacity_ = 1;
  delete[] characters_;
  characters_ = new char[capacity_];
}

String operator+(const String &lhs, char rhs) {
  String result = lhs;
  result.push_back(rhs);

  return result;
}

String operator+(char lhs, const String &rhs) {
  String result;
  result.push_back(lhs);
  result += rhs;

  return result;
}

String operator+(const String &lhs, const String &rhs) {
  String result = lhs;
  result += rhs;

  return result;
}

std::istream &operator>>(std::istream &is, String &subject) {
  subject.clear();

  char current_char;
  bool starting_ws_skipped = false;
  is >> std::noskipws;

  while (is >> current_char) {
    if (!isspace(current_char)) {
      subject.push_back(current_char);
      starting_ws_skipped = true;
    } else if (starting_ws_skipped) {
      break;
    }
  }

  return is;
}

std::ostream &operator<<(std::ostream &os, String &subject) {
  for (size_t i = 0; i < subject.length(); i++) {
    os << subject[i];
  }

  return os;
}

std::ostream &operator<<(std::ostream &os, String &&subject) {
  for (size_t i = 0; i < subject.length(); i++) {
    os << subject[i];
  }

  return os;
}