// Задача Matrix. Цена 12 баллов. Тип задачи: практическая, с код-ревью.
// Баллы за эту задачу можно получить только при условии прохождения тестов по задачам BigInteger и Rational.
//
// Написать шаблонный класс Finite<int N>, реализующий концепцию конечного поля из N элементов. Должны поддерживаться
// арифметические операции, кроме деления, а в случае простого N и деление тоже. Элемент поля Finite<N> должно быть
// можно сконструировать от int путем взятия остатка от деления на N (в математическом смысле).

// После этого, используя ранее написанный класс рациональных чисел, написать класс Matrix с тремя шаблонными
// параметрами: unsigned M, unsigned N, typename Field=Rational. (По умолчанию берётся поле рациональных чисел, но можно
// создать матрицу и над конечным полем.)
//
// Матрицы должны поддерживать следующие операции:
// + Проверка на равенство: операторы == и !=.
// + Конструктор по умолчанию, создающий единичную матрицу. Для неквадратных матриц конструктор по умолчанию не
//   требуется.
// + Конструктор, создающий матрицу из vector<vector<T>>. Должно быть можно создать матрицу из vector<vector<int>>.
// + Сложение, вычитание, операторы +=, -=. Сложение и вычитание матриц несоответствующих размеров не должно
// компилироваться.
// + Умножение на число.
// + Умножение матриц, работающие за max(M,N,K)**3. Для квадратных матриц должен поддерживаться еще и оператор *=.
//   Попытка перемножить матрицы несоответствующих размеров должна приводить к ошибке компиляции.
// + Метод det(), возвращающий определитель матрицы за O(N**3). Взятие определителя от неквадратной матрицы не должно
//   компилироваться.
// + Метод transposed(), возвращающий транспонированную матрицу.
// + Метод rank() - вычислить ранг матрицы.
// + Метод trace() - вычислить след матрицы.
// + Методы inverted() и invert() - вернуть обратную матрицу и обратить данную матрицу.
// + Методы getRow(unsigned) и getColumn(unsigned), возвращающие std::vector<Field> из соответствующих значений.
// + К матрице должен быть дважды применим оператор [], причём это должно работать как для неконстантных, так и для
//   константных матриц. В первом случае содержимое матрицы должно быть можно таким способом поменять.
// + Другие способы изменения содержимого матрицы, кроме описанных выше, должны отсутствовать. Однако не запрещается
//   реализовать дополнительные методы для выполнения каких-либо иных алгебраических операций или для удобства работы,
//   если по названию и сигнатуре этих методов будет без комментариев понятно их действие.
// + Квадратные матрицы размера N должно быть можно объявлять всего с одним обязательным шаблонным параметром:
//   SquareMatrix<N>.
//
// В вашем файле должна отсутствовать функция main(), а сам файл должен называться matrix.h. В качестве компилятора
// необходимо выбирать Make. Ваш код будет вставлен посредством #include в программу, содержащую тесты.

#pragma once

#include <cmath>
#include <cassert>
#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
#include <stdexcept>
#include "biginteger.h"

template<unsigned N>
class Finite {
 public:
  Finite(long long k) : value_(mod(k)) {}
  Finite(const Finite<N> &other) = default;

  static long long mod(long long x) { return ((x % N) >= 0) ? (x % N) : (N + (x % N)); }

  std::string toString() const { return std::to_string(value_); }

  bool operator==(const Finite<N> &other) const { return (value_ == other.value_); }
  bool operator!=(const Finite<N> &other) const { return !((*this) == other); }

  Finite<N> &operator+=(const Finite<N> &other);
  Finite<N> &operator-=(const Finite<N> &other);
  Finite<N> &operator*=(const Finite<N> &other);
  Finite<N> &operator/=(const Finite<N> &other);

  Finite<N> &operator++();
  const Finite<N> operator++(int);
  Finite<N> &operator--();
  const Finite<N> operator--(int);

 private:
  long long value_;
};

template<unsigned N>
Finite<N> &Finite<N>::operator+=(const Finite<N> &other) {
  value_ = mod(value_ + other.value_);
  return (*this);
}

template<unsigned N>
Finite<N> &Finite<N>::operator-=(const Finite<N> &other) {
  value_ = mod(value_ - other.value_);
  return (*this);
}

template<unsigned N>
Finite<N> &Finite<N>::operator*=(const Finite<N> &other) {
  value_ = mod(value_ * other.value_);
  return (*this);
}

template<unsigned N>
Finite<N> operator+(const Finite<N> &lhs, const Finite<N> &rhs) {
  auto copy = lhs;
  copy += rhs;

  return copy;
}

template<unsigned N>
Finite<N> operator-(const Finite<N> &lhs, const Finite<N> &rhs) {
  auto copy = lhs;
  copy -= rhs;

  return copy;
}

template<unsigned N>
Finite<N> operator*(const Finite<N> &lhs, const Finite<N> &rhs) {
  auto copy = lhs;
  copy *= rhs;

  return copy;
}

template<unsigned N>
Finite<N> &Finite<N>::operator/=(const Finite<N> &other) {
  assert(other != Finite<N>(0));

  for (unsigned field_element = 0; field_element < N; field_element++) {
    Finite<N> quotient(field_element);
    if ((*this) == (other * quotient)) {
      (*this) = quotient;
      return (*this);
    }
  }

  throw std::runtime_error("unable to divide: " + toString() + " / " + other.toString() + " is not expressible");
}

template<unsigned N>
Finite<N> operator/(const Finite<N> &lhs, const Finite<N> &rhs) {
  auto copy = lhs;
  copy /= rhs;

  return copy;
}

template<unsigned N>
Finite<N> &Finite<N>::operator++() {
  (*this) += Finite<N>(1);
  return (*this);
}

template<unsigned N>
const Finite<N> Finite<N>::operator++(int) {
  auto copy = (*this);
  (*this) += Finite<N>(1);

  return copy;
}

template<unsigned N>
Finite<N> &Finite<N>::operator--() {
  (*this) -= Finite<N>(1);
  return (*this);
}

template<unsigned N>
const Finite<N> Finite<N>::operator--(int) {
  auto copy = (*this);
  (*this) -= Finite<N>(1);

  return copy;
}

template<unsigned M, unsigned N, typename Field = Rational>
class Matrix {
 public:
  Matrix(std::vector<std::vector<Field>> coefficients);
  Matrix(const std::vector<std::vector<int>> &coefficients);
  explicit Matrix(bool fill_main_diag = true);
  Matrix(const Matrix<M, N, Field> &other) = default;
  Matrix(Matrix<M, N, Field> &&other) noexcept = default;

  Matrix<N, M, Field> transposed() const;
  int rank() const;
  Field trace() const;
  std::vector<Field> getRow(unsigned row) const { return (*this)[row]; }
  std::vector<Field> getColumn(unsigned column) const;

  Field det() const;
  Matrix<N, N, Field> inverted() const;
  void invert();

  Matrix<N, N, Field> &operator*=(const Matrix<N, N, Field> &other);

  Matrix<M, N, Field> &operator=(const Matrix<M, N, Field> &other) = default;

  bool operator==(const Matrix<M, N, Field> &other) const;
  bool operator!=(const Matrix<M, N, Field> &other) const { return !((*this) == other); }

  void operator+=(const Matrix<M, N, Field> &other);
  void operator-=(const Matrix<M, N, Field> &other);
  void operator*=(Field scalar);

  std::vector<Field> &operator[](int row) { return coefficients_[row]; }
  const std::vector<Field> &operator[](int row) const { return coefficients_[row]; }
 protected:
  std::vector<std::vector<Field>> coefficients_;
};

template<unsigned M, unsigned N, typename Field>
Matrix<M, N, Field>::Matrix(std::vector<std::vector<Field>> coefficients) : coefficients_(std::move(coefficients)) {
  assert(coefficients_.size() == M);
  for (const auto &row : coefficients_) {
    assert(row.size() == N);
  }
}

template<unsigned M, unsigned N, typename Field>
Matrix<M, N, Field>::Matrix(const std::vector<std::vector<int>> &coefficients) {
  assert(coefficients.size() == M);
  coefficients_.reserve(M);
  for (const auto &row : coefficients) {
    assert(row.size() == N);
    coefficients_.push_back(std::vector<Field>());
    coefficients_.back().reserve(N);
    for (const auto &element : row) {
      coefficients_.back().emplace_back(element);
    }
  }
}

template<unsigned M, unsigned N, typename Field>
Matrix<M, N, Field>::Matrix(bool fill_main_diag)
  : coefficients_(std::vector<std::vector<Field>>(M, std::vector<Field>(N, Field(0)))) {
  if (fill_main_diag) {
    for (unsigned diag = 0; diag < M; diag++) {
      (*this)[diag][diag] = Field(1);
    }
  }
}

template<unsigned M, unsigned N, typename Field>
Matrix<N, M, Field> Matrix<M, N, Field>::transposed() const {
  Matrix<N, M, Field> transposed(false);
  for (unsigned row = 0; row < M; row++) {
    for (unsigned column = 0; column < N; column++) {
      transposed[column][row] = (*this)[row][column];
    }
  }

  return transposed;
}

template<unsigned M, unsigned N, typename Field>
int Matrix<M, N, Field>::rank() const {
  auto copy = (*this);
  auto short_side_length = std::min(M, N);

  for (unsigned diag = 0; diag < short_side_length; diag++) {
    bool leader_is_zero = (copy[diag][diag] == Field(0));
    if (leader_is_zero) {
      for (unsigned next_row = diag + 1; next_row < M; next_row++) {
        if (copy[next_row][diag] != Field(0)) {
          std::swap(copy[diag], copy[next_row]);
          leader_is_zero = false;
          break;
        }
      }
    }

    if (leader_is_zero) {
      continue;
    }

    for (unsigned next_row = diag + 1; next_row < M; next_row++) {
      auto coefficient = copy[next_row][diag] / copy[diag][diag];
      for (unsigned column = 0; column < N; column++) {
        copy[next_row][column] -= copy[diag][column] * coefficient;
      }
    }
  }

  auto rank = 0;
  std::vector<Field> zero_row(N, Field(0));
  for (unsigned diag = 0; (diag < short_side_length) && (copy[diag] != zero_row); diag++) {
    rank++;
  }

  return rank;
}

template<unsigned M, unsigned N, typename Field>
Field Matrix<M, N, Field>::trace() const {
  Field trace(0);
  auto short_side_length = std::min(M, N);
  for (unsigned diag = 0; diag < short_side_length; diag++) {
    trace += (*this)[diag][diag];
  }

  return trace;
}

template<unsigned M, unsigned N, typename Field>
std::vector<Field> Matrix<M, N, Field>::getColumn(unsigned int column) const {
  std::vector<Field> column_elements;
  column_elements.reserve(M);
  for(const auto &row : coefficients_) {
    column_elements.push_back(row[column]);
  }

  return column_elements;
}

template<unsigned M, unsigned N, typename Field>
Field Matrix<M, N, Field>::det() const {
  static_assert(M == N);

  auto copy = (*this);
  auto swaps_sign = 1;
  for (unsigned diag = 0; diag < N; diag++) {
    bool leader_is_zero = (copy[diag][diag] == Field(0));
    if (leader_is_zero) {
      for (unsigned next_row = diag + 1; next_row < N; next_row++) {
        if (copy[next_row][diag] != Field(0)) {
          std::swap(copy[diag], copy[next_row]);
          swaps_sign *= -1;
          leader_is_zero = false;
          break;
        }
      }
    }

    if (leader_is_zero) {
      return 0;
    }

    for (unsigned next_row = diag + 1; next_row < N; next_row++) {
      auto coefficient = copy[next_row][diag] / copy[diag][diag];
      for (unsigned column = diag; column < N; column++) {
        copy[next_row][column] -= copy[diag][column] * coefficient;
      }
    }
  }

  Field det(1);
  for (unsigned diag = 0; diag < N; diag++) {
    det *= copy[diag][diag];
  }
  det *= Field(swaps_sign);

  return det;
}

template<unsigned M, unsigned N, typename Field>
Matrix<N, N, Field> Matrix<M, N, Field>::inverted() const {
  static_assert(M == N);

  auto copy = (*this);
  Matrix<N, N, Field> inverse;
  for (unsigned diag = 0; diag < N; diag++) {
    if (copy[diag][diag] == Field(0)) {
      for (unsigned next_row = diag + 1; next_row < N; next_row++) {
        if (copy[next_row][diag] != Field(0)) {
          std::swap(copy[diag], copy[next_row]);
          std::swap(inverse[diag], inverse[next_row]);
          break;
        }
      }
    }

    auto leader = copy[diag][diag];
    for (unsigned column = 0; column < N; column++) {
      copy[diag][column] /= leader;
      inverse[diag][column] /= leader;
    }

    for (unsigned other_row = 0; other_row < N; other_row++) {
      if (other_row == diag) {
        continue;
      }

      auto other_row_leader = copy[other_row][diag];
      for (unsigned column = 0; column < N; column++) {
        copy[other_row][column] -= copy[diag][column] * other_row_leader;
        inverse[other_row][column] -= inverse[diag][column] * other_row_leader;
      }
    }
  }

  return inverse;
}

template<unsigned M, unsigned N, typename Field>
void Matrix<M, N, Field>::invert() {
  static_assert(M == N);

  auto inverted = this->inverted();
  (*this) = std::move(inverted);
}

template<unsigned M, unsigned N, typename Field>
Matrix<N, N, Field> &Matrix<M, N, Field>::operator*=(const Matrix<N, N, Field> &other) {
  static_assert(M == N);

  auto product = (*this) * other;
  (*this) = std::move(product);

  return (*this);
}

template<unsigned M, unsigned N, typename Field>
bool Matrix<M, N, Field>::operator==(const Matrix<M, N, Field> &other) const {
  for (unsigned row = 0; row < M; row++) {
    for (unsigned column = 0; column < N; column++) {
      if ((*this)[row][column] != other[row][column]) {
        return false;
      }
    }
  }

  return true;
}

template<unsigned M, unsigned N, typename Field>
void Matrix<M, N, Field>::operator+=(const Matrix<M, N, Field> &other) {
  for (unsigned row = 0; row < M; row++) {
    for (unsigned column = 0; column < N; column++) {
      (*this)[row][column] += other[row][column];
    }
  }
}

template<unsigned M, unsigned N, typename Field>
Matrix<M, N, Field> operator+(const Matrix<M, N, Field> &lhs, const Matrix<M, N, Field> &rhs) {
  auto copy = lhs;
  copy += rhs;

  return copy;
}

template<unsigned M, unsigned N, typename Field>
void Matrix<M, N, Field>::operator-=(const Matrix<M, N, Field> &other) {
  for (unsigned row = 0; row < M; row++) {
    for (unsigned column = 0; column < N; column++) {
      (*this)[row][column] -= other[row][column];
    }
  }
}

template<unsigned M, unsigned N, typename Field>
Matrix<M, N, Field> operator-(const Matrix<M, N, Field> &lhs, const Matrix<M, N, Field> &rhs) {
  auto copy = lhs;
  copy -= rhs;

  return copy;
}

template<unsigned M, unsigned N, typename Field>
void Matrix<M, N, Field>::operator*=(Field scalar) {
  for (auto &row : coefficients_) {
    for (auto &coefficient : row) {
      coefficient *= scalar;
    }
  }
}

template<unsigned M, unsigned N, typename Field>
Matrix<M, N, Field> operator*(const Matrix<M, N, Field> &lhs, Field rhs) {
  auto copy = lhs;
  copy *= rhs;

  return copy;
}

template<unsigned M, unsigned N, typename Field>
Matrix<M, N, Field> operator*(Field lhs, const Matrix<M, N, Field> &rhs) {
  return (rhs * lhs);
}

template<unsigned M, unsigned N, unsigned K, typename Field>
Matrix<M, K, Field> operator*(const Matrix<M, N, Field> &lhs, const Matrix<N, K, Field> &rhs) {
  Matrix<M, K, Field> product(false);
  for (unsigned row = 0; row < M; row++) {
    for (unsigned column = 0; column < K; column++) {
      for (unsigned sum_index = 0; sum_index < N; sum_index++) {
        product[row][column] += lhs[row][sum_index] * rhs[sum_index][column];
      }
    }
  }

  return product;
}

template<unsigned N, typename Field = Rational>
using SquareMatrix = Matrix<N, N, Field>;
