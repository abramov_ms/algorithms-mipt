// Реализуйте структуру данных из n элементов a_1, a_2, …, a_n, поддерживающую следующие операции:
// + присвоить элементу a_i значение j;
// + найти знакочередующуюся сумму на отрезке от l до r включительно (a_l − a_{l + 1} + a_{l + 2} − … ± a_r).
//
// #Входные данные
// В первой строке входного файла содержится натуральное число n (1 ≤ n ≤ 10^5) — длина массива. Во второй строке
// записаны начальные значения элементов (неотрицательные целые числа, не превосходящие 10^4).
// В третьей строке находится натуральное число m (1 ≤ m ≤ 10^5) — количество операций. В последующих m строках записаны
// операции:
// + операция первого типа задается тремя числами 0 i j (1 ≤ i ≤ n, 1 ≤ j ≤ 10^4).
// + операция второго типа задается тремя числами 1 l r (1 ≤ l ≤ r ≤ n).
//
// #Выходные данные
// Для каждой операции второго типа выведите на отдельной строке соответствующую знакочередующуюся сумму.

#include <iostream>
#include <vector>
#include <cassert>

class FenwickTree {
 public:
  explicit FenwickTree(const std::vector<int> &sequence) {
    std::vector<int> pref_sums;
    pref_sums.reserve(sequence.size());
    pref_sums.push_back(sequence.front());
    for (int position = 1; position < sequence.size(); position++) {
      pref_sums.push_back(pref_sums.back() + sequence[position]);
    }

    partial_sums_.reserve(pref_sums.size());
    for (int position = 0; position < pref_sums.size(); position++) {
      partial_sums_.push_back(pref_sums[position]);
      auto prev_prefix_end = f(position) - 1;
      if (prev_prefix_end >= 0) {
        partial_sums_.back() -= pref_sums[prev_prefix_end];
      }
    }
  }

  [[nodiscard]] long long PrefSum(int prefix_end) const {
    assert(prefix_end < Size());

    long long sum = 0;
    for (int position = prefix_end; position >= 0; position = f(position) - 1) {
      sum += partial_sums_[position];
    }

    return sum;
  }

  [[nodiscard]] long long Sum(int left_index, int right_index) const {
    assert(left_index <= right_index);

    return (PrefSum(right_index) - PrefSum(left_index - 1));
  }

  void Update(int index, int new_value) {
    assert(index < Size());

    auto old_value = (*this)[index];
    auto difference = new_value - old_value;
    for (int position = index; position < Size(); position = g(position)) {
      partial_sums_[position] += difference;
    }
  }

  [[nodiscard]] int operator[](int index) const {
    assert(index < Size());

    return static_cast<int>(Sum(index, index));
  }

  [[nodiscard]] int Size() const { return partial_sums_.size(); }

 private:
  static int f(int x) { return (x & (x + 1)); }
  static int g(int x) { return (x | (x + 1)); }

  std::vector<int> partial_sums_;
};

int MinusOnePow(int n) {
  return ((n % 2) == 0) ? 1 : -1;
}

int main() {
  int n;
  std::cin >> n;

  std::vector<int> sequence;
  sequence.reserve(n);
  for (int i = 0; i < n; i++) {
    int a;
    std::cin >> a;

    sequence.push_back(a * MinusOnePow(i));
  }

  FenwickTree fenwick_tree(sequence);

  int m;
  std::cin >> m;
  for (int query = 0; query < m; query++) {
    bool query_type;
    std::cin >> query_type;

    if (query_type) {
      int l, r;
      std::cin >> l >> r;
      l--, r--;

      auto sum = fenwick_tree.Sum(l, r) * MinusOnePow(l);
      std::cout << sum << '\n';
    } else {
      int i, j;
      std::cin >> i >> j;
      i--;

      fenwick_tree.Update(i, j * MinusOnePow(i));
    }
  }
}
