// Даны n точек с весами на плоскости. Каждая задаётся тремя числами x_i, y_i, w_i (координаты и вес). Вам нужно
// обработать m запросов двух типов:
// + get rx ry – посчитать сумму весов точек, у которых x_i <= rx и y_i <= ry.
// + change i z – задать i-й точке новый вес равный z
//
// #Формат входных данных
// На первой строке число n (1 <= n <= 100000). На следующих n строках тройки целых чисел x_i, y_i, w_i
// (0 <= x_i, y_i, w_i < 10^9). Следующая строка содержит количество запросов m (1 <= m <= 300000). На следующих m
// строках описания запросов в формате get rx, ry и change i z. Здесь 1 <= i <= n, а остальные числа целые
// от 0 до (10^9 − 1).
//
// #Формат выходных данных
// Для каждого запроса типа “get” выведите одно целое число на отдельной строке — ответ на запрос.

#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <algorithm>

struct Point {
  Point(int x, int y, int weight = 0) : x(x), y(y), weight(weight) {}

  static bool CompareByX(const Point &lhs, const Point &rhs) { return (lhs.x < rhs.x); }
  static bool CompareByY(const Point &lhs, const Point &rhs) { return (lhs.y < rhs.y); }

  int x;
  int y;
  int weight;
};

int f(int x) {
  return (x & (x + 1));
}

int g(int x) {
  return (x | (x + 1));
}

int BsForPosition(const std::vector<Point> &sequence, const Point &point, bool (*compare)(const Point &, const Point &)) {
  auto upper_bound = std::upper_bound(sequence.begin(), sequence.end(), point, compare);
  if (upper_bound == sequence.begin()) {
    return -1;
  }

  upper_bound--;
  auto position = std::distance(sequence.begin(), upper_bound);

  return position;
}

int main() {
  int n;
  std::cin >> n;

  std::vector<Point> points;
  points.reserve(n);
  for (int i = 0; i < n; i++) {
    int x, y, w;
    std::cin >> x >> y >> w;

    points.emplace_back(x, y, w);
  }

  auto sorted_by_x_points = points;
  std::sort(sorted_by_x_points.begin(), sorted_by_x_points.end(), Point::CompareByX);

  std::vector<std::vector<Point>> dx_segments;
  dx_segments.reserve(n);
  std::vector<std::vector<long long>> outer_fenwick_tree;
  outer_fenwick_tree.reserve(n);
  for (int i = 0; i < n; i++) {
    std::vector<Point> dx_segment(sorted_by_x_points.begin() + f(i), sorted_by_x_points.begin() + i + 1);
    std::sort(dx_segment.begin(), dx_segment.end(), Point::CompareByY);

    std::vector<long long> pref_sums = {dx_segment.front().weight};
    pref_sums.reserve(dx_segment.size());
    for (int j = 1; j < dx_segment.size(); j++) {
      pref_sums.push_back(pref_sums.back() + dx_segment[j].weight);
    }

    std::vector<long long> inner_fenwick_tree;
    inner_fenwick_tree.reserve(pref_sums.size());
    for (int j = 0; j < pref_sums.size(); j++) {
      auto sum = pref_sums[j];
      if (f(j) - 1 >= 0) {
        sum -= pref_sums[f(j) - 1];
      }
      inner_fenwick_tree.push_back(sum);
    }

    dx_segments.push_back(std::move(dx_segment));
    outer_fenwick_tree.push_back(std::move(inner_fenwick_tree));
  }

  int m;
  std::cin >> m;

  for (int j = 0; j < m; j++) {
    std::string query_type;
    std::cin >> query_type;

    if (query_type == "get") {
      int rx, ry;
      std::cin >> rx >> ry;

      Point limit_point(rx, ry);
      auto outer_fenwick_position = BsForPosition(sorted_by_x_points, limit_point, Point::CompareByX);

      if (outer_fenwick_position == -1) {
        std::cout << 0 << '\n';
        continue;
      }

      long long sum = 0;
      for (int x = outer_fenwick_position; x >= 0; x = f(x) - 1) {
        auto &inner_fenwick_tree = outer_fenwick_tree[x];
        auto inner_fenwick_position = BsForPosition(dx_segments[x], limit_point, Point::CompareByY);

        if (inner_fenwick_position == -1) {
          continue;
        }

        for (int y = inner_fenwick_position; y >= 0; y = f(y) - 1) {
          sum += inner_fenwick_tree[y];
        }
      }

      std::cout << sum << '\n';
    } else if (query_type == "change") {
      int i, z;
      std::cin >> i >> z;
      i--;

      auto &point_to_update = points[i];
      auto difference = z - point_to_update.weight;

      auto outer_fenwick_position = BsForPosition(sorted_by_x_points, point_to_update, Point::CompareByX);
      while (sorted_by_x_points[outer_fenwick_position].y != point_to_update.y) {
        outer_fenwick_position--;
      }

      for (int x = outer_fenwick_position; x < n; x = g(x)) {
        auto &inner_fenwick_tree = outer_fenwick_tree[x];
        auto inner_fenwick_position = BsForPosition(dx_segments[x], point_to_update, Point::CompareByY);

        for (int y = inner_fenwick_position; y < inner_fenwick_tree.size(); y = g(y)) {
          inner_fenwick_tree[y] += difference;
        }
      }

      points[i].weight = z;
    }
  }
}
