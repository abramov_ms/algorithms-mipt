// Ваша задача — написать программу, моделирующую простое устройство, которое умеет прибавлять целые значения к
// целочисленным переменным.
//
// #Входные данные
// Входной файл состоит из одной или нескольких строк, описывающих операции. Строка состоит из названия переменной и
// числа, которое к этой переменной надо добавить. Все числа не превосходят 100 по абсолютной величине. Изначально все
// переменные равны нулю. Названия переменных состоят из не более чем 100000 маленьких латинских букв. Размер входного
// файла не превосходит 2 мегабайта.
//
// #Выходные данные
// Для каждой операции выведите на отдельной строке значение соответствующей переменной после выполнения операции.

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <utility>
#include <functional>

class VarsMap {
public:
    VarsMap() : table(1) {}

    int add(const std::string &key, int diff) {
        if (is_overloaded()) {
            rehash();
        }

        auto key_hash = hashing_function(key) % table.size();
        auto &bucket = table[key_hash];

        for (auto& [element_key, element_value] : bucket) {
            if (element_key == key) {
                element_value += diff;
                return element_value;
            }
        }

        bucket.emplace_back(key, diff);
        ++actual_size;

        return bucket.back().second;
    }

private:
    const double loadFactor = 0.95;

    bool is_overloaded() const {
        return actual_size > loadFactor * table.size();
    }

    void rehash() {
        std::vector<std::list<std::pair<std::string, int>>> new_table(actual_size * 2);

        for (const auto &bucket : table) {
            for (const auto &element : bucket) {
                auto key_hash = hashing_function(element.first) % new_table.size();
                auto& new_bucket = new_table[key_hash];
                new_bucket.push_back(element);
            }
        }

        table = std::move(new_table);
    }

    std::hash<std::string> hashing_function;
    std::vector<std::list<std::pair<std::string, int>>> table;
    size_t actual_size = 0;
};

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    VarsMap vars;

    std::string var_name;
    while (std::cin >> var_name) {
        int diff;
        std::cin >> diff;

        auto new_value = vars.add(var_name, diff);
        std::cout << new_value << '\n';
    }
}
