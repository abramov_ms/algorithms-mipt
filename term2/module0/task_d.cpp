// В Триаметистовом королевстве было n городов и m дорог, соединявших некоторые из них друг с другом. Однажды в
// результате экспериментов придворного мага Д произошло катастрофическое расщепление: во вселенной вместо одного
// Триаметистового королевства появлось множество его копий, или отражений. Более того, в каждом из них каждая дорога
// стала заколдованной либо способом α, либо способом ω (cтоит отметить, что каждое из возможных сочетаний
// заколдованностей дорог появилось ровно в одном из отражений).
//
// Свойства заколодованностей α и ω таковы, что города a, b и c образуют торговый союз тогда и только тогда, когда есть
// дороги между a и b, между b и c и между c и a, заколдованные одним и тем же способом.
//
// #Входные данные
// В первой строке входных данных записаны два целых числа n и m — количество городов и дорог Триаметистового
// королевства. В следующих m строках записаны пары чисел, задающие города, соединённые соответствующими дорогами.
// 3 ≤ n ≤ 10000, 1 ≤ m ≤ 100000. Никакие два города не соединены более чем одной дорогой, никакая дорога не соединяет
// город сам с собой.
//
// #Выходные данные
// Выведите как можно точнее единственное вещественное число — среднее количество союзов, которое образовалось во всех
// отражениях Триаметистового королевства.

#include <iostream>
#include <vector>
#include <utility>
#include <iomanip>

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    size_t vertices_count, edges_count;
    std::cin >> vertices_count >> edges_count;

    std::vector<std::vector<bool>> adj_matrix(vertices_count, std::vector<bool>(vertices_count));
    for (size_t edge = 0; edge < edges_count; ++edge) {
        size_t start_vertex, end_vertex;
        std::cin >> start_vertex >> end_vertex;
        --start_vertex, --end_vertex;

        adj_matrix[start_vertex][end_vertex] = adj_matrix[end_vertex][start_vertex] = true;
    }

    size_t triangles_count = 0;
    for (size_t grandparent = 0; grandparent < vertices_count; ++grandparent) {
        for (size_t parent = grandparent + 1; parent < vertices_count; ++parent) {
            if (!adj_matrix[grandparent][parent]) {
                continue;
            }

            for (size_t child = parent + 1; child < vertices_count; ++child) {
                if (adj_matrix[parent][child] && adj_matrix[child][grandparent]) {
                    ++triangles_count;
                }
            }
        }
    }

    long double answer = triangles_count / 4.0L;

    std::cout << std::setprecision(17) << std::fixed;
    std::cout << answer << std::endl;
}
