// Пусть все натуральные числа исходно организованы в список в естественном порядке. Разрешается выполнить следующую
// операцию: swap(a,b). Эта операция возвращает в качестве результата расстояние в текущем списке между числами a и b и
// меняет их местами.
//
// Задана последовательность операций swap. Требуется вывести в выходной файл результат всех этих операций.
//
// #Входные данные
// Первая строка входного файла содержит число n (1 ⩽ n ⩽ 200000) — количество операций. Каждая из следующих n строк
// содержит по два числа в диапазоне от 1 до 10^9 — аргументы операций swap.
//
// #Выходные данные
// Для каждой операции во входном файле выведите ее результат.

#include <iostream>
#include <vector>
#include <utility>

class HashMap {
public:
    typedef std::pair<int, int> value_type;
    typedef std::vector<value_type>::iterator iterator;

    HashMap() : table(1) {}

    std::pair<iterator, bool> insert(const value_type& element) {
        if (actual_size == table.size()) {
            rehash();
        }

        for (size_t run = 0; run < table.size(); ++run) {
            auto key_hash = hash(element.first, run);
            auto& entry = table[key_hash];

            if (entry.first == element.first) {
                return make_pair(table.begin() + key_hash, false);
            }
            if (entry == empty) {
                entry = element;
                ++actual_size;
                return std::make_pair(table.begin() + key_hash, true);
            }
        }
    }

    iterator find(int key) {
        for (size_t run = 0; run < table.size(); ++run) {
            auto key_hash = hash(key, run);
            auto& entry = table[key_hash];

            if (entry == empty) {
                return table.end();
            }
            if (entry.first == key) {
                return table.begin() + key_hash;
            }
        }

        return table.end();
    }

    int& operator[](size_t key) {
        auto position = find(key);
        if (position == table.end()) {
            insert({key, key});
        }

        return position->second;
    }

private:
    static const value_type empty;

    void rehash() {
        auto table_backup = std::move(table);
        table = std::vector<value_type>(actual_size * 2);
        actual_size = 0;

        for (const auto& element : table_backup) {
            if (element != empty) {
                insert(element);
            }
        }
    }

    size_t hash(int key, size_t run) {
        return (primary_hash(key) + run * run_hash(key)) % table.size();
    }

    size_t primary_hash(int key) {
        return key % table.size();
    }

    size_t run_hash(int key) {
        return (key % table.size()) * 2 + 1;
    }

    std::vector<value_type> table;
    size_t actual_size = 0;
};

const HashMap::value_type HashMap::empty = std::pair<int, int>();

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n;
    std::cin >> n;

    HashMap map;

    for (int swap = 0; swap < n; ++swap) {
        int a, b;
        std::cin >> a >> b;

        map.insert({a, a});
        map.insert({b, b});

        auto& a_position = map[a];
        auto& b_position = map[b];

        auto distance = std::abs(a_position - b_position);
        std::cout << distance << '\n';

        std::swap(a_position, b_position);
    }
}
