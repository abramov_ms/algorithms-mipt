// Реализуйте ассоциативный массив с использованием хеш-таблицы. Использовать стандартную библиотеку (set, map,
// LinkedHashMap, и т. п.) не разрешается.
//
// #Входные данные
// Входной файл содержит описание операций, их количество не превышает 100000. В каждой строке находится одна из
// следующих операций:
// + put x y — поставить в соответствие ключу x значение y. Если ключ уже есть, то значение необходимо изменить.
// + delete x — удалить ключ x. Если элемента x нет, то ничего делать не надо.
// + get x — если ключ x есть в ассоциативном массиве, то выведите соответствующее ему значение, иначе выведите «none».
// Ключи и значения — строки из латинских букв длинной не более 20 символов.
//
// #Выходные данные
// Выведите последовательно результат выполнения всех операций get. Следуйте формату выходного файла из примера.

#include <fstream>
#include <vector>
#include <list>
#include <utility>
#include <string>
#include <functional>

class Dictionary {
public:
    typedef std::pair<std::string, std::string> value_type;

    Dictionary() : table(1) {}

    bool put(const value_type &pair) {
        if (is_overloaded()) {
            rehash();
        }

        auto& bucket = get_bucket(pair.first);
        for (auto& element : bucket) {
            if (element.first == pair.first) {
                element.second = pair.second;
                return false;
            }
        }

        bucket.push_back(pair);
        ++actual_size;

        return true;
    }

    bool del(const std::string& key) {
        auto& bucket = get_bucket(key);
        for (auto it = bucket.begin(); it != bucket.end(); ++it) {
            if (it->first == key) {
                bucket.erase(it);
                return true;
            }
        }

        return false;
    }

    std::string get(const std::string& key) {
        auto& bucket = get_bucket(key);
        for (const auto& element : bucket) {
            if (element.first == key) {
                return element.second;
            }
        }

        return std::string();
    }

private:
    static const double loadFactor;
    static const std::hash<std::string> hash_function;

    [[nodiscard]] bool is_overloaded() const {
        return actual_size > loadFactor * table.size();
    }

    void rehash() {
        std::vector<std::list<value_type>> new_table(actual_size * 2);

        for (const auto &bucket : table) {
            for (const auto &element : bucket) {
                auto index = hash_function(element.first) % new_table.size();
                auto &new_bucket = new_table[index];
                new_bucket.push_back(element);
            }
        }

        table = std::move(new_table);
    }

    std::list<value_type>& get_bucket(const std::string &key) {
        auto index = hash_function(key) % table.size();
        return table[index];
    }

    std::vector<std::list<value_type>> table;
    size_t actual_size = 0;
};

const double Dictionary::loadFactor = 0.95;
const std::hash<std::string> Dictionary::hash_function = std::hash<std::string>();

int main() {
    std::ifstream is("map.in");
    std::ofstream os("map.out");

    Dictionary dictionary;

    std::string query;
    while (is >> query) {
        if (query == "put") {
            std::string x, y;
            is >> x >> y;

            dictionary.put({x, y});
        } else if (query == "delete") {
            std::string x;
            is >> x;

            dictionary.del(x);
        } else if (query == "get") {
            std::string x;
            is >> x;

            auto value = dictionary.get(x);
            os << (value.empty() ? "none" : value) << '\n';
        }
    }
}
