// Вам требуется написать программу, которая по заданной последовательности находит максимальную невозрастающую её
// подпоследовательность (т.е такую последовательность чисел a_i1, a_i2, …, a_ik (i1 < i2 < … < ik), что
// a_i1 ≥ a_i2 ≥ … ≥ aik и не существует последовательности с теми же свойствами длиной (k + 1).
//
// #Входные данные
// В первой строке задано число n  — количество элементов последовательности (1 ≤ n ≤ 239017). В последующих строках
// идут сами числа последовательности ai, отделенные друг от друга произвольным количеством пробелов и переводов строки
// (все числа не превосходят по модулю 2^31 − 2).
//
// #Выходные данные
// Вам необходимо выдать в первой строке выходного файла число k  — длину максимальной невозрастающей
// подпоследовательности. В последующих строках должны быть выведены (по одному числу в каждой строке) все номера
// элементов исходной последовательности i_j, образующих искомую подпоследовательность. Номера выводятся в порядке
// возрастания. Если оптимальных решений несколько, разрешается выводить любое.

#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>
#include <stack>

int main() {
    const int negativeInfinity = std::numeric_limits<int>::min();
    
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    size_t seq_length;
    std::cin >> seq_length;
    std::vector<int> sequence;
    sequence.reserve(seq_length);
    for (size_t i = 0; i < seq_length; ++i) {
        int element;
        std::cin >> element;
        sequence.push_back(element);
    }

    std::vector<int> max_endings(seq_length, negativeInfinity);
    std::vector<size_t> endings_indicies(seq_length);
    std::vector<int> prev_element_indicies(seq_length, -1);
    max_endings[0] = sequence[0];

    auto comp = [](const int& x, const int& y) { return x > y; };
    size_t subseq_length = 1;
    for (size_t last_index = 1; last_index < seq_length; ++last_index) {
        size_t last_element = sequence[last_index];
        auto first_smaller = std::upper_bound(max_endings.begin(), max_endings.end(), last_element, comp);
        if (first_smaller != max_endings.end()) {
            if (*first_smaller == negativeInfinity) {
                ++subseq_length;
            }

            *first_smaller = last_element;
            size_t update_index = std::distance(max_endings.begin(), first_smaller);
            endings_indicies[update_index] = last_index;
            if (update_index > 0) {
                prev_element_indicies[last_index] = endings_indicies[update_index - 1];
            }
        }
    }
    
    std::cout << subseq_length << '\n';
    std::stack<int> subseq_indicies;
    for (int index = endings_indicies[subseq_length - 1]; index != -1; index = prev_element_indicies[index]) {
        subseq_indicies.push(index + 1);
    }
    while (!subseq_indicies.empty()) {
        std::cout << subseq_indicies.top() << ' ';
        subseq_indicies.pop();
    }
}
