#include <iostream>
#include <vector>

size_t lcis_length(const std::vector<int>& seq1, const std::vector<int>& seq2) {
    auto seq1_length = seq1.size();
    auto seq2_length = seq2.size();

    std::vector<std::vector<size_t>> lcis_length(seq1_length + 1, std::vector<size_t>(seq2_length));
    std::vector<int> prev_element_index(seq2_length, -1);
    for (size_t count1 = 1; count1 <= seq1_length; ++count1) {
        int best_prev_index = -1;
        size_t max_prev_lcis_length = 0;
        for (size_t pref2 = 0; pref2 < seq2_length; ++pref2) {
            lcis_length[count1][pref2] = lcis_length[count1 - 1][pref2];
            if (seq2[pref2] == seq1[count1 - 1] &&
                max_prev_lcis_length + 1 > lcis_length[count1][pref2]
                    ) {
                lcis_length[count1][pref2] = max_prev_lcis_length + 1;
                prev_element_index[pref2] = best_prev_index;
            }
            if (seq2[pref2] < seq1[count1 - 1] &&
                lcis_length[count1 - 1][pref2] > max_prev_lcis_length
                    ) {
                best_prev_index = pref2;
                max_prev_lcis_length = lcis_length[count1 - 1][pref2];
            }
        }
    }

    size_t best_lcis_end_index = 0;
    for (size_t end_index = 0; end_index < seq2_length; ++end_index) {
        if (lcis_length[seq1_length][end_index] > lcis_length[seq1_length][best_lcis_end_index]) {
            best_lcis_end_index = end_index;
        }
    }

    return lcis_length[seq1_length][best_lcis_end_index];
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    size_t seq1_length, seq2_length;
    std::cin >> seq1_length >> seq2_length;
    std::vector<int> seq1;
    seq1.reserve(seq1_length);
    for (size_t i = 0; i < seq1_length; ++i) {
        int element;
        std::cin >> element;
        seq1.push_back(element);
    }
    std::vector<int> seq2;
    seq2.reserve(seq2_length);
    for (size_t i = 0; i < seq2_length; ++i) {
        int element;
        std::cin >> element;
        seq2.push_back(element);
    }

    std::cout << lcis_length(seq1, seq2) << '\n';
}