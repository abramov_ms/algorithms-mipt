// Найдите максимальный вес золота, который можно унести в рюкзаке вместительностью S, если есть N золотых слитков с
// заданными весами.
// 
// #Входные данные
// В первой строке входного файла запианы два числа — S и N (1⩽S⩽10000, 1⩽N⩽300). Далее следует N неотрицательных целых
// чисел, не превосходящих 100 000 — веса слитков.
// 
// #Выходные данные
// Выведите искомый максимальный вес.

#include <iostream>
#include <vector>
#include <climits>

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    size_t S, N;
    std::cin >> S >> N;
    std::vector<size_t> weights;
    weights.reserve(N);
    for (size_t bar = 0; bar < N; ++bar) {
        size_t bar_weight;
        std::cin >> bar_weight;
        weights.push_back(bar_weight);
    }

    std::vector<std::vector<size_t>> knapsock(N + 1, std::vector<size_t>(S + 1));
    for (size_t max_bar = 1; max_bar <= N; ++max_bar) {
        size_t bar_weight = weights[max_bar - 1];
        for (size_t capacity = 1; capacity <= S; ++capacity) {
            size_t keep_current = knapsock[max_bar - 1][capacity];
            if (bar_weight <= capacity) {
                size_t take_new = knapsock[max_bar - 1][capacity - bar_weight] + bar_weight;
                knapsock[max_bar][capacity] = std::max(take_new, keep_current);
            } else {
                knapsock[max_bar][capacity] = keep_current;
            }
        }
    }

    std::cout << knapsock[N][S] << '\n';
}
