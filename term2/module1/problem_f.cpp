// Даны две последовательности. Найдите длину их наибольшей общей подпоследовательности (подпоследовательность — это
// то, что можно получить из данной последовательности вычеркиванием некоторых элементов).
// 
// #Входные данные
// В первой строке входного файла записано число N  — длина первой последовательности (1 ⩽ N ⩽ 1000). Во второй строке
// записаны члены первой последовательности (через пробел) — целые числа, не превосходящие 10 000 по модулю. В третьей
// строке записано число M — длина второй последовательности (1⩽M⩽1000). В четвертой строке записаны члены второй
// последовательности (через пробел) — целые числа, не превосходящие 10 000 по модулю.
// 
// #Выходные данные
// В выходной файл требуется вывести единственное целое число: длину наибольшей общей подпоследовательности, или число
// 0, если такой не существует.

#include <iostream>
#include <vector>

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);
    
    int N;
    std::cin >> N;
    std::vector<int> seq_1;
    seq_1.reserve(N);
    for (int i = 0; i < N; ++i) {
        int element;
        std::cin >> element;
        seq_1.push_back(element);
    }
    int M;
    std::cin >> M;
    std::vector<int> seq_2;
    seq_2.reserve(M);
    for (int j = 0; j < M; ++j) {
        int element;
        std::cin >> element;
        seq_2.push_back(element);
    }

    std::vector<int> lcs_prev(M + 1);
    for (int pref_1 = 1; pref_1 <= N; ++pref_1) {
        std::vector<int> lcs_current;
        lcs_current.reserve(M + 1);
        lcs_current.push_back(0);
        for (int pref_2 = 1; pref_2 <= M; ++pref_2) {
            lcs_current.push_back(
                seq_1[pref_1 - 1] == seq_2[pref_2 - 1] ?
                lcs_prev[pref_2 - 1] + 1 :
                std::max(lcs_prev[pref_2], lcs_current.back())
            );
        }
        lcs_prev = std::move(lcs_current);
    }

    std::cout << lcs_prev.back() << '\n';
}
