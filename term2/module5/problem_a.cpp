// Требуется найти в связном графе остовное дерево минимального веса.
//
// Воспользуйтесь алгоритмом Прима.
//
// #Входные данные
// Первая строка входного файла содержит два натуральных числа n и m — количество вершин и ребер графа соответственно.
// Следующие m строк содержат описание ребер по одному на строке. Ребро номер i описывается тремя натуральными числами
// b_i, e_i и w_i — номера концов ребра и его вес соответственно (1 ≤ b_i,e_i ≤ n, 0 ≤ w_i ≤ 100000). n ≤ 5000,
// m ≤ 100000.
// Граф является связным.
//
// #Выходные данные
// Первая строка выходного файла должна содержать одно натуральное число — вес минимального остовного дерева.

#include <iostream>
#include <utility>
#include <limits>
#include <vector>
#include <queue>

const size_t kInfinity = std::numeric_limits<size_t>::max();

class Graph {
private:
    std::vector<std::vector<std::pair<size_t, size_t>>> adjacency_list_;

public:
    explicit Graph(size_t vertices_count) : adjacency_list_(vertices_count) {}

    void AddEdge(size_t from, size_t to, size_t weight) {
        adjacency_list_[from].emplace_back(to, weight);
    }

    [[nodiscard]] const std::vector<std::pair<size_t, size_t>>& GetOutEdges(size_t vertex) const
            { return adjacency_list_[vertex]; }

    [[nodiscard]] size_t vertices_count() const { return adjacency_list_.size(); }
};

size_t PrimFindMstWeight(const Graph& graph) {
    std::priority_queue<
            std::pair<size_t, size_t>, std::vector<std::pair<size_t, size_t>>,
            std::greater<std::pair<size_t, size_t>>> prim_queue;
    prim_queue.emplace(0, 0);
    std::vector<bool> visited(graph.vertices_count());

    size_t mst_weight = 0;
    while (!prim_queue.empty()) {
        auto [edge_weight, vertex] = prim_queue.top();
        prim_queue.pop();

        if (!visited[vertex]) {
            mst_weight += edge_weight;
            visited[vertex] = true;

            for (const auto& [to, weight] : graph.GetOutEdges(vertex)) {
                if (!visited[to]) {
                    prim_queue.emplace(weight, to);
                }
            }
        }
    }

    return mst_weight;
}

int main() {
    size_t vertices_count, edges_count;
    std::cin >> vertices_count >> edges_count;

    Graph graph(vertices_count);
    for (size_t edge = 0; edge < edges_count; ++edge) {
        size_t from, to, weight;
        std::cin >> from >> to >> weight;
        --from, --to;

        graph.AddEdge(from, to, weight);
        graph.AddEdge(to, from, weight);
    }

    std::cout << PrimFindMstWeight(graph) << std::endl;
}