// Требуется найти в связном графе остовное дерево минимального веса.
//
// Воспользуйтесь алгоритмом Крускала.
//
// #Входные данные
// Первая строка входного файла содержит два натуральных числа n и m — количество вершин и ребер графа соответственно.
// Следующие m строк содержат описание ребер по одному на строке. Ребро номер i описывается тремя натуральными числами
// b_i, e_i и w_i — номера концов ребра и его вес соответственно (1 ≤ b_i , e_i ≤ n, 0 ≤ w_i ≤ 100000). n ≤ 20000,
// m ≤ 100000.
// Граф является связным.
//
// #Выходные данные
// Первая строка выходного файла должна содержать одно натуральное число — вес минимального остовного дерева.

#include <iostream>
#include <vector>
#include <limits>

const size_t kInfinity = std::numeric_limits<size_t>::max();

class Dsu {
public:
    Dsu(size_t elements_count) : parent_(elements_count, kInfinity), subtree_size_(elements_count, 1) {}

    size_t SetLeader(size_t set_element) {
        if (parent_[set_element] == kInfinity) {
            return set_element;
        } else {
            parent_[set_element] = SetLeader(parent_[set_element]);
            return parent_[set_element];
        }
    }

    void Unite(size_t lhs_set_element, size_t rhs_set_element) {
        auto lhs_set_leader = SetLeader(lhs_set_element);
        auto rhs_set_leader = SetLeader(rhs_set_element);
        if (lhs_set_leader == rhs_set_leader) {
            return;
        }

        if (subtree_size_[lhs_set_leader] < subtree_size_[rhs_set_leader]) {
            std::swap(lhs_set_leader, rhs_set_leader);
        }
        parent_[rhs_set_leader] = lhs_set_leader;
        subtree_size_[lhs_set_leader] += subtree_size_[rhs_set_leader];
    }

private:
    std::vector<size_t> parent_;
    std::vector<size_t> subtree_size_;
};

class Graph {
public:
    static const size_t kMaxEdgeWeight = 100'000;

    struct Edge {
        size_t from;
        size_t to;
        size_t weight;
    };

    Graph(size_t vertices_count, size_t edges_count_to_reserve = 0) : vertices_count_(vertices_count) {
        edges_.reserve(edges_count_to_reserve);
    }

    void AddEdge(size_t from, size_t to, size_t weight) {
        edges_.push_back({from, to, weight});
    }

    [[nodiscard]] const std::vector<Edge>& edges() const { return edges_; }
    [[nodiscard]] size_t vertices_count() const { return vertices_count_; }

private:
    size_t vertices_count_;
    std::vector<Edge> edges_;
};

std::vector<Graph::Edge> SortedEdges(const Graph& graph) {
    std::vector<size_t> counts(Graph::kMaxEdgeWeight + 1);
    for (const auto& [from, to, weight] : graph.edges()) {
        ++counts[weight];
    }

    std::vector<size_t> block_start_indices;
    block_start_indices.reserve(counts.size());
    block_start_indices.push_back(0);
    for (size_t count = 1; count < counts.size(); ++count) {
        block_start_indices.push_back(block_start_indices[count - 1] + counts[count - 1]);
    }

    std::vector<Graph::Edge> sorted(graph.edges().size());
    for (const auto& [from, to, weight] : graph.edges()) {
        sorted[block_start_indices[weight]] = {from, to, weight};
        ++block_start_indices[weight];
    }

    return sorted;
}

size_t KruskalFindMstWeight(const Graph& graph) {
    auto sorted_edges = SortedEdges(graph);
    auto dsu = Dsu(graph.vertices_count());

    size_t mst_weight = 0;
    for (const auto& [from, to, weight] : sorted_edges) {
        if (dsu.SetLeader(from) != dsu.SetLeader(to)) {
            dsu.Unite(from, to);
            mst_weight += weight;
        }
    }

    return mst_weight;
}

int main() {
    size_t vertices_count, edges_count;
    std::cin >> vertices_count >> edges_count;

    Graph graph(vertices_count, edges_count);
    for (size_t edge = 0; edge < edges_count; ++edge) {
        size_t from, to, weight;
        std::cin >> from >> to >> weight;
        --from, --to;

        graph.AddEdge(from, to, weight);
    }

    std::cout << KruskalFindMstWeight(graph) << std::endl;
}