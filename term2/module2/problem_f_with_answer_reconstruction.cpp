#include <iostream>
#include <algorithm>
#include <vector>
#include <limits>

struct PathState {
    size_t length;
    size_t prev_node;
    size_t prev_mask;
};

std::pair<size_t, std::vector<size_t>> find_min_hamiltonian_path(size_t nodes_count,
                                                                 const std::vector<std::vector<size_t>>& graph) {
    size_t inf = std::numeric_limits<size_t>::max();
    size_t subsets_count = 1ULL << nodes_count;
    std::vector<std::vector<PathState>> hamiltonian_path(
            nodes_count,
            std::vector<PathState>(subsets_count, {inf, inf, 0})
    );

    for (size_t node = 0, node_bit = 1; node < nodes_count; ++node, node_bit <<= 1) {
        hamiltonian_path[node][node_bit].length = 0;
    }
    for (size_t mask = 0; mask < subsets_count; ++mask) {
        for (size_t last_node = 0, last_node_bit = 1; last_node < nodes_count; ++last_node, last_node_bit <<= 1) {
            if ((last_node_bit & mask) == 0) {
                continue;
            }
            for (size_t next_node = 0, next_node_bit = 1; next_node < nodes_count; ++next_node, next_node_bit <<= 1) {
                if ((next_node_bit & mask) != 0) {
                    continue;
                }

                size_t new_path_length = hamiltonian_path[last_node][mask].length + graph[last_node][next_node];
                size_t new_mask = mask | next_node_bit;
                if (new_path_length < hamiltonian_path[next_node][new_mask].length) {
                    hamiltonian_path[next_node][new_mask] = {new_path_length, last_node, mask};
                }
            }
        }
    }

    auto min_hamiltonian_path = hamiltonian_path[0][subsets_count - 1];
    size_t min_path_last_node = 0;
    for (size_t last_node = 1; last_node < nodes_count; ++last_node) {
        if (hamiltonian_path[last_node][subsets_count - 1].length < min_hamiltonian_path.length) {
            min_hamiltonian_path = hamiltonian_path[last_node][subsets_count - 1];
            min_path_last_node = last_node;
        }
    }

    std::vector<size_t> path;
    size_t node = min_path_last_node;
    size_t mask = subsets_count - 1;
    while (node != inf) {
        path.push_back(node + 1);
        size_t prev_node = hamiltonian_path[node][mask].prev_node;
        size_t prev_mask = hamiltonian_path[node][mask].prev_mask;
        node = prev_node;
        mask = prev_mask;
    }

    return std::make_pair(min_hamiltonian_path.length, path);
}

int main() {
    size_t nodes_count;
    std::cin >> nodes_count;

    std::vector<std::vector<size_t>> graph(nodes_count, std::vector<size_t>(nodes_count));
    for (size_t from = 0; from < nodes_count; ++from) {
        for (size_t to = 0; to < nodes_count; ++to) {
            size_t distance;
            std::cin >> distance;
            graph[from][to] = distance;
        }
    }

    auto min_hamiltonian_path = find_min_hamiltonian_path(nodes_count, graph);
    std::cout << min_hamiltonian_path.first << '\n';
    for (size_t node : min_hamiltonian_path.second) {
        std::cout << node << ' ';
    }
    std::cout << '\n';
}
