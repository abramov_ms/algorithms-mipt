#include "matrix.h"

using Field = Finite<999'999'937>;

Matrix<5, 5, Field> binPower(const Matrix<5, 5, Field> &matrix, size_t power) {
    Matrix<5, 5, Field> result;
    Matrix<5, 5, Field> current_power = matrix;
    for (size_t bit = 1; bit <= power; bit <<= 1) {
        if (power & bit) {
            result *= current_power;
        }
        current_power *= current_power;
    }

    return result;
}

Field dna_combinations_count(const Matrix<5, 5, Field>& correctness_matrix, const Matrix<5, 1, Field>& initial_column,
                             size_t dna_length) {
    auto power = binPower(correctness_matrix, dna_length - 1);
    auto dna_combinations = power * initial_column;
    Field combinations_count = 0;
    for (size_t row = 0; row < 5; ++row) {
        combinations_count += dna_combinations[row][0];
    }

    return combinations_count;
}

int main() {
    const Matrix<5, 5, Field> correctness_matrix({
        {1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1},
        {1, 1, 1, 0, 0},
        {1, 1, 1, 1, 1},
        {1, 1, 1, 0, 0}
    });
    const Matrix<5, 1, Field> initial_column({
        {1},
        {1},
        {1},
        {1},
        {1}
    });

    size_t dna_length;
    for (std::cin >> dna_length; dna_length != 0; std::cin >> dna_length) {
        std::cout << dna_combinations_count(correctness_matrix, initial_column, dna_length).toString() << '\n';
    }
}