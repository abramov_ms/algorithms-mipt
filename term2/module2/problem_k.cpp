#include <iostream>
#include <vector>

size_t solve(size_t rows_count, size_t columns_count) {
    size_t masks_count = 1ULL << (rows_count + 1);
    size_t oldest_bit = 1ULL << rows_count;

    if (columns_count == 1) {
        return masks_count / 2;
    }

    std::vector<std::vector<size_t>> prev_patterns(rows_count, std::vector<size_t>(masks_count)),
            patterns(rows_count, std::vector<size_t>(masks_count));

    for (size_t column = 1; column < columns_count; ++column) {
        if (column == 1) {
            for (size_t mask = 0; mask < masks_count; ++mask) {
                patterns[0][mask] = 1;
            }
        } else {
            for (size_t mask = 0; mask < masks_count; ++mask) {
                size_t without_oldest_bit = mask >> 1;
                size_t with_oldest_bit = without_oldest_bit | oldest_bit;
                patterns[0][mask] = prev_patterns[rows_count - 1][without_oldest_bit] +
                                    prev_patterns[rows_count - 1][with_oldest_bit];
            }
        }

        for (size_t row = 1; row < rows_count; ++row) {
            for (size_t mask = 0; mask < masks_count; ++mask) {
                size_t with_same_bits = mask;
                size_t with_inverse_bit = mask ^ (1 << row);
                patterns[row][mask] = patterns[row - 1][with_inverse_bit];

                bool prev_bit = mask & (1 << (row - 1));
                bool next_bit = mask & (1 << (row + 1));
                bool current_bit = mask & (1 << row);
                if (prev_bit != current_bit || next_bit != current_bit) {
                    patterns[row][mask] += patterns[row - 1][with_same_bits];
                }
            }
        }

        prev_patterns = patterns;
    }

    size_t patterns_count = 0;
    for (size_t mask = 0; mask < masks_count; ++mask) {
        patterns_count += patterns[rows_count - 1][mask];
    }

    return patterns_count;
}

int main() {
    size_t rows_count, columns_count;
    std::cin >> rows_count >> columns_count;

    auto patterns_count = solve(rows_count, columns_count);

    std::cout << patterns_count << '\n';
}
