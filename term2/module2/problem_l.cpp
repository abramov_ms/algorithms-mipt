#include "biginteger.h"

class Matrix {
public:
    Matrix(size_t height, size_t width): height_(height), width_(width), matrix_(height, std::vector<size_t>(width)) {}
    Matrix(const Matrix& other) = default;
    Matrix(Matrix&& other) = default;

    Matrix& operator=(const Matrix& other) = default;

    std::vector<size_t>& operator[](size_t row) {
        return matrix_[row];
    }

    const std::vector<size_t>& operator[](size_t row) const {
        return matrix_[row];
    }

    [[nodiscard]] size_t height() const {
        return height_;
    }

    [[nodiscard]] size_t width() const {
        return width_;
    }

private:
    size_t height_;
    size_t width_;
    std::vector<std::vector<size_t>> matrix_;
};

Matrix product(const Matrix& lhs, const Matrix& rhs, size_t mod) {
    if (lhs.width() != rhs.height()) {
        throw std::logic_error("cannot multiply incompatible matrices");
    }

    Matrix prod(lhs.height(), rhs.width());
    for (size_t row = 0; row < prod.height(); ++row) {
        for (size_t column = 0; column < prod.width(); ++column) {
            size_t sum = 0;
            for (size_t inner_index = 0; inner_index < lhs.width(); ++inner_index) {
                sum += lhs[row][inner_index] * rhs[inner_index][column];
                sum %= mod;
            }
            prod[row][column] = sum;
        }
    }

    return prod;
}

void fillCorrectnessMatrix(Matrix& matrix) {
    if (matrix.height() != matrix.width()) {
        throw std::logic_error("cannot fill non-square matrix");
    }

    size_t masks_count = matrix.height();
    for (size_t prev_mask = 0; prev_mask < masks_count; ++prev_mask) {
        for (size_t next_mask = 0; next_mask < masks_count; ++next_mask) {
            bool step_is_correct = true;
            bool preceding_prev_mask_bit = prev_mask & 1;
            bool preceding_next_mask_bit = next_mask & 1;
            for (size_t bit = 2; bit < masks_count; bit <<= 1) {
                bool prev_mask_bit = prev_mask & bit;
                bool next_mask_bit = next_mask & bit;
                if (preceding_prev_mask_bit == preceding_next_mask_bit &&
                    preceding_next_mask_bit == prev_mask_bit &&
                    prev_mask_bit == next_mask_bit
                ) {
                    step_is_correct = false;
                    break; 
                }
                preceding_prev_mask_bit = prev_mask_bit;
                preceding_next_mask_bit = next_mask_bit;
            }
            matrix[prev_mask][next_mask] = step_is_correct;
        }
    }
}

Matrix binPower(Matrix& matrix, BigInteger& power, size_t mod) {
    Matrix result(matrix.height(), matrix.width());
    for (size_t diag = 0; diag < result.height(); ++diag) {
        result[diag][diag] = 1;
    }

    while (power > 0) {
        if (!power.IsEven()) {
            result = product(result, matrix, mod);
        }
        matrix = product(matrix, matrix, mod);
        power.Halve();
    }

   return result;
}

int main() {
    BigInteger n;
    size_t m, mod;
    std::cin >> n;
    std::cin >> m >> mod;

    --n;
    size_t masks_count = 1 << m;
    Matrix correctness_matrix(masks_count, masks_count);
    fillCorrectnessMatrix(correctness_matrix);
    auto power = binPower(correctness_matrix, n, mod);

    Matrix initial_column(masks_count, 1);
    for (size_t row = 0; row < masks_count; ++row) {
        initial_column[row][0] = 1;
    }

    auto result_column = product(power, initial_column, mod);
    size_t patterns_count = 0;
    for (size_t row = 0; row < masks_count; ++row) {
        patterns_count += result_column[row][0];
        patterns_count %= mod;
    }
    std::cout << patterns_count << '\n';
}

