#include <iostream>
#include <vector>

class Graph {
private:
    std::vector<std::vector<size_t>> adjacency_list_;

public:
    explicit Graph(size_t vertices_count) : adjacency_list_(vertices_count) {}

    void AddEdge(size_t from, size_t to) {
        adjacency_list_[from].push_back(to);
    }

    [[nodiscard]] const std::vector<size_t>& GetChildren(size_t vertex) const { return adjacency_list_[vertex]; }

    [[nodiscard]] size_t vertices_count() const { return adjacency_list_.size(); }
};

enum VertexColor {
    kWhite, kGray, kBlack
};

void dfs(const Graph& graph, size_t vertex, std::vector<VertexColor>& colors,
         std::vector<size_t>& in_times, std::vector<size_t>& out_times, size_t& time) {
    ++time;
    in_times[vertex] = time;
    colors[vertex] = kGray;
    for (size_t child : graph.GetChildren(vertex)) {
        if (colors[child] == kWhite) {
            dfs(graph, child, colors, in_times, out_times, time);
        }
    }

    ++time;
    out_times[vertex] = time;
    colors[vertex] = kBlack;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    size_t vertices_count;
    std::cin >> vertices_count;

    Graph graph(vertices_count);
    size_t root;
    for (size_t vertex = 0; vertex < vertices_count; ++vertex) {
        size_t parent;
        std::cin >> parent;

        if (parent == 0) {
            root = vertex;
        } else {
            --parent;
            graph.AddEdge(parent, vertex);
        }
    }

    std::vector<VertexColor> colors(vertices_count);
    std::vector<size_t> in_times(vertices_count), out_times(vertices_count);
    size_t time = 0;
    dfs(graph, root, colors, in_times, out_times, time);

    size_t queries_count;
    std::cin >> queries_count;
    for (size_t query = 0; query < queries_count; ++query) {
        size_t vertex_1, vertex_2;
        std::cin >> vertex_1 >> vertex_2;
        --vertex_1, --vertex_2;

        std::cout << (in_times[vertex_1] < in_times[vertex_2] && out_times[vertex_2] < out_times[vertex_1]) << '\n';
    }
}