#include <algorithm>
#include <iostream>
#include <vector>

class Graph {
private:
    std::vector<std::vector<size_t>> adjacency_list_;

public:
    explicit Graph(size_t vertices_count) : adjacency_list_(vertices_count) {}

    void AddEdge(size_t from, size_t to) { adjacency_list_[from].push_back(to); }

    [[nodiscard]] std::vector<size_t> GetChildren(size_t vertex) const { return adjacency_list_[vertex]; }

    [[nodiscard]] size_t vertices_count() const { return adjacency_list_.size(); }
};

void dfs(const Graph& graph, size_t vertex, int parent, std::vector<bool>& visited,
         std::vector<size_t>& in_times, std::vector<size_t>& ret, std::vector<size_t>& cut_vertices,
         size_t& time) {
    ++time;
    in_times[vertex] = ret[vertex] = time;

    visited[vertex] = true;
    size_t children_count = 0;
    bool is_cut_vertex = false;
    for (size_t child : graph.GetChildren(vertex)) {
        if (child == parent) {
            continue;
        }

        if (visited[child]) {
            ret[vertex] = std::min(ret[vertex], in_times[child]);
        } else {
            dfs(graph, child, vertex, visited, in_times, ret, cut_vertices, time);
            ++children_count;
            if (((parent != -1 && ret[child] >= in_times[vertex]) ||
                (parent == -1 && children_count >= 2)) && !is_cut_vertex) {
                cut_vertices.push_back(vertex);
                is_cut_vertex = true;
            }
            ret[vertex] = std::min(ret[vertex], ret[child]);
        }
    }
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    size_t vertices_count, edges_count;
    std::cin >> vertices_count >> edges_count;

    Graph graph(vertices_count);
    for (size_t edge = 0; edge < edges_count; ++edge) {
        size_t from, to;
        std::cin >> from >> to;
        --from, --to;

        graph.AddEdge(from, to);
        graph.AddEdge(to, from);
    }

    std::vector<bool> visited(vertices_count);
    std::vector<size_t> in_times(vertices_count), ret(vertices_count);
    std::vector<size_t> cut_vertices;
    size_t time = 0;
    for (size_t vertex = 0; vertex < vertices_count; ++vertex) {
        if (!visited[vertex]) {
            dfs(graph, vertex, -1, visited, in_times, ret, cut_vertices, time);
        }
    }

    std::cout << cut_vertices.size() << '\n';
    std::sort(cut_vertices.begin(), cut_vertices.end());
    for (size_t cut_vertex : cut_vertices) {
        std::cout << cut_vertex + 1 << ' ';
    }
    std::cout << '\n';
}
