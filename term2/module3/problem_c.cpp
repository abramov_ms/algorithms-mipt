#include <iostream>
#include <vector>

class Graph {
private:
    std::vector<std::vector<size_t>> adjacency_list_;

public:
    explicit Graph(size_t vertices_count) : adjacency_list_(vertices_count) {}

    void AddEdge(size_t from, size_t to) {
        adjacency_list_[from].push_back(to);
    }

    [[nodiscard]] const std::vector<size_t>& GetChildren(size_t vertex) const { return adjacency_list_[vertex]; }

    [[nodiscard]] size_t vertices_count() const { return adjacency_list_.size(); }
};

enum VertexColor {
    kWhite, kGray, kBlack
};

bool dfs(const Graph& graph, size_t vertex, std::vector<VertexColor>& colors,
         std::vector<size_t>& parent, std::vector<size_t>& cycle) {
    colors[vertex] = kGray;
    for (const size_t& child : graph.GetChildren(vertex)) {
        if (colors[child] == kGray) {
            cycle.push_back(child + 1);
            for (size_t prev = vertex; prev != child; prev = parent[prev]) {
                cycle.push_back(prev + 1);
            }
            return true;
        }

        if (colors[child] == kWhite) {
            parent[child] = vertex;
            if (dfs(graph, child, colors, parent, cycle)) {
                return true;
            }
        }
    }

    colors[vertex] = kBlack;

    return false;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    size_t vertices_count, edges_count;
    std::cin >> vertices_count >> edges_count;

    Graph graph(vertices_count);
    for (size_t edge = 0; edge < edges_count; ++edge) {
        size_t from, to;
        std::cin >> from >> to;
        --from, --to;

        graph.AddEdge(from, to);
    }

    std::vector<VertexColor> colors(vertices_count);
    std::vector<size_t> parent(vertices_count);
    parent.reserve(vertices_count);
    for (size_t vertex = 0; vertex < vertices_count; ++vertex) {
        if (colors[vertex] == kWhite) {
            std::vector<size_t> cycle;
            dfs(graph, vertex, colors, parent, cycle);
            if (!cycle.empty()) {
                std::cout << "YES\n";
                while (!cycle.empty()) {
                    std::cout << cycle.back() << ' ';
                    cycle.pop_back();
                }
                return 0;
            }
        }
    }

    std::cout << "NO\n";
}