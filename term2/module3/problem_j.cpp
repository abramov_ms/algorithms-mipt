#include <algorithm>
#include <iostream>
#include <vector>

class Graph {
private:
    std::vector<std::vector<size_t>> adjacency_list_;

public:
    explicit Graph(size_t vertices_count) : adjacency_list_(vertices_count) {}

    void AddEdge(size_t from, size_t to) { adjacency_list_[from].push_back(to); }

    std::vector<size_t>& operator[](size_t vertex) {
        return adjacency_list_[vertex];
    }

    [[nodiscard]] size_t vertices_count() const { return adjacency_list_.size(); }
};


void euler(Graph& graph, size_t vertex, std::vector<size_t>& path) {
    while (!graph[vertex].empty()) {
        size_t to = graph[vertex].back();
        graph[vertex].pop_back();
        euler(graph, to, path);
    }
    path.push_back(vertex);
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    size_t vertices_count, start_planet;
    std::cin >> vertices_count >> start_planet;
    --start_planet;
    Graph graph(vertices_count);
    std::vector<std::vector<size_t>> adj_matrix(vertices_count, std::vector<size_t>(vertices_count));
    for (size_t from = 0; from < vertices_count; ++from) {
        for (size_t to = 0; to < vertices_count; ++to) {
            bool edge_exists;
            std::cin >> edge_exists;
            adj_matrix[from][to] = edge_exists;
            if (!edge_exists && from != to) {
                graph.AddEdge(from, to);
            }
        }
    }

    std::vector<size_t> path;
    euler(graph, start_planet, path);

    for (size_t planet = path.size() - 1; planet >= 1; --planet) {
        std::cout << path[planet] + 1 << ' ' << path[planet - 1] + 1 << '\n';
    }
}