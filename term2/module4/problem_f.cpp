// Дан взвешенный неориентированный граф. Требуется найти вес минимального пути между двумя вершинами.
//
// #Входные данные
// Первая строка входного файла содержит два натуральных числа n и m — количество вершин и ребер графа соответственно.
// Вторая строка входного файла содержит натуральные числа s и t — номера вершин, длину пути между которыми требуется
// найти (1 ≤ s, t ≤ n, s ≠ t).
// Следующие m строк содержат описание ребер по одному на строке. Ребро номер i описывается тремя натуральными числами
// b_i, e_i и w_i — номера концов ребра и его вес соответственно (1 ≤ b_i, e_i ≤ n, 0 ≤ w_i ≤ 100).
// n ≤ 100000, m ≤ 200000.
//
// #Выходные данные
// Первая строка выходного файла должна содержать одно натуральное число — вес минимального пути между вершинами s и t.
// Если путь из s в t не существует, выведите -1.

#include <iostream>
#include <limits>
#include <vector>
#include <queue>
#include <utility>

const size_t kInfinity = std::numeric_limits<size_t>::max();

class Graph {
private:
    std::vector<std::vector<std::pair<size_t, size_t>>> adjacency_list_;

public:
    explicit Graph(size_t vertices_count) : adjacency_list_(vertices_count) {}

    void AddEdge(size_t from, size_t to, size_t weight) {
        adjacency_list_[from].push_back({to, weight});
    }

    [[nodiscard]] const std::vector<std::pair<size_t, size_t>>& GetOutEdges(size_t vertex) const {
        return adjacency_list_[vertex];
    }

    [[nodiscard]] size_t vertices_count() const { return adjacency_list_.size(); }
};

size_t MinDistance(size_t vertices_count, size_t begin, size_t end,
                   const Graph& graph) {
    auto comparator = [](const std::pair<size_t, size_t>& lhs, const std::pair<size_t, size_t>& rhs) {
        return lhs.second > rhs.second;
    };
    std::priority_queue<std::pair<size_t, size_t>, std::vector<std::pair<size_t, size_t>>, decltype(comparator)>
            vertices(comparator);
    vertices.emplace(begin, 0);
    std::vector<size_t> distances(vertices_count, kInfinity);
    distances[begin] = 0;

    while (!vertices.empty()) {
        auto closest_vertex = vertices.top();
        vertices.pop();

        for (const auto& [to, weight] : graph.GetOutEdges(closest_vertex.first)) {
            if (closest_vertex.second + weight < distances[to]) {
                distances[to] = closest_vertex.second + weight;
                vertices.emplace(to, distances[to]);
            }
        }
    }

    return distances[end];
}

int main() {
    size_t vertices_count, edges_count;
    std::cin >> vertices_count >> edges_count;

    size_t begin, end;
    std::cin >> begin >> end;
    --begin, --end;

    Graph graph(vertices_count);
    for (size_t edge = 0; edge < edges_count; ++edge) {
        size_t from, to;
        std::cin >> from >> to;
        --from, --to;

        size_t weight;
        std::cin >> weight;

        graph.AddEdge(from, to, weight);
        graph.AddEdge(to, from, weight);
    }

    auto min_distance = MinDistance(vertices_count, begin, end, graph);
    if (min_distance == kInfinity) {
        std::cout << -1;
    } else {
        std::cout << min_distance;
    }
}
