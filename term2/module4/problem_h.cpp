// Дано две перестановки чисел от 1 до n.
//
// Определите минимальное количество операций разворота на отрезке (изменения порядка нескольких подряд идущих чисел на
// противоположный) первой перестановки, переводящих ее во вторую.
//
// #Входные данные
// Первая строка содержит размер перестановки n (1 ⩽ n ⩽ 10).
// Следующие две строки задают элементы перестановок.
//
// #Выходные данные
// Выведите неотрицательное целое число — ответ на вопрос задачи.
//
// Гарантируется, что хотя бы одна последовательность разворотов, переводящих первую перестановку во вторую, существует.

#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <optional>

using Permutation = uint_fast64_t;

Permutation ReadPermutation(size_t permutation_size) {
    Permutation permutation;
    std::cin >> permutation;
    permutation %= 10;
    for (int i = 1; i < permutation_size; ++i) {
        int digit;
        std::cin >> digit;
        permutation = permutation * 10 + digit % 10;
    }

    return permutation;
}

std::vector<int> BitsCount(size_t permutation_size) {
    std::vector<int> bits_count;
    bits_count.reserve(permutation_size);

    int masks_count = 1ULL << permutation_size;
    for (int mask = 0; mask < masks_count; ++mask) {
        int count = 0;
        for (int bit = 1; bit <= mask; bit <<= 1) {
            if (mask & bit) {
                ++count;
            }
        }
        bits_count.push_back(count);
    }

    return bits_count;
}

std::vector<int> Factorials(size_t permutation_size) {
    std::vector<int> factorials;
    factorials.reserve(permutation_size + 1);

    size_t factor = 1;
    factorials.push_back(factor);
    for (size_t i = 1; i <= permutation_size; ++i) {
        factor *= i;
        factorials.push_back(factor);
    }

    return factorials;
}

size_t PermutationNumber(Permutation permutation, size_t permutation_size, const std::vector<int>& bits_count,
                         const std::vector<int>& factorials) {
    size_t number = 0;
    uint16_t used_digits = 0;
    for (size_t i = 0; i < permutation_size; ++i) {
        size_t digit = permutation % 10;
        if (digit == 0) {
            digit = 10;
        }
        permutation /= 10;

        auto prev_not_seen_digits_count = digit - 1 - bits_count[((1ULL << digit) - 1) & used_digits];
        number += prev_not_seen_digits_count * factorials[permutation_size - i - 1];

        used_digits |= 1ULL << digit;
    }

    return number;
}

std::vector<int> PermutationToDigits(Permutation permutation, size_t permutation_size) {
    std::vector<int> digits;
    digits.reserve(permutation_size);
    for (int digit = 0; digit < permutation_size; ++digit) {
        digits.push_back(permutation % 10);
        permutation /= 10;
    }
    std::reverse(digits.begin(), digits.end());

    return digits;
}

struct BfsConfig {
    std::queue<std::pair<Permutation, size_t>>& bfs_queue;
    std::vector<bool>& visited;
    std::vector<int>& distances;
    std::vector<int>& other_distances;
    const std::vector<int>& target;
};

std::optional<long long> DoBfsStep(BfsConfig& config, size_t permutation_size,
                                   const std::vector<int>& bits_count, const std::vector<int>& factorials) {
    auto min_distance = config.distances[config.bfs_queue.front().second];

    while (!config.bfs_queue.empty()) {
        auto[permutation, permutation_number] = config.bfs_queue.front();
        auto distance = config.distances[permutation_number];
        if (distance != min_distance) {
            break;
        }
        config.bfs_queue.pop();

        std::vector<int> digits = PermutationToDigits(permutation, permutation_size);
        int prefix, suffix;
        for (prefix = 0; digits[prefix] == config.target[prefix]; ++prefix) { /* intentionally do nothing */ }
        for (suffix = 0;
             digits[permutation_size - suffix - 1] == config.target[permutation_size - suffix - 1];
             ++suffix) { /* intentionally do nothing */ }

        for (auto begin = digits.begin() + prefix; begin != digits.end() - 1 - suffix; ++begin) {
            for (auto end = begin + 1; end != digits.end() - suffix; ++end) {
                std::reverse(begin, end + 1);
                Permutation reversal = digits[0];
                for (int digit = 1; digit < permutation_size; ++digit) {
                    reversal = reversal * 10 + digits[digit];
                }
                std::reverse(begin, end + 1);

                auto reversal_number = PermutationNumber(reversal, permutation_size, bits_count, factorials);
                if (!config.visited[reversal_number]) {
                    config.distances[reversal_number] = distance + 1;
                    config.bfs_queue.push({reversal, reversal_number});
                    config.visited[reversal_number] = true;
                }
                if (config.other_distances[reversal_number] >= 0) {
                    return reversal_number;
                }
            }
        }
    }

    return std::optional<long long>();
}

int main() {
    size_t permutation_size;
    std::cin >> permutation_size;
    std::vector<int> bits_count = BitsCount(permutation_size);
    std::vector<int> factorials = Factorials(permutation_size);

    auto begin = ReadPermutation(permutation_size);
    auto begin_number = PermutationNumber(begin, permutation_size, bits_count, factorials);
    auto begin_digits = PermutationToDigits(begin, permutation_size);
    auto end = ReadPermutation(permutation_size);
    auto end_number = PermutationNumber(end, permutation_size, bits_count, factorials);
    auto end_digits = PermutationToDigits(end, permutation_size);

    if (begin == end) {
        std::cout << 0;
        return 0;
    }

    std::queue<std::pair<Permutation, size_t>> bfs_queue_from_begin, bfs_queue_from_end;
    bfs_queue_from_begin.push({begin, begin_number});
    bfs_queue_from_end.push({end, end_number});
    std::vector<int> distances_from_begin(factorials.back(), -1), distances_from_end(factorials.back(), -1);
    distances_from_begin[begin_number] = 0;
    distances_from_end[end_number] = 0;
    std::vector<bool> visited_from_begin(factorials.back()), visited_from_end(factorials.back());

    std::optional<long long> intersection;
    int step = 0;
    BfsConfig from_begin = { bfs_queue_from_begin, visited_from_begin, distances_from_begin, distances_from_end,
                             end_digits };
    BfsConfig from_end = { bfs_queue_from_end, visited_from_end, distances_from_end, distances_from_begin,
                           begin_digits };
    while (true) {
        auto& current_config = step % 2 == 0 ? from_begin : from_end;

        intersection = DoBfsStep(current_config, permutation_size, bits_count, factorials);
        if (intersection.has_value()) {
            std::cout << distances_from_begin[intersection.value()] + distances_from_end[intersection.value()];
            return 0;
        }

        if (++step >= permutation_size - 1) {
            std::cout << permutation_size - 1;
            return 0;
        }
    }
}