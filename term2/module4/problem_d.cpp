// Дан взвешенный ориентированный граф и вершина s в нем. Требуется для каждой вершины u найти длину кратчайшего пути из
// s в u.

// #Входные данные
// Первая строка входного файла содержит n, m и s — количество вершин, ребер и номер выделенной вершины соответственно
// (2 ≤ n ≤ 2000, 1 ≤ m ≤ 6000).
// Следующие m строк содержат описание ребер. Каждое ребро задается стартовой вершиной, конечной вершиной и весом ребра.
// Вес каждого ребра — целое число, не превосходящее 10^15 по модулю. В графе могут быть кратные ребра и петли.

// #Выходные данные
// Выведите n строк — для каждой вершины u выведите длину кратчайшего пути из s в u, '*' если не существует путь из s в
// u и '-' если не существует кратчайший путь из s в u.

#include <iostream>
#include <vector>
#include <limits>

const long long kPositiveInfinity = std::numeric_limits<long long>::max();
const long long kNegativeInfinity = std::numeric_limits<long long>::min();

enum VertexColor {
    kWhite, kGray, kBlack
};

class Graph {
public:
    struct InEdge {
        size_t from;
        long long weight;
    };

    struct OutEdge {
        size_t to;
        long long weight;
    };

    explicit Graph(size_t vertices_count) : in_edges_(vertices_count), out_edges_(vertices_count) {}

    void AddInEdge(size_t to, InEdge edge) { in_edges_[to].push_back(edge); }
    void AddOutEdge(size_t from, OutEdge edge) { out_edges_[from].push_back(edge); }

    [[nodiscard]] const std::vector<InEdge>& GetInEdges(size_t vertex) const { return in_edges_[vertex]; }
    [[nodiscard]] const std::vector<OutEdge>& GetOutEdges(size_t vertex) const { return out_edges_[vertex]; }

private:
    std::vector<std::vector<InEdge>> in_edges_;
    std::vector<std::vector<OutEdge>> out_edges_;
};

void DfsMarkNegativeCycleVertices(size_t vertex, const Graph& graph, std::vector<VertexColor>& colors,
                                  std::vector<long long>& min_distances) {
    colors[vertex] = VertexColor::kGray;
    min_distances[vertex] = kNegativeInfinity;

    for (const auto& edge : graph.GetOutEdges(vertex)) {
        if (colors[edge.to] == kWhite) {
            DfsMarkNegativeCycleVertices(edge.to, graph, colors, min_distances);
        }
    }

    colors[vertex] = VertexColor::kBlack;
}

std::vector<long long> FindMinDistances(size_t vertices_count, size_t start_vertex, const Graph& graph) {
    std::vector<long long> prev_min_distances(vertices_count, kPositiveInfinity);
    prev_min_distances[start_vertex] = 0;
    for (size_t i = 1; i < vertices_count; ++i) {
        std::vector<long long> min_distances;
        min_distances.reserve(vertices_count);
        for (size_t vertex = 0; vertex < vertices_count; ++vertex) {
            auto new_min_distance = prev_min_distances[vertex];
            for (const auto& edge : graph.GetInEdges(vertex)) {
                if (prev_min_distances[edge.from] != kPositiveInfinity) {
                    new_min_distance = std::min(new_min_distance, prev_min_distances[edge.from] + edge.weight);
                }
            }
            min_distances.push_back(new_min_distance);
        }

        prev_min_distances = std::move(min_distances);
    }

    std::vector<size_t> negative_cycle_vertices;
    for (size_t vertex = 0; vertex < vertices_count; ++vertex) {
        auto new_min_distance = prev_min_distances[vertex];
        for (const auto& edge : graph.GetInEdges(vertex)) {
            if (prev_min_distances[edge.from] != kPositiveInfinity) {
                new_min_distance = std::min(new_min_distance, prev_min_distances[edge.from] + edge.weight);
            }
        }

        if (new_min_distance != prev_min_distances[vertex]) {
            negative_cycle_vertices.push_back(vertex);
        }
    }

    auto& min_distances = prev_min_distances;
    std::vector<VertexColor> colors(vertices_count, VertexColor::kWhite);
    for (size_t vertex : negative_cycle_vertices) {
        DfsMarkNegativeCycleVertices(vertex, graph, colors, min_distances);
    }

    return min_distances;
}

int main() {
    size_t vertices_count;
    std::cin >> vertices_count;
    size_t edges_count;
    std::cin >> edges_count;
    size_t start_vertex;
    std::cin >> start_vertex;
    --start_vertex;

    Graph graph(vertices_count);
    for (size_t edge = 0; edge < edges_count; ++edge) {
        size_t from;
        std::cin >> from;
        --from;
        size_t to;
        std::cin >> to;
        --to;
        long long weight;
        std::cin >> weight;

        graph.AddInEdge(to, {from, weight});
        graph.AddOutEdge(from, {to, weight});
    }

    for (long long distance : FindMinDistances(vertices_count, start_vertex, graph)) {
        if (distance == kPositiveInfinity) {
            std::cout << "*\n";
        } else if (distance == kNegativeInfinity) {
            std::cout << "-\n";
        } else {
            std::cout << distance << '\n';
        }
    }
}
