// Даны неотрицательные целые числа N, K и массив целых чисел из диапазона [0, 10^9] размера N.
// Требуется найти K-ю порядковую статистику, т.е. напечатать число, которое бы стояло на позиции
// с индексом K ∈ [0, N − 1] в отсортированном массиве.
//
// Напишите нерекурсивный алгоритм. Требования к дополнительной памяти: O(1). Требуемое среднее время работы: O(N).
//
// #Входные данные
// В первой строке записаны N и K.
// В N последующих строках записаны числа последовательности.
//
// #Выходные данные
// K-я порядковая статистика.

#include <iostream>
#include <vector>
#include <ctime>

int HoarePartition(std::vector<int> &array, int left, int right) {
  std::srand(std::time(nullptr));
  int pivot = array[left + std::rand() % (right - left + 1)];

  int i = left - 1;
  int j = right + 1;

  while (true) {
    do {
      j--;
    } while (array[j] > pivot);

    do {
      i++;
    } while (array[i] < pivot);

    if (i < j) {
      std::swap(array[i], array[j]);
    } else {
      return j;
    }
  }
}

// Чтобы совсем честно выполнить O(1) по памяти, нужно передавать sequence по ссылке,
// но тогда функция будет делать что-то страшное с вектором, кроме поиска нужной статистики.
// Это неочевидное поведение.
int KthOrderStatistics(std::vector<int> &sequence, int k) {
  int left = 0;
  int right = sequence.size() - 1;

  while (left != right) {
    int pivot = HoarePartition(sequence, left, right);

    // Все, что стоит на позиции <= pivot, автоматически не больше всего, что стоит на позиции >= (pivot + 1).
    // Можем посмотреть на array[pivot + 1]. Все элементы левее не больше него. Если (pivot + 1) <= k,
    // то k-я порядковая статистика не может быть в левой половине => она в правой (правая: [(pivot + 1)..right]).
    // Если (pivot + 1) > k, то k-я порядковая статистика точно в левой половине. (левая половина: [left..pivot].
    if ((pivot - left + 1) >= (k + 1)) {
      right = pivot;
    } else {
      k -= (pivot - left + 1);
      left = pivot + 1;
    }
  }

  return sequence[left];
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);

  int N, K;
  std::scanf("%d%d", &N, &K);

  std::vector<int> sequence;
  sequence.reserve(N);
  for (int i = 0; i < N; i++) {
    int current_element;
    std::scanf("%d", &current_element);
    sequence.push_back(current_element);
  }

  int kth = KthOrderStatistics(sequence, K);
  std::printf("%d", kth);
} 
