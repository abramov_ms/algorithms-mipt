#include <iostream>
#include <string>
#include <vector>

class MinMaxHeap {
  struct Node {
    int value;
    int min_heap_key;
    int max_heap_key;
  };

 public:
  void Insert(int element) {
    Node *node = new Node;
    node->value = element;
    node->min_heap_key = min_heap_.size();
    node->max_heap_key = max_heap_.size();

    min_heap_.push_back(node);
    max_heap_.push_back(node);
    MinSiftUp(min_heap_.size() - 1);
    MaxSiftUp(max_heap_.size() - 1);
  }

  int GetMin() {
    Node *min = min_heap_[0];
    int min_value = min->value;
    int max_heap_key = min->max_heap_key;

    ExtractMin();
    MaxDeleteKey(max_heap_key);
    delete min;

    return min_value;
  }

  int GetMax() {
    Node *max = max_heap_[0];
    int max_value = max->value;
    int min_heap_key = max->min_heap_key;

    ExtractMax();
    MinDeleteKey(min_heap_key);
    delete max;

    return max_value;
  }

 private:
  void MinSiftUp(int key) {
    while (key != 0 && min_heap_[key]->value <= min_heap_[(key - 1) / 2]->value) {
      std::swap(min_heap_[key], min_heap_[(key - 1) / 2]);
      std::swap(min_heap_[key]->min_heap_key, min_heap_[(key - 1) /2]->min_heap_key);

      key = (key - 1) /2;
    }
  }

  void MaxSiftUp(int key) {
    while (key != 0 && max_heap_[key]->value >= max_heap_[(key - 1) / 2]->value) {
      std::swap(max_heap_[key], max_heap_[(key - 1) / 2]);
      std::swap(max_heap_[key]->max_heap_key, max_heap_[(key - 1) /2]->max_heap_key);

      key = (key - 1) /2;
    }
  }

  void MinSiftDown(int key) {
    while (((2 * key) + 1) < min_heap_.size()) {
      int left_key = (2 * key) + 1;
      int right_key = (2 * key) + 2;

      int swap_key = left_key;
      if ((right_key < min_heap_.size()) && (min_heap_[right_key]->value < min_heap_[swap_key]->value)) {
        swap_key = right_key;
      }

      if (min_heap_[key]->value > min_heap_[swap_key]->value) {
        std::swap(min_heap_[key], min_heap_[swap_key]);
        std::swap(min_heap_[key]->min_heap_key, min_heap_[swap_key]->min_heap_key);

        key = swap_key;
      } else {
        break;
      }
    }
  }

  void MaxSiftDown(int key) {
    while (((2 * key) + 1) < max_heap_.size()) {
      int left_key = (2 * key) + 1;
      int right_key = (2 * key) + 2;

      int swap_key = left_key;
      if ((right_key < max_heap_.size()) && (max_heap_[right_key]->value > max_heap_[swap_key]->value)) {
        swap_key = right_key;
      }

      if (max_heap_[key]->value < max_heap_[swap_key]->value) {
        std::swap(max_heap_[key], max_heap_[swap_key]);
        std::swap(max_heap_[key]->max_heap_key, max_heap_[swap_key]->max_heap_key);

        key = swap_key;
      } else {
        break;
      }
    }
  }

  void ExtractMin() {
    std::swap(min_heap_[0], min_heap_[min_heap_.size() - 1]);
    std::swap(min_heap_[0]->min_heap_key, min_heap_[min_heap_.size() - 1]->min_heap_key);
    min_heap_.pop_back();
    MinSiftDown(0);
  }

  void ExtractMax() {
    std::swap(max_heap_[0], max_heap_[max_heap_.size() - 1]);
    std::swap(max_heap_[0]->max_heap_key, max_heap_[max_heap_.size() - 1]->max_heap_key);
    max_heap_.pop_back();
    MaxSiftDown(0);
  }

  void MinDeleteKey(int key) {
    min_heap_[key]->value = 1;
    MinSiftUp(key);
    ExtractMin();
  }

  void MaxDeleteKey(int key) {
    max_heap_[key]->value = 0x7EEEEEEE;
    MaxSiftUp(key);
    ExtractMax();
  }

  std::vector<Node *> min_heap_;
  std::vector<Node *> max_heap_;
};

int main() {
  int N;
  std::cin >> N;

  MinMaxHeap heap;

  for (int i = 0; i < N; i++) {
    std::string query;
    std::cin >> query;

    if (query == "GetMin") {
      int min = heap.GetMin();
      std::cout << min << std::endl;
    } else if (query == "GetMax") {
      int max = heap.GetMax();
      std::cout << max << std::endl;
    } else {
      std::string int_substring = query.substr(7, query.length() - 8);
      int A = std::stoi(int_substring);
      heap.Insert(A);
    }
  }
}