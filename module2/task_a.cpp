#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

bool compare_numbers(const std::string &x, const std::string &y) {
  return ((x + y) < (y + x));
}

int main() {
  std::ifstream in("number.in");
  std::ofstream out("number.out");

  std::vector<std::string> numbers;
  std::string current_number;
  while (in >> current_number) {
    numbers.push_back(current_number);
  }

  std::sort(numbers.rbegin(), numbers.rend(), compare_numbers);
  for (const auto &number : numbers) {
    out << number;
  }
}