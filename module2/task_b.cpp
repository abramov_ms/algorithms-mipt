#include <iostream>
#include <vector>
#include <algorithm>

struct Bracket {
  int position;
  bool is_opening;
};

bool compare_brackets(const Bracket &x, const Bracket &y) {
  return (x.position < y.position);
}

int main() {
  int N;
  std::cin >> N;

  std::vector<Bracket> brackets;
  brackets.reserve(2 * N);
  for (int i = 0; i < 2 * N; i++) {
    int bracket_position;
    std::cin >> bracket_position;

    Bracket current_bracket = {bracket_position, ((i % 2) == 0)};
    brackets.push_back(current_bracket);
  }

  std::sort(brackets.begin(), brackets.end(), compare_brackets);

  int current_opened_position = 0;
  int opened_brackets_count = 0;
  int total_length = 0;

  for (const auto &bracket : brackets) {
    if (opened_brackets_count == 1) {
      total_length += bracket.position - current_opened_position;
    }

    if (bracket.is_opening) {
      opened_brackets_count++;
    } else {
      opened_brackets_count--;
    }

    if (opened_brackets_count == 1) {
      current_opened_position = bracket.position;
    }
  }

  std::cout << total_length << std::endl;
}