// Дана очень длинная последовательность целых чисел длины N.
// Требуется вывести в отсортированном виде её наименьшие K элементов.
// Последовательность может не помещаться в память. Время работы O(N⋅log(K)), память O(K).
// 1 ≤ N ≤ 10^5, 1 ≤ K ≤ 500
//
// #Входные данные
// В первой строке записаны N и K.
// В следующей строке через пробел записана последовательность целых чисел.
//
// #Выходные данные
// K наименьших элементов последовательности в отсортированном виде.

#include <iostream>
#include <vector>

class MaxHeap {
 public:
  MaxHeap(int capacity = 0);

  int Size() const;
  bool Empty() const;
  int GetMax() const;

  void Insert(int element);
  void ReplaceMax(int element);
  void ExtractMax();

 private:
  void SiftUp(int key);
  void SiftDown(int key);

  std::vector<int> elements_;
};

MaxHeap::MaxHeap(int capacity) {
  elements_.reserve(capacity);
}

int MaxHeap::Size() const {
  return elements_.size();
}

bool MaxHeap::Empty() const {
  return (Size() == 0);
}

int MaxHeap::GetMax() const {
  if (Empty()) {
    throw std::runtime_error("cannot GetMax() from empty heap");
  }

  return elements_.front();
}

void MaxHeap::Insert(int element) {
  elements_.push_back(element);
  SiftUp(Size() - 1);
}

void MaxHeap::ReplaceMax(int element) {
  if (Empty()) {
    throw std::runtime_error("cannot ReplaceMax() in empty heap");
  }

  elements_.front() = element;
  SiftDown(0);
}

void MaxHeap::ExtractMax() {
  if (Empty()) {
    throw std::runtime_error("cannot ExtractMax() from empty heap");
  }

  std::swap(elements_.front(), elements_.back());
  elements_.pop_back();
  SiftDown(0);
}

void MaxHeap::SiftUp(int key) {
  while (elements_[key] > elements_[(key - 1) / 2]) {
    std::swap(elements_[key], elements_[(key - 1) / 2]);
    key = (key - 1) / 2;
  }
}

void MaxHeap::SiftDown(int key) {
  while (((2 * key) + 1) < Size()) {
    int left_key = (2 * key) + 1;
    int right_key = (2 * key) + 2;

    int swap_key = left_key;
    if ((right_key < Size()) && (elements_[right_key] > elements_[swap_key])) {
      swap_key = right_key;
    }

    if (elements_[key] < elements_[swap_key]) {
      std::swap(elements_[key], elements_[swap_key]);
    }

    key = swap_key;
  }
}

int main() {
  int N, K;
  std::cin >> N >> K;

  MaxHeap heap(K);

  for (int i = 0; i < K; i++) {
    int x;
    std::cin >> x;
    heap.Insert(x);
  }

  // После того как вставили в кучу первые K элементов, проверяем, что новый элемент
  // меньше максимального. Если меньше, то тогда его надо включить в список K минимальных для
  // текущего начального отрезка. В итоге каждый раз обращение к куче занимает O(log(K)) времени, всего N обращений.
  for (int i = K; i < N; i++) {
    int x;
    std::cin >> x;

    int max = heap.GetMax();
    if (x < max) {
      heap.ReplaceMax(x);
    }
  }

  std::vector<int> reverse_sorted;
  reverse_sorted.reserve(K);
  while (!heap.Empty()) {
    int max = heap.GetMax();
    reverse_sorted.push_back(max);
    heap.ExtractMax();
  }

  while (!reverse_sorted.empty()) {
    int element = reverse_sorted.back();
    reverse_sorted.pop_back();
    std::cout << element << " ";
  }
}
