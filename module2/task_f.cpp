#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

struct Node {
  long long value;
  long long min_heap_key;
  long long max_heap_key;
};

class MinMaxHeap {
 public:
  MinMaxHeap(long long size) {
    min_heap_.reserve(size);
    max_heap_.reserve(size);
  }

  Node *Insert(long long element) {
    Node *node = new Node;
    node->value = element;
    node->min_heap_key = min_heap_.size();
    node->max_heap_key = max_heap_.size();

    min_heap_.push_back(node);
    max_heap_.push_back(node);
    MinSiftUp(min_heap_.size() - 1);
    MaxSiftUp(max_heap_.size() - 1);

    return node;
  }

  long long GetMin() {
    Node *min = min_heap_[0];
    long long min_value = min->value;

    return min_value;
  }

  long long GetMax() {
    Node *max = max_heap_[0];
    long long max_value = max->value;

    return max_value;
  }

  void DecreaseKey(Node *node, long long n) {
    node->value -= n;
    MinSiftUp(node->min_heap_key);
    MaxSiftDown(node->max_heap_key);
  }

  void IncreaseKey(Node *node, long long n) {
    node->value += n;
    MaxSiftUp(node->max_heap_key);
    MinSiftDown(node->min_heap_key);
  }

 private:
  void MinSiftUp(long long key) {
    while (key != 0 && min_heap_[key]->value < min_heap_[(key - 1) / 2]->value) {
      std::swap(min_heap_[key], min_heap_[(key - 1) / 2]);
      std::swap(min_heap_[key]->min_heap_key, min_heap_[(key - 1) / 2]->min_heap_key);

      key = (key - 1) / 2;
    }
  }

  void MaxSiftUp(long long key) {
    while (key != 0 && max_heap_[key]->value > max_heap_[(key - 1) / 2]->value) {
      std::swap(max_heap_[key], max_heap_[(key - 1) / 2]);
      std::swap(max_heap_[key]->max_heap_key, max_heap_[(key - 1) / 2]->max_heap_key);

      key = (key - 1) / 2;
    }
  }

  void MinSiftDown(long long key) {
    while (((2 * key) + 1) < min_heap_.size()) {
      long long left_key = (2 * key) + 1;
      long long right_key = (2 * key) + 2;

      long long swap_key = left_key;
      if ((right_key < min_heap_.size()) && (min_heap_[right_key]->value < min_heap_[swap_key]->value)) {
        swap_key = right_key;
      }

      if (min_heap_[key]->value > min_heap_[swap_key]->value) {
        std::swap(min_heap_[key], min_heap_[swap_key]);
        std::swap(min_heap_[key]->min_heap_key, min_heap_[swap_key]->min_heap_key);

        key = swap_key;
      } else {
        break;
      }
    }
  }

  void MaxSiftDown(long long key) {
    while (((2 * key) + 1) < max_heap_.size()) {
      long long left_key = (2 * key) + 1;
      long long right_key = (2 * key) + 2;

      long long swap_key = left_key;
      if ((right_key < max_heap_.size()) && (max_heap_[right_key]->value > max_heap_[swap_key]->value)) {
        swap_key = right_key;
      }

      if (max_heap_[key]->value < max_heap_[swap_key]->value) {
        std::swap(max_heap_[key], max_heap_[swap_key]);
        std::swap(max_heap_[key]->max_heap_key, max_heap_[swap_key]->max_heap_key);

        key = swap_key;
      } else {
        break;
      }
    }
  }

  std::vector<Node *> min_heap_;
  std::vector<Node *> max_heap_;
};

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  long long L;
  long long N, K;
  //std::cin >> L >> N >> K;
  scanf("%lld%lld%lld", &L, &N, &K);

  long long M = L / K;

  MinMaxHeap heap(K);
  std::vector<Node *> groups;
  groups.reserve(K);
  for (long long i = 0; i < K; i++) {
    groups.push_back(heap.Insert(0));
  }

  std::vector<std::pair<long long, long long>> numbers;
  numbers.reserve(N);

  for (long long i = 0; i < N; i++) {
    long long x;
    //std::cin >> x;
    scanf("%lld", &x);
    x--; // дэлбики нумеруют с единички

    long long characteristic = M - (x % M);
    long long default_group = x / M;

    if (characteristic < M) {
      numbers.emplace_back(characteristic, default_group);
    }
    heap.IncreaseKey(groups[default_group], 1);
  }

  std::sort(numbers.begin(), numbers.end(), [](std::pair<long long, long long> x, std::pair<long long, long long> y) {
    return (x.first > y.first);
  }); // в конце лежат элементы с наименьшей характеристикой

  long long min_F = M;
  long long min_F_count = 1;
  long long min_F_slice = 0;

  long long current_slice = 0;

  long long min = heap.GetMin();
  long long max = heap.GetMax();
  long long F = max - min;
  if (F < min_F) {
    min_F = F;
    min_F_slice = current_slice;
  } // посчитали F для дефолтного разреза и сделали min_F = F
  bool is_prev_min = true; // сказали, что пока что 0 разрез минимальный

  while (!numbers.empty()) {
    long long current_characteristic = numbers.back().first;
    while (!numbers.empty() && numbers.back().first == current_characteristic) {
      heap.DecreaseKey(groups[numbers.back().second], 1);
      heap.IncreaseKey(groups[(numbers.back().second + 1) % K], 1);
      numbers.pop_back();
    } // переложили элементы с одинаковой характеристикой в новые группы

    long long new_min = heap.GetMin();
    long long new_max = heap.GetMax();
    long long new_F = new_max - new_min;
    if (new_F < min_F) {
      min_F = new_F;
      min_F_slice = current_characteristic;
      min_F_count = 1;
      is_prev_min = true;
    } else if (new_F == min_F) {
      if (is_prev_min) {
        min_F_count += current_characteristic - min_F_slice - 1;
      }
      min_F_slice = current_characteristic;
      min_F_count++;
      is_prev_min = true;
    } else if (is_prev_min) {
      min_F_count += current_characteristic - min_F_slice - 1;
      is_prev_min = false;
    }
  }

  if (is_prev_min) { // если элемент с максимальной характеристикой дает наилучший разрез, обрабатываем это
    min_F_count += (M - 1) - min_F_slice + 1 - 1;
  }

  min_F_count *= K; // разрезов можно сделать в K раз больше, но тогда будут повторяться ситуации

  // если slice = d, то мы режем после (M-1-d)-го элемента, то есть по факту
  //std::cout << min_F << " " << min_F_count << std::endl << M - min_F_slice;
  printf("%lld %lld\n%lld", min_F, min_F_count, M - min_F_slice);
}