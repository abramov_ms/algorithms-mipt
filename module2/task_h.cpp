// Дан массив неотрицательных целых 64-битных чисел. Количество чисел не больше 10^6.
// Отсортировать массив методом поразрядной сортировки LSD по байтам.
//
// #Входные данные
// В первой строке вводится количество чисел в массиве N. На следующей строке через пробел
// вводятся N неотрицательных чисел.
//
// #Выходные данные
// Выведите этот массив, отсортированный в порядке возрастания, в одну строчку через пробел.

#include <iostream>
#include <vector>

void ByteSort(std::vector<unsigned long long> &sequence) {
  std::vector<int> counts(256);
  unsigned long long power = 1;
  for (int i = 0; i < 8; i++) {
    counts.assign(256, 0);
    for (const auto& element : sequence) {
      int byte = (element / power) % 256;
      counts[byte]++;
    }
    for (int j = 1; j < counts.size(); j++) {
      counts[j] += counts[j - 1];
    }
    std::vector<unsigned long long> result(sequence.size());
    for (int j = sequence.size() - 1; j >= 0; j--) {
      int byte = (sequence[j] / power) % 256;
      result[--counts[byte]] = sequence[j];
    }
    sequence = std::move(result);
    power *= 256;
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);

  int N;
  std::cin >> N;

  std::vector<unsigned long long> sequence;
  sequence.reserve(N);
  for (int i = 0; i < N; i++) {
    unsigned long long element;
    std::cin >> element;
    sequence.push_back(element);
  }

  ByteSort(sequence);

  for (const auto& element : sequence) {
    std::cout << element << '\n';
  }
}