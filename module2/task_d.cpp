// Реализуйте двоичную кучу.
//
// Обработайте запросы следующих видов:
// + insert x: вставить целое число x в кучу;
// + getMin: вывести значение минимального элемента в куче (гарантируется, что к этому моменту куча не пуста);
// + extractMin: удалить минимальный элемент из кучи (гарантируется, что к этому моменту куча не пуста);
// + decreaseKey i Δ: уменьшить число, вставленное на i-м запросе, на целое число Δ ≥ 0
//   (гарантируется, что i-й запрос был осуществлён ранее, являлся запросом добавления,
//   а добавленное на этом шаге число всё ещё лежит в куче).
// Можете считать, что в любой момент времени все числа, хранящиеся в куче, попарно различны,
// а их количество не превышает 10^5.
//
// #Входные данные
// В первой строке содержится число q (1 ≤ q ≤ 10^6), означающее число запросов.
// В следующих q строках содержатся запросы в описанном выше формате.
// Добавляемые числа x и поправки Δ лежат в промежутке [−10^9, 10^9], а Δ ≥ 0.
//
// #Выходные данные
// На каждый запрос вида getMin выведите ответ в отдельной строке.

#include <iostream>
#include <vector>
#include <string>
#include <memory>

class Heap {
 public:
  Heap(int capacity = 0);

  long long GetMin() const;

  const int *Insert(int element);
  void ExtractMin();
  void DecreaseKey(int key, int delta);

 private:
  struct Node;

  void SiftUp(int key);
  void SiftDown(int key);

  std::vector<std::unique_ptr<Node>> elements_;
};

struct Heap::Node {
  long long value;
  int key;
};

Heap::Heap(int capacity) {
  elements_.reserve(capacity);
}

long long Heap::GetMin() const {
  return elements_.front()->value;
}

const int *Heap::Insert(int element) {
  auto current_node = std::make_unique<Node>();
  current_node->value = element;
  current_node->key = elements_.size();

  auto key = &current_node->key;

  elements_.push_back(std::move(current_node));
  SiftUp(elements_.back()->key);

  return key;
}

void Heap::ExtractMin() {
  std::swap(elements_.front(), elements_.back());
  std::swap(elements_.front()->key, elements_.back()->key);

  elements_.pop_back();
  SiftDown(0);
}

void Heap::DecreaseKey(int key, int delta) {
  elements_[key]->value -= delta;
  SiftUp(key);
}

void Heap::SiftUp(int key) {
  while (elements_[key]->value < elements_[(key - 1) / 2]->value) {
    std::swap(elements_[key], elements_[(key - 1) / 2]);
    std::swap(elements_[key]->key, elements_[(key - 1) / 2]->key);

    key = (key - 1) / 2;
  }
}

void Heap::SiftDown(int key) {
  while (((2 * key) + 1) < elements_.size()) {
    int left_key = (2 * key) + 1;
    int right_key = (2 * key) + 2;

    int swap_key = left_key;
    if ((right_key < elements_.size()) && (elements_[right_key]->value < elements_[swap_key]->value)) {
      swap_key = right_key;
    }

    if (elements_[key]->value > elements_[swap_key]->value) {
      std::swap(elements_[key], elements_[swap_key]);
      std::swap(elements_[key]->key, elements_[swap_key]->key);

      key = swap_key;
    } else {
      break;
    }
  }
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);

  int q;
  std::cin >> q;

  std::vector<const int *> insertion_keys(q);
  Heap heap((q < 100000) ? q : 100000);

  for (int i = 0; i < q; i++) {
    std::string query;
    std::cin >> query;

    if (query == "insert") {
      int x;
      std::cin >> x;
      auto insertion_key = heap.Insert(x);
      insertion_keys[i] = insertion_key;
    } else if (query == "getMin") {
      long long min = heap.GetMin();
      std::cout << min << '\n';
    } else if (query == "extractMin") {
      heap.ExtractMin();
    } else if (query == "decreaseKey") {
      int query_number, delta;
      std::cin >> query_number >> delta;
      int key = *insertion_keys[query_number - 1];
      heap.DecreaseKey(key, delta);
    }
  }
}