// Напишите программу, реализующую структуру данных, позволяющую добавлять и удалять элементы, а также находить k-й
// максимум.
//
// #Входные данные
// Первая строка входного файла содержит натуральное число n — количество команд (n ≤ 100000). Последующие n строк
// содержат по одной команде каждая. Команда записывается в виде двух чисел c_i и k_i — тип и аргумент команды
// соответственно (|k_i| ≤ 10^9).
//
// Поддерживаемые команды:
// +1 (или просто 1): Добавить элемент с ключом k_i.
//  0: Найти и вывести k_i-й максимум.
// −1: Удалить элемент с ключом k_i.
// Гарантируется, что в процессе работы в структуре не требуется хранить элементы с равными ключами или удалять
// несуществующие элементы. Также гарантируется, что при запросе k_i-го максимума, он существует.
//
// #Выходные данные
// Для каждой команды нулевого типа в выходной файл должна быть выведена строка, содержащая единственное число — k_i-й
// максимум.

#include <iostream>
#include <memory>

class AvlTree {
 public:
  AvlTree() : key_(0), height_(0), size_(0) {}

  void Insert(int);
  void Delete(int);
  bool Exists(int) const;
  const AvlTree &Next(int) const;
  const AvlTree &Prev(int) const;
  const AvlTree &KthElement(int) const;

  int GetKey() const { return key_; }
  int GetSize() const { return size_; }
  bool Empty() const { return (Height() == 0); }

 private:
  int FindMin() const;
  void DeleteMin();

  void RotateLeft();
  void RotateRight();
  void Balance();

  int BalanceFactor() const { return (Left()->Height() - Right()->Height()); }
  void UpdateHeightAndSize();

  int &Key() { return key_; }
  int &Size() { return size_; }
  int &Height() { return height_; }
  int Height() const { return height_; }
  std::unique_ptr<AvlTree> &Left() const;
  std::unique_ptr<AvlTree> &Right() const;

  int key_;
  int height_;
  int size_;
  mutable std::unique_ptr<AvlTree> left_;
  mutable std::unique_ptr<AvlTree> right_;
};

void AvlTree::Insert(int key) {
  if (Empty()) {
    Key() = key;
    Height() = 1;
    Size() = 1;

    return;
  }

  if (key == Key()) {
    return;
  }

  if (key < Key()) {
    Left()->Insert(key);
  } else {
    Right()->Insert(key);
  }

  Balance();
}

void AvlTree::Delete(int key) {
  if (Empty()) {
    return;
  }

  if (key < Key()) {
    Left()->Delete(key);
  } else if (key > Key()) {
    Right()->Delete(key);
  } else {
    if (Right()->Empty()) {
      auto left = std::move(Left());
      Key() = left->Key();
      Height() = left->Height();
      Size() = left->Size();
      Left() = std::move(left->Left());
      Right() = std::move(left->Right());

      return;
    }

    Key() = Right()->FindMin();
    Right()->DeleteMin();
  }

  Balance();
}

const AvlTree &AvlTree::Next(int key) const {
  if (Empty()) {
    return *this;
  }

  if (GetKey() <= key) {
    return Right()->Next(key);
  } else {
    auto &tighter_upper_bound = Left()->Next(key);
    return tighter_upper_bound.Empty() ? *this : tighter_upper_bound;
  }
}

const AvlTree &AvlTree::Prev(int key) const {
  if (Empty()) {
    return *this;
  }

  if (GetKey() >= key) {
    return Left()->Prev(key);
  } else {
    auto &tighter_lower_bound = Right()->Prev(key);
    return tighter_lower_bound.Empty() ? *this : tighter_lower_bound;
  }
}

const AvlTree &AvlTree::KthElement(int k) const {
  if (Empty()) {
    return *this;
  }

  auto count = 1 + Left()->Size();

  if (count == k) {
    return *this;
  } else if (count > k) {
    return Left()->KthElement(k);
  } else {
    return Right()->KthElement(k - count);
  }
}

bool AvlTree::Exists(int key) const {
  if (Empty()) {
    return false;
  }

  if (key == GetKey()) {
    return true;
  }

  return (key < GetKey()) ? Left()->Exists(key) : Right()->Exists(key);
}

int AvlTree::FindMin() const {
  return Left()->Empty() ? GetKey() : Left()->FindMin();
}

void AvlTree::DeleteMin() {
  if (Left()->Empty()) {
    auto right = std::move(Right());
    Key() = right->Key();
    Height() = right->Height();
    Size() = right->Size();
    Left() = std::move(right->Left());
    Right() = std::move(right->Right());

    return;
  }

  Left()->DeleteMin();
  Balance();
}

// Поворот влево ребра (alpha, beta):
//
//      alpha               beta
//     /    \              /    \
//    X    beta   ~~>   alpha    Z
//        /    \       /     \
//       Y      Z     X       Y
//
void AvlTree::RotateLeft() {
  auto beta = std::move(Right());
  auto X = std::move(Left());
  auto Y = std::move(beta->Left());
  auto Z = std::move(beta->Right());

  std::swap(Key(), beta->Key());
  Right() = std::move(Z);
  beta->Left() = std::move(X);
  beta->Right() = std::move(Y);
  Left() = std::move(beta);

  Left()->UpdateHeightAndSize();
  UpdateHeightAndSize();
}

// Поворот вправо ребра (alpha, beta):
//
//        alpha           beta
//       /     \         /    \
//     beta     Z  ~~>  X    alpha
//    /    \                /     \
//   X      Y              Y       Z
//
void AvlTree::RotateRight() {
  auto beta = std::move(Left());
  auto X = std::move(beta->Left());
  auto Y = std::move(beta->Right());
  auto Z = std::move(Right());

  std::swap(Key(), beta->Key());
  Left() = std::move(X);
  beta->Left() = std::move(Y);
  beta->Right() = std::move(Z);
  Right() = std::move(beta);

  Right()->UpdateHeightAndSize();
  UpdateHeightAndSize();
}

void AvlTree::Balance() {
  UpdateHeightAndSize();

  if (BalanceFactor() == -2) {
    if (Right()->BalanceFactor() > 0) {
      Right()->RotateRight();
    }
    RotateLeft();
  } else if (BalanceFactor() == 2) {
    if (Left()->BalanceFactor() < 0) {
      Left()->RotateLeft();
    }
    RotateRight();
  }
}

void AvlTree::UpdateHeightAndSize() {
  if (!Empty()) {
    Height() = 1 + std::max(Left()->Height(), Right()->Height());
    Size() = 1 + Left()->Size() + Right()->Size();
  }
}

std::unique_ptr<AvlTree> &AvlTree::Left() const {
  if (left_ == nullptr) {
    left_ = std::make_unique<AvlTree>();
  }

  return left_;
}

std::unique_ptr<AvlTree> &AvlTree::Right() const {
  if (right_ == nullptr) {
    right_ = std::make_unique<AvlTree>();
  }

  return right_;
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);

  AvlTree tree;

  int n;
  std::cin >> n;
  for (int i = 0; i < n; i++) {
    int query, arg;
    std::cin >> query >> arg;

    if (query == 1) {
      tree.Insert(arg);
    } else if (query == 0) {
      auto &max = tree.KthElement(tree.GetSize() - arg + 1);
      std::cout << max.GetKey() << '\n';
    } else {
      tree.Delete(arg);
    }
  }
}