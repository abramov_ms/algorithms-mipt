Сначала отсортируем футболки по качеству и цене (сделаем компаратор, который в первую очередь учитывает качество и потом цену),
так чтобы сначала шли самые дешевые из самых хороших футболок, на это потребуется *O(n log(n))* времени. Далее построим дерево,
в котором вершинами будут структуры с номером какого-то покупателя, количеством денег у него и полями для отложенных операций
(упорядочивание в дереве происходит по количеству денег). Пройдемся теперь по отсортированному массиву с футболками. Пусть на очередном шаге стоимость футболки из
массива составляет *M*. Найдем в дереве покупателя с наименьшим количеством денег, достаточным, чтобы купить эту футболку –
на это потребуется *O(log(k))* времени.  Если не нашелся, то идем к следующей футболке. Если нашелся, то этому покупателю 
соответствуют более правые вершины покупателей, которые одновременно тоже хотят (в силу сортировки футболок) и могут себе
позволить (потому что они правее) купить эту футболку. Сделаем `Split()`  дерева по *M* за *O(log(k))* – в левом дереве после сплита –
 те, кто не будет покупать футболку, в правом – те, кто будет, – и заберем у каждого покупателя из правого дерева *M* денег, добавим *+1*
 к числу купленных футболок. Это можно сделать записав в корень отложенную операцию, которая будет проталкиваться вниз при обращении
 к детям. Потом найдем в правом дереве покупателя с минимальным количеством денег. Нам нужно сохранить разделение исходного
 дерева по *M*, поэтому делаем так: если теперь у минимального покупателя денег меньше *M*, вырезаем его из правого дерева и вставляем
 в левое. Повторяем такую операцию, пока у минимального покупателя в правом дереве не будет как минимум M денег. После этого можем
  объединить  правое и левое дерево за *O(log(k))*. \[Этот этап можно пропустить, если левое дерево пусто.\] Заметим, что если
  после покупки покупатель перешел из правого дерева в левое, то у него стало как минимум вдвое меньше денег (*x - M < M <=> x < 2M
  <=> 2x < 2M + x <=> 2(x – M) < x <=> x/(x-M) > 2* при *x  > M*). Это значит, что мы можем отдельно от остальных операций оценить
  все перемещения из одного дерева в другое как *O(k log(C) log(k))*. Остальные операции: сортировка футболок за *O(n log(n))*,
  `Split()`/`Merge()`+вычитание денег+подсчет количества футболок за *O(n log(k))* с учетом отложенных операций. В конце нужно будет
  обойти дерево, и завершить выполнение отложенных операций, но это лишь *O(k)*, поэтому итоговая асимптотика
  *O(n log(n) + n log(k) + k log(C) log(k))*.