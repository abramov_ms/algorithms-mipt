// Реализуйте AVL-дерево. Решения с использованием других структур засчитываться не будут.
//
// #Входные данные
// Входной файл содержит описание операций с деревом. Операций не больше 10^5.
// В каждой строке находится одна из следующих операций:
// + insert x — добавить в дерево ключ x.
// + delete x — удалить из дерева ключ x. Если ключа x в дереве нет, то ничего делать не надо.
// + exists x — если ключ x есть в дереве, вывести «true», иначе «false»
// + next x — минимальный элемент в дереве, больший x, или «none», если такого нет.
// + prev x — максимальный элемент в дереве, меньший x, или «none», если такого нет.
// Все числа во входном файле целые и по модулю не превышают 10^9.
//
// #Выходные данные
// Выведите последовательно результат выполнения всех операций exists, next, prev.

#include <iostream>
#include <memory>

class AvlTree {
 public:
  AvlTree() : key_(0), height_(0) {}

  void Insert(int);
  void Delete(int);
  bool Exists(int) const;
  const AvlTree *Next(int) const;
  const AvlTree *Prev(int) const;

  int Key() const { return key_; }
  bool Empty() const { return (Height() == 0); }

 private:
  int FindMin() const;
  void DeleteMin();

  void RotateLeft();
  void RotateRight();
  void Balance();

  int BalanceFactor() const;
  void UpdateHeight();

  int &Key() { return key_; }
  int &Height() { return height_; }
  int Height() const { return height_; }
  std::unique_ptr<AvlTree> &Left();
  std::unique_ptr<AvlTree> &Right();

  int key_;
  int height_;
  std::unique_ptr<AvlTree> left_;
  std::unique_ptr<AvlTree> right_;
};

void AvlTree::Insert(int key) {
  if (Empty()) {
    Key() = key;
    Height() = 1;

    return;
  }

  if (key == Key()) {
    return;
  }

  if (key < Key()) {
    Left()->Insert(key);
  } else {
    Right()->Insert(key);
  }

  Balance();
}

void AvlTree::Delete(int key) {
  if (Empty()) {
    return;
  }

  if (key < Key()) {
    Left()->Delete(key);
  } else if (key > Key()) {
    Right()->Delete(key);
  } else {
    if (Right()->Empty()) {
      auto left = std::move(Left());
      Key() = left->Key();
      Height() = left->Height();
      Left() = std::move(left->Left());
      Right() = std::move(left->Right());

      return;
    }

    Key() = Right()->FindMin();
    Right()->DeleteMin();
  }

  Balance();
}

const AvlTree *AvlTree::Next(int key) const {
  if (Empty()) {
    return nullptr;
  }

  if (Key() <= key) {
    return (right_ != nullptr) ? right_->Next(key) : nullptr;
  } else {
    auto tighter_upper_bound = (left_ != nullptr) ? left_->Next(key) : nullptr;
    return (tighter_upper_bound == nullptr) ? this : tighter_upper_bound;
  }
}

const AvlTree *AvlTree::Prev(int key) const {
  if (Empty()) {
    return nullptr;
  }

  if (Key() >= key) {
    return (left_ != nullptr) ? left_->Prev(key) : nullptr;
  } else {
    auto tighter_lower_bound = (right_ != nullptr) ? right_->Prev(key) : nullptr;
    return (tighter_lower_bound == nullptr) ? this : tighter_lower_bound;
  }
}

bool AvlTree::Exists(int key) const {
  if (Empty()) {
    return false;
  }

  if (key == Key()) {
    return true;
  }

  if (key < Key()) {
    return (left_ != nullptr) && left_->Exists(key);
  } else {
    return (right_ != nullptr) && right_->Exists(key);
  }
}

int AvlTree::FindMin() const {
  return ((left_ == nullptr) || left_->Empty()) ? Key() : left_->FindMin();
}

void AvlTree::DeleteMin() {
  if (Left()->Empty()) {
    auto right = std::move(Right());
    Key() = right->Key();
    Height() = right->Height();
    Left() = std::move(right->Left());
    Right() = std::move(right->Right());

    return;
  }

  Left()->DeleteMin();
  Balance();
}

// Поворот влево ребра (alpha, beta):
//
//      alpha               beta
//     /    \              /    \
//    X    beta   ~~>   alpha    Z
//        /    \       /     \
//       Y      Z     X       Y
//
void AvlTree::RotateLeft() {
  auto beta = std::move(Right());
  auto X = std::move(Left());
  auto Y = std::move(beta->Left());
  auto Z = std::move(beta->Right());

  std::swap(Key(), beta->Key());
  Right() = std::move(Z);
  beta->Left() = std::move(X);
  beta->Right() = std::move(Y);
  Left() = std::move(beta);

  Left()->UpdateHeight();
  UpdateHeight();
}

// Поворот вправо ребра (alpha, beta):
//
//        alpha           beta
//       /     \         /    \
//     beta     Z  ~~>  X    alpha
//    /    \                /     \
//   X      Y              Y       Z
//
void AvlTree::RotateRight() {
  auto beta = std::move(Left());
  auto X = std::move(beta->Left());
  auto Y = std::move(beta->Right());
  auto Z = std::move(Right());

  std::swap(Key(), beta->Key());
  Left() = std::move(X);
  beta->Left() = std::move(Y);
  beta->Right() = std::move(Z);
  Right() = std::move(beta);

  Right()->UpdateHeight();
  UpdateHeight();
}

void AvlTree::Balance() {
  UpdateHeight();

  if (BalanceFactor() == -2) {
    if (Right()->BalanceFactor() > 0) {
      Right()->RotateRight();
    }
    RotateLeft();
  } else if (BalanceFactor() == 2) {
    if (Left()->BalanceFactor() < 0) {
      Left()->RotateLeft();
    }
    RotateRight();
  }
}

int AvlTree::BalanceFactor() const {
  auto left_height = (left_ != nullptr) ? left_->height_ : 0;
  auto right_height = (right_ != nullptr) ? right_->height_ : 0;

  return (left_height - right_height);
}

void AvlTree::UpdateHeight() {
  if (!Empty()) {
    Height() = 1 + std::max(Left()->Height(), Right()->Height());
  }
}

std::unique_ptr<AvlTree> &AvlTree::Left() {
  if (left_ == nullptr) {
    left_ = std::make_unique<AvlTree>();
  }

  return left_;
}

std::unique_ptr<AvlTree> &AvlTree::Right() {
  if (right_ == nullptr) {
    right_ = std::make_unique<AvlTree>();
  }

  return right_;
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);

  AvlTree tree;

  std::string query_type;
  while (std::cin >> query_type) {
    if (query_type == "exit") {
      break;
    }

    int query_argument;
    std::cin >> query_argument;

    if (query_type == "insert") {
      tree.Insert(query_argument);
    } else if (query_type == "delete") {
      tree.Delete(query_argument);
    } else if (query_type == "exists") {
      std::cout << (tree.Exists(query_argument) ? "true" : "false") << '\n';
    } else if (query_type == "next") {
      auto next = tree.Next(query_argument);
      if (next != nullptr) {
        std::cout << next->Key() << '\n';
      } else {
        std::cout << "none" << '\n';
      }
    } else if (query_type == "prev") {
      auto prev = tree.Prev(query_argument);
      if (prev != nullptr) {
        std::cout << prev->Key() << '\n';
      } else {
        std::cout << "none" << '\n';
      }
    }
  }
}