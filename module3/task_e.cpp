// На вступительном контесте в пилотную группу по программированию Вашему другу предложили реализовать структуру данных
// для хранения множеств чисел. Так как он специализируется на истории литературы, данную структуру придётся реализовать
// Вам.
// Структура должна хранить (m + 1) множеств чисел от 0 до n, пронумерованных от 0 до m включительно, при этом одно число
// может принадлежать сразу нескольким множествам. Изначально все множества пустые.
// Вы должны реализовать следующие операции на этой структуре:
// + ADD e s
//   Добавить в множество №s (0 ≤ s ≤ m) число e (0 ≤ e ≤ n).
// + DELETE e s
//   Удалить из множества №s (0 ≤ s ≤ m) число e (0 ≤ e ≤ n). Гарантируется, что до этого число e было помещено в множество
// + CLEAR s
//   Очистить множество №s (0 ≤ s ≤ m).
// + LISTSET s
//   Показать содержимое множества №s (0 ≤ s ≤ m) в возрастающем порядке, либо −1, если множество пусто.
// + LISTSETSOF e
//   Показать множества, в которых лежит число e (0≤e≤n), либо −1, если этого числа нет ни в одном множестве.
//
// #Входные данные
// Сначала вводятся числа N (1 ≤ N ≤ 9223372036854775807), M (1 ≤ M ≤ 100000) и K (0 ≤ K ≤ 100000)  — максимальное
// число, номер максимального множества и количество запросов к структуре данных. Далее следуют K строк указанного
// формата запросов.
//
// #Выходные данные
// На каждый запрос LISTSET Ваша программа должна вывести числа  — содержимое запрошенного множества или −1, если
// множество пусто.
// На каждый запрос LISTSETSOF Ваша программа должна вывести числа  — номера множеств, содержащих запрошенное число, или
// −1, если таких множеств не существует.
// На прочие запросы не должно быть никакого вывода.
//
// Гарантируется, что правильный вывод программы не превышает одного мегабайта.

#include <iostream>
#include <set>
#include <map>

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);

  long long N;
  std::cin >> N;
  int M, K;
  std::cin >> M >> K;

  std::map<long long, std::set<int>> num_set_dict;
  std::map<int, std::set<long long>> set_num_dict;

  for (int i = 0; i < K; i++) {
    std::string query;
    std::cin >> query;

    if (query == "ADD") {
      long long e;
      std::cin >> e;
      int s;
      std::cin >> s;

      num_set_dict[e].insert(s);
      set_num_dict[s].insert(e);
    } else if (query == "DELETE") {
      long long e;
      std::cin >> e;
      int s;
      std::cin >> s;

      num_set_dict[e].erase(s);
      set_num_dict[s].erase(e);
    } else if (query == "CLEAR") {
      int s;
      std::cin >> s;

      for (const auto& elem : set_num_dict[s]) {
        num_set_dict[elem].erase(s);
      }
      set_num_dict[s].clear();
    } else if (query == "LISTSET") {
      int s;
      std::cin >> s;

      if ((set_num_dict.count(s) == 0) || set_num_dict[s].empty()) {
        std::cout << -1 << '\n';
      } else {
        for (const auto& elem : set_num_dict[s]) {
          std::cout << elem << ' ';
        }
        std::cout << '\n';
      }
    } else if (query == "LISTSETSOF") {
      long long e;
      std::cin >> e;

      if ((num_set_dict.count(e) == 0) || num_set_dict[e].empty()) {
        std::cout << -1 << '\n';
      } else {
        for (const auto& elem : num_set_dict[e]) {
          std::cout << elem << ' ';
        }
        std::cout << '\n';
      }
    }
  }
}