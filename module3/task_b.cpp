// Реализуйте splay-дерево, которое поддерживает множество S целых чисел, в котором разрешается производить следующие
// операции:
// + add(i) — добавить в множество S число i (если он там уже есть, то множество не меняется);
// + sum(l,r) — вывести сумму всех элементов x из S, которые удовлетворяют неравенству l ≤ x ≤ r.
//
// Решения, не использующие splay-деревья, будут игнорироваться.
// Решения, не использующие операции split и merge, будут оцениваться в 2 балла. Решения, использующие операции split и
// merge, оцениваются в 3 балла.
//
// #Входные данные
// Исходно множество S пусто. Первая строка входного файла содержит n — количество операций (1≤n≤300000). Следующие n
// строк содержат операции. Каждая операция имеет вид либо «+ i», либо «? l r». Операция «? l r» задает запрос sum(l,r).
// Если операция «+ i» идет во входном файле в начале или после другой операции «+», то она задает операцию add(i).
// Если же она идет после запроса «?», и результат этого запроса был y, то выполняется операция add((i + y) mod 10^9).
// Во всех запросах и операциях добавления параметры лежат в интервале от 0 до 10^9.
//
// #Выходные данные
// Для каждого запроса выведите одно число — ответ на запрос.

#include <iostream>
#include <memory>

class SplayTree {
 public:
  void Insert(int);
  long long Sum(int lower_bound, int upper_bound);

 private:
  struct Node;
  typedef std::shared_ptr<Node> NodePtr;

  static void Rotate(NodePtr &, NodePtr &);
  static NodePtr Find(NodePtr, int);
  static NodePtr Splay(NodePtr &);
  static std::pair<NodePtr, NodePtr> Split(NodePtr &, int, bool);
  static NodePtr Merge(NodePtr &, NodePtr &);

  NodePtr root_;
};

struct SplayTree::Node {
  Node(int key) : key(key), subtree_sum(key) {};

  void UpdateSum();

  int key;
  long long subtree_sum;
  NodePtr left;
  NodePtr right;
  NodePtr parent;
};

void SplayTree::Node::UpdateSum() {
  subtree_sum = key;
  if (left != nullptr) {
    subtree_sum += left->subtree_sum;
  }
  if (right != nullptr) {
    subtree_sum += right->subtree_sum;
  }
}

void SplayTree::Insert(int key) {
  auto roots = Split(root_, key, true);

  if (roots.first != nullptr && roots.first->key == key) {
    root_ = Merge(roots.first, roots.second);
    return;
  }

  auto new_root = std::make_shared<Node>(key);
  new_root->left = roots.first;
  if (roots.first != nullptr) {
    roots.first->parent = new_root;
  }
  new_root->right = roots.second;
  if (roots.second != nullptr) {
    roots.second->parent = new_root;
  }
  new_root->UpdateSum();

  root_ = new_root;
}

long long SplayTree::Sum(int lower_bound, int upper_bound) {
  auto lb_split = Split(root_, lower_bound, false);
  auto ub_split = Split(lb_split.second, upper_bound, true);
  auto sum_tree = ub_split.first;

  auto sum = (sum_tree != nullptr) ? sum_tree->subtree_sum : 0;

  auto ub_merge = Merge(ub_split.first, ub_split.second);
  auto lb_merge = Merge(lb_split.first, ub_merge);
  root_ = lb_merge;

  return sum;
}

void SplayTree::Rotate(NodePtr &parent, NodePtr &child) {
  auto grandparent = parent->parent;
  if (grandparent != nullptr) {
    if (parent == grandparent->left) {
      grandparent->left = child;
    } else {
      grandparent->right = child;
    }
  }
  child->parent = grandparent;

  if (child == parent->left) {
    parent->left = child->right;
    if (parent->left != nullptr) {
      parent->left->parent = parent;
    }
    child->right = parent;
  } else {
    parent->right = child->left;
    if (parent->right != nullptr) {
      parent->right->parent = parent;
    }
    child->left = parent;
  }
  parent->parent = child;

  parent->UpdateSum();
  child->UpdateSum();
}

SplayTree::NodePtr SplayTree::Splay(NodePtr &node) {
  auto parent = node->parent;
  if (parent == nullptr) {
    return node;
  }

  auto grandparent = parent->parent;
  if (grandparent == nullptr) {
    Rotate(parent, node);
    return node;
  }

  auto is_zig_zig = ((parent == grandparent->left) == (node == parent->left));
  if (is_zig_zig) {
    Rotate(grandparent, parent);
    Rotate(parent, node);
  } else {
    Rotate(parent, node);
    Rotate(grandparent, node);
  }
  return Splay(node);
}

SplayTree::NodePtr SplayTree::Find(NodePtr root, int key) {
  if (root == nullptr) {
    return nullptr;
  }

  if ((key < root->key) && (root->left != nullptr)) {
    return Find(root->left, key);
  } else if ((key > root->key) && (root->right != nullptr)) {
    return Find(root->right, key);
  }

  return Splay(root);
}

std::pair<SplayTree::NodePtr, SplayTree::NodePtr> SplayTree::Split(NodePtr &root, int key, bool keep_in_left) {
  if (root == nullptr) {
    return std::make_pair(nullptr, nullptr);
  }

  auto closest = Find(root, key);
  if ((key < closest->key) || (!keep_in_left && (key == closest->key))) {
    auto left = closest->left;
    closest->left = nullptr;
    if (left != nullptr) {
      left->parent = nullptr;
    }
    closest->UpdateSum();

    return std::make_pair(left, closest);
  } else {
    auto right = closest->right;
    closest->right = nullptr;
    if (right != nullptr) {
      right->parent = nullptr;
    }
    closest->UpdateSum();

    return std::make_pair(closest, right);
  }
}

SplayTree::NodePtr SplayTree::Merge(NodePtr &lhs, NodePtr &rhs) {
  if (lhs == nullptr) {
    return rhs;
  } else if (rhs == nullptr) {
    return lhs;
  }

  rhs = Find(rhs, lhs->key);
  rhs->left = lhs;
  lhs->parent = rhs;
  rhs->UpdateSum();

  return rhs;
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);

  SplayTree tree;

  int n;
  std::cin >> n;
  bool is_last_add = true;
  long long last_sum = 0;
  for (int j = 0; j < n; j++) {
    char c;
    std::cin >> c;

    if (c == '+') {
      int i;
      std::cin >> i;

      if (is_last_add) {
        tree.Insert(i);
      } else {
        tree.Insert((i + last_sum) % 1'000'000'000);
      }

      is_last_add = true;
    } else {
      int l, r;
      std::cin >> l >> r;
      last_sum = tree.Sum(l, r);

      std::cout << last_sum << '\n';
      is_last_add = false;
    }
  }
}
